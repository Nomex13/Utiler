# Utiler

A utility library.

## Usage

Add the `Iodynis.Libraries.Utility` namespace for utility classes and static methods and the `Iodynis.Libraries.Utility.Extensions` namespace for extensions.

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.