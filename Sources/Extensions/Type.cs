﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Get inherited types.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="includeClasses">Include or not inherited classes.</param>
        /// <param name="includeInterfaces">Include or not inherited interfaces.</param>
        /// <returns></returns>
        public static IEnumerable<Type> GetInherited(this Type type, bool includeClasses = true, bool includeInterfaces = true)
        {
            // Check for base type
            if (type == null)
            {
                yield break;
            }

            // Return all implemented or inherited interfaces
            if (includeInterfaces)
            {
                foreach (var @interface in type.GetInterfaces())
                {
                    yield return @interface;
                }
            }

            // Return all inherited types
            if (includeClasses)
            {
                var currentBaseType = type.BaseType;
                while (currentBaseType != null)
                {
                    yield return currentBaseType;
                    currentBaseType = currentBaseType.BaseType;
                }
            }
        }
    }
}
