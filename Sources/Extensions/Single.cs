﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static int ToNearest(this float value)
        {
            return value > 0 ? (int)(value + 0.5f) : (value < 0 ? (int)(value - 0.5f) : 0);
        }
        /// <summary>
        /// Round the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to round.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value rounded to specified count of significant digits.</returns>
        public static float RoundToSignificantDigits(this float value, int count)
        {
            return (float)RoundToSignificantDigits((double)value, count);
        }
        ///// <summary>
        ///// Format as string with the specified count of significant digits.
        ///// </summary>
        ///// <param name="value">Value to format as string.</param>
        ///// <param name="count">Significant digits count.</param>
        ///// <returns>String value rounded to specified count of significant digits.</returns>
        //public static string ToStringOnlySignificantDigits(this float value, int count)
        //{
        //    return ToStringOnlySignificantDigits((double)value, count);
        //}
        public static long GetMantise(this float value)
        {
            long bits = BitConverter.DoubleToInt64Bits(value);
            return bits & 0x007F_FFFF;
        }
        public static int GetExponent(this float value)
        {
            long bits = BitConverter.DoubleToInt64Bits(value);
            return (int) ((bits >> 23) & 0xFF);
        }
        //public static float Truncate(this float value, int significantDigitsCount)
        //{
        //    if (significantDigitsCount < 0)
        //    {
        //        throw new ArgumentException("Count cannot be negative.", nameof(significantDigitsCount));
        //    }
        //    if (significantDigitsCount == 0)
        //    {
        //        throw new ArgumentException("Count cannot be zero.", nameof(significantDigitsCount));
        //    }

        //    //
        //    // Float in-memory bit representation accoring to IEEE 754 standard:
        //    //
        //    //        ____ Sign, 1 bit
        //    //       /
        //    //      /
        //    //     /    ____ Exponent, 8 bits
        //    //    /    /
        //    //   /    /               ____ Mantissa, 23 bits
        //    //  /    /               /
        //    // SEEEEEEE EMMMMMMM MMMMMMMM MMMMMMMM
        //    //

        //    int integer = BitConverter.ToInt32(BitConverter.GetBytes(value), 0);
        //    int mantissa = integer & 0x007F_FFFF;
        //    mantissa = mantissa.TruncateToSignificantDigits(significantDigitsCount);
        //    integer = (integer & unchecked((int)0xFF800000)) + mantissa;
        //    value = BitConverter.ToSingle(BitConverter.GetBytes(integer), 0);
        //    BitConverter.ToSingle(BitConverter.GetBytes(value), 0);
        //    return value;
        //}
        /// <summary>
        /// Limit the value from above.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Minimum of <paramref name="value"/> and <paramref name="max"/>.</returns>
        public static void Max(this ref float value, float max)
        {
            if (value > max)
            {
                value = max;
            }
        }
        /// <summary>
        /// Limit the value from below.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <returns>Maximum of <paramref name="value"/> and <paramref name="min"/>.</returns>
        public static void Min(this ref float value, float min)
        {
            if (value < min)
            {
                value = min;
            }
        }
        /// <summary>
        /// Limit the value.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Value limited to the interval [<paramref name="min"/>, <paramref name="max"/>].</returns>
        public static void Limit(this ref float value, float min, float max)
        {
            value.Min(min);
            value.Max(max);
        }
        /// <summary>
        /// Make the value positive.
        /// </summary>
        /// <param name="value">The value to make positive.</param>
        /// <returns>The absolute value.</returns>
        public static void Abs(this ref float value)
        {
            if (value < 0)
            {
                value = -value;
            }
        }
    }
}