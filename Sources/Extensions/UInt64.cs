﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this UInt64 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF] +
                BitsCountInByte[(value >> 16) & 0xFF] +
                BitsCountInByte[(value >> 24) & 0xFF] +
                BitsCountInByte[(value >> 32) & 0xFF] +
                BitsCountInByte[(value >> 40) & 0xFF] +
                BitsCountInByte[(value >> 48) & 0xFF] +
                BitsCountInByte[(value >> 56) & 0xFF];
        }
        public static int HighestBitIndex(this UInt64 value)
        {
            if (value < 0)
                return 63;
            if (value == 0)
                return 0;

            if (value < 1UL << 32)
                if (value < 1UL << 16)
                    if (value < 1UL << 8)
                        if (value < 1UL << 4)
                            if (value < 1UL << 2)
                                if (value < 1UL << 1) return  0;
                                else return  1;
                            else
                                if (value < 1UL << 3) return  2;
                                else return  3;
                        else
                            if (value < 1UL << 6)
                                if (value < 1UL << 5) return  4;
                                else return  5;
                            else
                                if (value < 1UL << 7) return  6;
                                else return  7;
                    else
                        if (value < 1UL << 12)
                            if (value < 1UL << 10)
                                if (value < 1UL << 9) return  8;
                                else return  9;
                            else
                                if (value < 1UL << 11) return  10;
                                else return  11;
                        else
                            if (value < 1UL << 14)
                                if (value < 1UL << 13) return  12;
                                else return  13;
                            else
                                if (value < 1UL << 15) return  14;
                                else return  15;
                else
                    if (value < 1UL << 24)
                        if (value < 1UL << 20)
                            if (value < 1UL << 18)
                                if (value < 1UL << 17) return  16;
                                else return  17;
                            else
                                if (value < 1UL << 19) return  18;
                                else return  19;
                        else
                            if (value < 1UL << 22)
                                if (value < 1UL << 21) return  20;
                                else return  21;
                            else
                                if (value < 1UL << 23) return  22;
                                else return  23;
                    else
                        if (value < 1UL << 28)
                            if (value < 1UL << 26)
                                if (value < 1UL << 25) return  24;
                                else return  25;
                            else
                                if (value < 1UL << 27) return  26;
                                else return  27;
                        else
                            if (value < 1UL << 30)
                                if (value < 1UL << 29) return  28;
                                else return  29;
                            else
                                if (value < 1UL << 31) return  30;
                                else return  31;
            else
                if (value < 1UL << 48)
                    if (value < 1UL << 40)
                        if (value < 1UL << 36)
                            if (value < 1UL << 34)
                                if (value < 1UL << 33) return  32;
                                else return  33;
                            else
                                if (value < 1UL << 35) return  34;
                                else return  35;
                        else
                            if (value < 1UL << 38)
                                if (value < 1UL << 37) return  36;
                                else return  37;
                            else
                                if (value < 1UL << 39) return  38;
                                else return  39;
                    else
                        if (value < 1UL << 44)
                            if (value < 1UL << 42)
                                if (value < 1UL << 41) return  40;
                                else return  41;
                            else
                                if (value < 1UL << 43) return  42;
                                else return  43;
                        else
                            if (value < 1UL << 46)
                                if (value < 1UL << 45) return  44;
                                else return  45;
                            else
                                if (value < 1UL << 47) return  46;
                                else return  47;
                else
                    if (value < 1UL << 56)
                        if (value < 1UL << 52)
                            if (value < 1UL << 50)
                                if (value < 1UL << 49) return  48;
                                else return  49;
                            else
                                if (value < 1UL << 51) return  50;
                                else return  51;
                        else
                            if (value < 1UL << 54)
                                if (value < 1UL << 53) return  52;
                                else return  53;
                            else
                                if (value < 1UL << 55) return  54;
                                else return  55;
                    else
                        if (value < 1UL << 60)
                            if (value < 1UL << 58)
                                if (value < 1UL << 57) return  56;
                                else return  57;
                            else
                                if (value < 1UL << 59) return  58;
                                else return  59;
                        else
                            if (value < 1UL << 62)
                                if (value < 1UL << 61) return  60;
                                else return  61;
                            else
                                if (value == 1 << 62) return  62;
                                else return  63;

            throw new Exception();
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static ulong Reverse(this ulong value)
        {
            return (ulong)(
                (((ulong)ReverseTable[value       & 0xFF]) << 56) |
                (((ulong)ReverseTable[value >>  8 & 0xFF]) << 48) |
                (((ulong)ReverseTable[value >> 16 & 0xFF]) << 40) |
                (((ulong)ReverseTable[value >> 24 & 0xFF]) << 32) |
                (((ulong)ReverseTable[value >> 32 & 0xFF]) << 24) |
                (((ulong)ReverseTable[value >> 40 & 0xFF]) << 16) |
                (((ulong)ReverseTable[value >> 48 & 0xFF]) <<  8) |
                (((ulong)ReverseTable[value >> 56       ])));
        }
    }
}
