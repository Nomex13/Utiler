﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this UInt32 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF] +
                BitsCountInByte[(value >> 16) & 0xFF] +
                BitsCountInByte[(value >> 24) & 0xFF];
        }
        public static int HighestBitIndex(this UInt32 value)
        {
            if (value < 0)
                return 31;
            if (value == 0)
                return 0;

            if (value < 1 << 32)
                if (value < 1 << 16)
                    if (value < 1 << 8)
                        if (value < 1 << 4)
                            if (value < 1 << 2)
                                if (value < 1 << 1) return  0;
                                else return  1;
                            else
                                if (value < 1 << 3) return  2;
                                else return  3;
                        else
                            if (value < 1 << 6)
                                if (value < 1 << 5) return  4;
                                else return  5;
                            else
                                if (value < 1 << 7) return  6;
                                else return  7;
                    else
                        if (value < 1 << 12)
                            if (value < 1 << 10)
                                if (value < 1 << 9) return  8;
                                else return  9;
                            else
                                if (value < 1 << 11) return  10;
                                else return  11;
                        else
                            if (value < 1 << 14)
                                if (value < 1 << 13) return  12;
                                else return  13;
                            else
                                if (value < 1 << 15) return  14;
                                else return  15;
                else
                    if (value < 1 << 24)
                        if (value < 1 << 20)
                            if (value < 1 << 18)
                                if (value < 1 << 17) return  16;
                                else return  17;
                            else
                                if (value < 1 << 19) return  18;
                                else return  19;
                        else
                            if (value < 1 << 22)
                                if (value < 1 << 21) return  20;
                                else return  21;
                            else
                                if (value < 1 << 23) return  22;
                                else return  23;
                    else
                        if (value < 1 << 28)
                            if (value < 1 << 26)
                                if (value < 1 << 25) return  24;
                                else return  25;
                            else
                                if (value < 1 << 27) return  26;
                                else return  27;
                        else
                            if (value < 1 << 30)
                                if (value < 1 << 29) return  28;
                                else return  29;
                            else
                                if (value == 1 << 30) return  30;
                                else return  31;

            throw new Exception();
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static uint Reverse(this uint value)
        {
            return (uint)(
                (((uint)ReverseTable[value       & 0xFF]) << 24) |
                (((uint)ReverseTable[value >>  8 & 0xFF]) << 16) |
                (((uint)ReverseTable[value >> 16 & 0xFF]) <<  8) |
                (((uint)ReverseTable[value >> 24       ])));
        }
    }
}
