﻿using System;
using System.Diagnostics;
using System.Text;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        /// <summary>
        /// Get formatted time.
        /// </summary>
        /// <param name="timeSpan">The time.</param>
        /// <param name="compact">Short format, like: 1h 4m, 4m 5s, 8.12s, 9.777ms instead of long format, like: 1 hour and 4 minutes, 4 minutes and 5 seconds, 8 seconds and 12 milliseconds, 9 milliseconds and 777 nanoseconds.</param>
        public static string ToString(this TimeSpan timeSpan, bool compact)
        {
            if (timeSpan.Ticks == 0)
            {
                return "0";
            }

            StringBuilder stringBuilder = new StringBuilder();

            // Short format, like: 1h 4m, 4m 5s, 8.12s, 9.777ms
            if (compact)
            {
                if (timeSpan.Days != 0)
                {
                    stringBuilder.Append($"{timeSpan.Days}d {timeSpan.Hours}h");
                }
                else if (timeSpan.Hours != 0)
                {
                    stringBuilder.Append($"{timeSpan.Hours}h {timeSpan.Minutes}m");
                }
                else if (timeSpan.Minutes != 0)
                {
                    stringBuilder.Append($"{timeSpan.Minutes}m {timeSpan.Seconds}s");
                }
                else if (timeSpan.Seconds != 0)
                {
                    stringBuilder.Append($"{timeSpan.Seconds}.{timeSpan.Milliseconds}s");
                }
                else if (timeSpan.Milliseconds != 0)
                {
                    if (Stopwatch.IsHighResolution)
                    {
                        int nanoseconds = (int)((timeSpan.Ticks / 10) % 1000);
                        stringBuilder.Append($"{timeSpan.Milliseconds}.{nanoseconds}ms");
                    }
                    else
                    {
                        stringBuilder.Append($"{timeSpan.Milliseconds}ms");
                    }
                }
                else
                {
                    int nanoseconds = (int)((timeSpan.Ticks / 10) % 1000);
                    stringBuilder.Append($"{nanoseconds}ns");
                }

                return stringBuilder.ToString();
            }
            // Long format, like: 1 hour and 4 minutes, 4 minutes and 5 seconds, 8 seconds and 12 milliseconds, 9 milliseconds and 777 nanoseconds
            else
            {
                if (timeSpan.Days != 0)
                {
                    stringBuilder.Append($"{timeSpan.Days} day{(timeSpan.Days == 1 ? "" : "s")} and {timeSpan.Hours} hour{(timeSpan.Hours == 1 ? "" : "s")}");
                }
                else if (timeSpan.Hours != 0)
                {
                    stringBuilder.Append($"{timeSpan.Hours} hour{(timeSpan.Hours == 1 ? "" : "s")} and {timeSpan.Minutes} minute{(timeSpan.Minutes == 1 ? "" : "s")}");
                }
                else if (timeSpan.Minutes != 0)
                {
                    stringBuilder.Append($"{timeSpan.Minutes} minute{(timeSpan.Minutes == 1 ? "" : "s")} and {timeSpan.Seconds} second{(timeSpan.Seconds == 1 ? "" : "s")}");
                }
                else if (timeSpan.Seconds != 0)
                {
                    stringBuilder.Append($"{timeSpan.Seconds} second{(timeSpan.Seconds == 1 ? "" : "s")} and {timeSpan.Milliseconds} millisecond{(timeSpan.Milliseconds == 1 ? "" : "s")}");
                }
                else if (timeSpan.Milliseconds != 0)
                {
                    if (Stopwatch.IsHighResolution)
                    {
                        int nanoseconds = (int)((timeSpan.Ticks / 10) % 1000);
                        stringBuilder.Append($"{timeSpan.Milliseconds} millisecond{(timeSpan.Milliseconds == 1 ? "" : "s")} and {nanoseconds} nanosecond{(nanoseconds == 1 ? "" : "s")}");
                    }
                    else
                    {
                        stringBuilder.Append($"{timeSpan.Milliseconds} millisecond{(timeSpan.Milliseconds == 1 ? "" : "s")}");
                    }
                }
                else
                {
                    int nanoseconds = (int)((timeSpan.Ticks / 10) % 1000);
                    stringBuilder.Append($"{nanoseconds} nanosecond{(nanoseconds == 1 ? "" : "s")}");
                }

                return stringBuilder.ToString();
            }
        }
    }
}