﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static List<T> Slice<T>(this List<T> list, int index, int length = -1)
        {
            length = length < 0 ? list.Count - index : length;
            return list.GetRange(index, length);
        }
        public static List<T> Remove<T>(this List<T> list, Func<T, bool> comparer)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (comparer(list[i]))
                {
                    list.RemoveAt(i);
                }
            }
            return list;
        }
        /// <summary>
        /// Remove all elements after the specified index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index">The index.</param>
        public static void RemoveAfter<T>(this List<T> list, int index)
        {
            if (list.Count == 0)
            {
                return;
            }
            list.RemoveRange(index + 1, list.Count - index);
        }
        /// <summary>
        /// Remove all elements before the specified index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index">The index.</param>
        public static void RemoveBefore<T>(this List<T> list, int index)
        {
            if (list.Count == 0)
            {
                return;
            }
            list.RemoveRange(0, index);
        }
        /// <summary>
        /// Remove the first element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static void RemoveFirst<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return;
            }
            list.RemoveAt(0);
        }
        /// <summary>
        /// Remove the last element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        public static void RemoveLast<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return;
            }
            list.RemoveAt(list.Count - 1);
        }
    }
}