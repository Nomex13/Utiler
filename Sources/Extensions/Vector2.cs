﻿#if UNITY_ENGINE

using UnityEngine;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static Vector2 Invert(this Vector2 vector)
        {
            return new Vector2(1f / vector.x, 1f / vector.y);
        }
        public static Vector2 SetX(this Vector2 vector, float x)
        {
            return new Vector2(x, vector.y);
        }
        public static Vector2 SetY(this Vector2 vector, float y)
        {
            return new Vector2(vector.x, y);
        }
        public static Vector3 ToXYz(this Vector2 vector, float z)
        {
            return new Vector3(vector.x, vector.y, z);
        }
        public static Vector3 ToXyY(this Vector2 vector, float y)
        {
            return new Vector3(vector.x, y, vector.y);
        }
        public static Vector3 ToxXY(this Vector2 vector, float x)
        {
            return new Vector3(x, vector.x, vector.y);
        }
    }
}

#endif