﻿using System;
using System.Text;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Repeat specified string specified number of times.
        /// </summary>
        /// <param name="count">Times to repeat.</param>
        /// <param name="string">String to repeat.</param>
        /// <returns>Repeated string.</returns>
        public static string Times(this int count, string @string)
        {
            if (count <= 0)
            {
                return "";
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < count; i++)
            {
                stringBuilder.Append(@string);
            }
            return stringBuilder.ToString();
        }
        /// <summary>
        /// Invoke the action specified amount of times.
        /// </summary>
        /// <param name="count">Times to invoke the provided action.</param>
        /// <param name="action">The action to invoke.</param>
        public static void Times(this int count, Action action)
        {
            for (int i = 0; i < count; i++)
            {
                action();
            }
        }
        /// <summary>
        /// Invoke the action specified amount of times.
        /// </summary>
        /// <param name="count">Times to invoke the provided action.</param>
        /// <param name="action">The action to invoke. The argument passed is the zero-based iteration number.</param>
        public static void Times(this int count, Action<int> action)
        {
            for (int i = 0; i < count; i++)
            {
                action(i);
            }
        }
        private static Int32[] Int32SignificantDigitsPowers = new Int32[]
        {
            1,
            10,
            100,
            1000,
            10000,
            100000,
            1000000,
            10000000,
            100000000,
            1000000000,
        };
        private static Int32[] Int32SignificantDigitsPowersHalved = new Int32[]
        {
            1,
            5,
            50,
            500,
            5000,
            50000,
            500000,
            5000000,
            50000000,
            500000000,
        };
        /// <summary>
        /// Round the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to round.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value rounded to specified count of significant digits.</returns>
        public static int RoundToSignificantDigits(this int value, int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }
            if (count == 0)
            {
                throw new ArgumentException("Count cannot be zero.", nameof(count));
            }

            if (value == 0)
            {
                return value;
            }
            // If count is more than integer can actually hold -- return it unmodified
            if (count >= Int32SignificantDigitsPowers.Length)
            {
                return value;
            }
            int countToCut = value.CountDigits() - count;
            if (countToCut <= 0)
            {
                return value;
            }

            int rest = value % Int32SignificantDigitsPowers[countToCut];
            // Rounding is needed
            if (Math.Abs(rest) >= Int32SignificantDigitsPowersHalved[countToCut])
            {
                int valueCandidate = value - rest + Math.Sign(value) * Int32SignificantDigitsPowers[countToCut];
                // Sign has changed, an overflow has occured
                if ((valueCandidate ^ value) < 0)
                {
                    // Return truncated value instead of rounded
                    return value - rest;
                }
                // Sign did not change
                else
                {
                    return valueCandidate;
                }
            }
            // Truncation is enough
            else
            {
                return value - rest;
            }
        }
        /// <summary>
        /// Truncate the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to truncate.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value truncated to specified count of significant digits.</returns>
        public static int TruncateToSignificantDigits(this int value, int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }
            if (count == 0)
            {
                throw new ArgumentException("Count cannot be zero.", nameof(count));
            }
            // If count is more than integer can actually hold -- return it unmodified
            if (count >= Int32SignificantDigitsPowers.Length)
            {
                return value;
            }
            int countToCut = value.CountDigits() - count;
            if (countToCut < 0)
            {
                return value;
            }
            return value - value % Int32SignificantDigitsPowers[countToCut];
        }
        /// <summary>
        /// Count decimal digits.
        /// </summary>
        /// <param name="value">Value to count its decimal digits.</param>
        /// <returns>Count of decimal digits in the specifed value.</returns>
        public static int CountDigits(this Int32 value)
        {
            // Int32.MinValue should be checked separately because Int32.MinValue == -Int32.MinValue == -2_147_483_648 == 0x80000000.
            // Also Math.Abs(Int32.MinValue) throws OverflowException.
            if (value == Int32.MinValue) return 10;

            if (value < 0) value = -value;

            if (value < 10_000)
                if (value < 100)
                    if (value < 10)            return 1;
                    else                       return 2;
                else
                    if (value < 1000)          return 3;
                    else                       return 4;
            else
                if (value < 1_000_000)
                    if (value < 100_000)       return 5;
                    else                       return 6;
                else if (value < 100_000_000)
                    if (value < 10_000_000)    return 7;
                    else                       return 8;
                else
                    if (value < 1_000_000_000) return 9;
                    else                       return 10;

            //
            // Sequential algorithm is faster only for -99..99 numbers
            // and is kept here for historical purpose only:
            //
            //if (value < 10) return 1;
            //if (value < 100) return 2;
            //if (value < 1000) return 3;
            //if (value < 10000) return 4;
            //if (value < 100000) return 5;
            //if (value < 1000000) return 6;
            //if (value < 10000000) return 7;
            //if (value < 100000000) return 8;
            //if (value < 1000000000) return 9;
            //return 10;
            //
        }
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this Int32 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF] +
                BitsCountInByte[(value >> 16) & 0xFF] +
                BitsCountInByte[(value >> 24) & 0xFF];
        }
        public static int HighestBitIndex(this Int32 value)
        {
            if (value < 0)
                return 31;
            if (value == 0)
                return 0;

            if (value < 1 << 16)
                if (value < 1 << 8)
                    if (value < 1 << 4)
                        if (value < 1 << 2)
                            if (value < 1 << 1) return  0;
                            else return  1;
                        else
                            if (value < 1 << 3) return  2;
                            else return  3;
                    else
                        if (value < 1 << 6)
                            if (value < 1 << 5) return  4;
                            else return  5;
                        else
                            if (value < 1 << 7) return  6;
                            else return  7;
                else
                    if (value < 1 << 12)
                        if (value < 1 << 10)
                            if (value < 1 << 9) return  8;
                            else return  9;
                        else
                            if (value < 1 << 11) return  10;
                            else return  11;
                    else
                        if (value < 1 << 14)
                            if (value < 1 << 13) return  12;
                            else return  13;
                        else
                            if (value < 1 << 15) return  14;
                            else return  15;
            else
                if (value < 1 << 24)
                    if (value < 1 << 20)
                        if (value < 1 << 18)
                            if (value < 1 << 17) return  16;
                            else return  17;
                        else
                            if (value < 1 << 19) return  18;
                            else return  19;
                    else
                        if (value < 1 << 22)
                            if (value < 1 << 21) return  20;
                            else return  21;
                        else
                            if (value < 1 << 23) return  22;
                            else return  23;
                else
                    if (value < 1 << 28)
                        if (value < 1 << 26)
                            if (value < 1 << 25) return  24;
                            else return  25;
                        else
                            if (value < 1 << 27) return  26;
                            else return  27;
                    else
                        if (value < 1 << 30)
                            if (value < 1 << 29) return  28;
                            else return  29;
                        else
                            if (value == 1 << 30) return  30;
                            else return  31;

            throw new Exception();
        }
        private static string[][] RomanDigits = new string[][]
        {
            new string[]
            {
                "",
                "I",
                "II",
                "III",
                "IV",
                "V",
                "VI",
                "VII",
                "VIII",
                "IX",
            },
            new string[]
            {
                "",
                "X",
                "XX",
                "XXX",
                "XL",
                "L",
                "LX",
                "LXX",
                "LXXX",
                "XC",
            },
            new string[]
            {
                "",
                "C",
                "CC",
                "CCC",
                "CD",
                "D",
                "DC",
                "DCC",
                "DCCC",
                "CM",
            },
            new string[]
            {
                "",
                "M",
                "MM",
                "MMM",
            }
        };
        /// <summary>
        /// Convert to Roman representation.
        /// </summary>
        /// <param name="integer">The number to convert.</param>
        /// <returns>Roman representation of the number.</returns>
        public static string ToRoman(this int integer)
        {
            if (integer == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(integer), "Roman digits cannot represent zero value.");
            }
            if (integer < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(integer), "Roman digits cannot represent negative values.");
            }
            if (integer > 3999)
            {
                throw new ArgumentOutOfRangeException(nameof(integer), "Roman digits cannot represent values greater than 3999.");
            }

            int[] digits = new int[4];
            for (int i = 0; i <= 3; i++)
            {
                digits[i] = integer % 10;
                integer = integer / 10;
            }

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 3; i >= 0; i--)
            {
                stringBuilder.Append(RomanDigits[i][digits[i]]);
            }
            return stringBuilder.ToString();
        }
        /// <summary>
        /// Limit the value from above.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Minimum of <paramref name="value"/> and <paramref name="max"/>.</returns>
        public static void Max(this ref int value, int max)
        {
            if (value > max)
            {
                value = max;
            }
        }
        /// <summary>
        /// Limit the value from below.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <returns>Maximum of <paramref name="value"/> and <paramref name="min"/>.</returns>
        public static void Min(this ref int value, int min)
        {
            if (value < min)
            {
                value = min;
            }
        }
        /// <summary>
        /// Limit the value.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Value limited to the interval [<paramref name="min"/>, <paramref name="max"/>].</returns>
        public static void Limit(this ref int value, int min, int max)
        {
            value.Min(min);
            value.Max(max);
        }
        /// <summary>
        /// Make the value positive.
        /// </summary>
        /// <param name="value">The value to make positive.</param>
        /// <returns>The absolute value.</returns>
        public static void Abs(this ref int value)
        {
            if (value < 0)
            {
                value = -value;
            }
        }
        /// <summary>
        /// Loop the value in the [0, <paramref name="threshold"/>) range for positiive <paramref name="threshold"/>
        /// or in the (<paramref name="threshold"/>, 0] range for negative one.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="threshold">The threshold to limit the value.</param>
        public static void Loop(this ref int value, int threshold)
        {
            if (threshold == 0)
            {
                value = 0;
            }

            if (threshold > 0)
            {
                value.Loop(0, threshold);
            }
            else
            {
                value.Loop(threshold, 0);

                // Change the behaviour for the border value to fit the positive threshold behaviour.
                if (value == threshold)
                {
                    value = 0;
                }
            }
        }
        /// <summary>
        /// Loop the value in the [<paramref name="min"/>, <paramref name="max"/>) range.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="min">The minimum threshold to limit the value.</param>
        /// <param name="max">The maximum threshold to limit the value.</param>
        public static void Loop(this ref int value, int min, int max)
        {
            if (min == max)
            {
                value = min;
                return;
            }

            // Fix the reverse
            if (min > max)
            {
                Utiler.Swap(ref min, ref max);
            }

            int length = max - min;

            // Go to relative coordinates
            value -= min;
            if (value < 0)
            {
                value = length - value;
            }

            // Clamp
            value %= length;

            // Go back to absolute coordinates
            value += min;
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static int Reverse(this int value)
        {
            return (int)(
                (((int)ReverseTable[value       & 0xFF]) << 24) |
                (((int)ReverseTable[value >> 8  & 0xFF]) << 16) |
                (((int)ReverseTable[value >> 16 & 0xFF]) <<  8) |
                (((int)ReverseTable[value >> 24 & 0xFF])      ));
        }
    }
}