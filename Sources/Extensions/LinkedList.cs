﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static void RemoveBefore<T>(this LinkedList<T> list, LinkedListNode<T> node)
        {
            while (node.Previous != null)
            {
                list.Remove(node.Previous);
            }
        }
        public static void RemoveAfter<T>(this LinkedList<T> list, LinkedListNode<T> node)
        {
            while (node.Next != null)
            {
                list.Remove(node.Next);
            }
        }
    }
}
