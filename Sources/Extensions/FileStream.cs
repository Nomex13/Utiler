﻿using System.IO;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static int Read(this FileStream fileStream, byte[] buffer, long position, long offset)
        {
            int index = 0;

            // Save the position to set it back when finished
            long positionInitial = fileStream.Position;

            // Read till offset or file runs out
            fileStream.Position = position;
            for (; index < offset; index++)
            {
                int @byte = fileStream.ReadByte();
                if (@byte < 0)
                {
                    break;
                }
                buffer[index] = (byte)@byte;
            }

            // Revert the position
            fileStream.Position = positionInitial;

            return index;
        }
        public static void Write(this FileStream fileStream, byte[] buffer, long position, long offset)
        {
            // Save the position to set it back when finished
            long positionInitial = fileStream.Position;

            // Write till offset or buffer runs out
            fileStream.Position = position;
            for (int index = 0; index < offset; index++)
            {
                fileStream.WriteByte(buffer[index]);
            }

            // Revert the position
            fileStream.Position = positionInitial;
        }
    }
}