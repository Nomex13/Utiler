﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Compiles a property getter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <param name="propertyInfo">Property info.</param>
        /// <returns>Property getter.</returns>
        public static Func<TObject, dynamic> CompileGetter<TObject>(this PropertyInfo propertyInfo)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject), "value");
            Expression expression = Expression.Property(parameterObject, propertyInfo.Name);
            Func<TObject, dynamic> getter = Expression.Lambda<Func<TObject, dynamic>>(expression, parameterObject).Compile();

            return getter;
        }
        /// <summary>
        /// Compiles a property setter.
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propertyInfo">Property info.</param>
        /// <returns>Property setter.</returns>
        public static Action<TObject, dynamic> CompileSetter<TObject>(this PropertyInfo propertyInfo)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject));
            ParameterExpression parameterPropertyValue = Expression.Parameter(propertyInfo.PropertyType, propertyInfo.Name);
            MemberExpression expression = Expression.Property(parameterObject, propertyInfo.Name);
            Action<TObject, dynamic> setter = Expression.Lambda<Action<TObject, dynamic>>
            (
                Expression.Assign(expression, parameterPropertyValue), parameterObject, parameterPropertyValue
            ).Compile();

            return setter;
        }
    }
}
