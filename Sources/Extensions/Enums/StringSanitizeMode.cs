﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    [Flags]
    public enum StringSanitizeMode
    {
        KEEP_LOWER = 1,
        KEEP_UPPER = 2,
        KEEP_DIGITS = 4,
        KEEP_SPACES = 8,
    }
}
