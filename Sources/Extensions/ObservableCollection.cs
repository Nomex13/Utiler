﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Adds objects to the end of the <see cref="System.Collections.ObjectModel.Collection{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items">The object to be added to the end of the <see cref="System.Collections.ObjectModel.Collection{T}"/>.</param>
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                collection.Add(item);
            }
        }
        /// <summary>
        /// Removes the first occurrence of the specific objects from the <see cref="System.Collections.ObjectModel.Collection{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="items">The objects to remove from the <see cref="System.Collections.ObjectModel.Collection{T}"/>.</param>
        public static void RemoveRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                collection.Remove(item);
            }
        }
        //public static void Sort<T>(this ObservableCollection<T> param_collection, Comparison<T> param_comparison)
        //{
        //    List<T> list = param_collection.ToList();
        //    list.Sort(param_comparison);
        //    param_collection.Clear();
        //    foreach (T t in list)
        //    {
        //        param_collection.Ins;
        //    }
        //}
        //public static void Sort<T>(this ObservableCollection<T> param_collection, IComparer<T> param_comparer)
        //{
        //    List<T> list = param_collection.ToList();
        //    list.Sort(param_comparer);
        //    param_collection.Clear();
        //    foreach (T t in list)
        //    {
        //        param_collection.Add(t);
        //    }
        //}
    }
}