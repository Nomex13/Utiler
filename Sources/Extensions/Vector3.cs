﻿#if UNITY_ENGINE

using UnityEngine;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static Vector3 Invert(this Vector3 vector)
        {
            return new Vector3(1f / vector.x, 1f / vector.y, 1f / vector.z);
        }
        public static Vector3 SetX(this Vector3 vector, float x)
        {
            return new Vector3(x, vector.y, vector.z);
        }
        public static Vector3 SetY(this Vector3 vector, float y)
        {
            return new Vector3(vector.x, y, vector.z);
        }
        public static Vector3 SetZ(this Vector3 vector, float z)
        {
            return new Vector3(vector.x, vector.y, z);
        }
        public static Vector3 SetXY(this Vector3 vector, float x, float y)
        {
            return new Vector3(x, y, vector.z);
        }
        public static Vector3 SetXZ(this Vector3 vector, float x, float z)
        {
            return new Vector3(x, vector.y, z);
        }
        public static Vector3 SetYZ(this Vector3 vector, float y, float z)
        {
            return new Vector3(vector.x, y, z);
        }
        public static Vector3 ToXY(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.y);
        }
        public static Vector3 ToXZ(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.z);
        }
        public static Vector3 ToYZ(this Vector3 vector)
        {
            return new Vector2(vector.y, vector.z);
        }
    }
}

#endif