﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        private static Int64[] Int64SignificantDigitsPowers = new Int64[]
        {
            1,
            10,
            100,
            1000,
            10000,
            100000,
            1000000,
            10000000,
            100000000,
            1000000000,
            10000000000,
            100000000000,
            1000000000000,
            10000000000000,
            100000000000000,
            1000000000000000,
            10000000000000000,
            100000000000000000,
            1000000000000000000,
        };
        private static Int64[] Int64SignificantDigitsPowersHalved = new Int64[]
        {
            1,
            5,
            50,
            500,
            5000,
            50000,
            500000,
            5000000,
            50000000,
            500000000,
            5000000000,
            50000000000,
            500000000000,
            5000000000000,
            50000000000000,
            500000000000000,
            5000000000000000,
            50000000000000000,
            500000000000000000,
        };
        /// <summary>
        /// Round the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to round.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value rounded to specified count of significant digits.</returns>
        public static Int64 RoundToSignificantDigits(this Int64 value, int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }
            if (count == 0)
            {
                throw new ArgumentException("Count cannot be zero.", nameof(count));
            }
            // If count is more than integer can actually hold -- return it unmodified
            if (count >= Int64SignificantDigitsPowers.Length)
            {
                return value;
            }
            int countToCut = value.CountDigits() - count;
            if (countToCut < 0)
            {
                return value;
            }
            Int64 rest = value % Int64SignificantDigitsPowers[countToCut];
            // Rounding is needed
            if (Math.Abs(rest) >= Int64SignificantDigitsPowersHalved[countToCut])
            {
                Int64 valueCandidate = value - rest + Math.Sign(value) * Int64SignificantDigitsPowers[countToCut];
                // Sign has changed, an overflow has occured
                if ((valueCandidate ^ value) < 0)
                {
                    // Return truncated value instead of rounded
                    return value - rest;
                }
                // Sign did not change
                else
                {
                    return valueCandidate;
                }
            }
            // Truncation is enough
            else
            {
                return value - rest;
            }
        }
        /// <summary>
        /// Truncate the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to truncate.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value truncated to specified count of significant digits.</returns>
        public static Int64 TruncateToSignificantDigits(this Int64 value, int count)
        {
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }
            if (count == 0)
            {
                throw new ArgumentException("Count cannot be zero.", nameof(count));
            }
            // If count is more than integer can actually hold -- return it unmodified
            if (count >= Int64SignificantDigitsPowers.Length)
            {
                return value;
            }
            int countToCut = value.CountDigits() - count;
            if (countToCut < 0)
            {
                return value;
            }
            return value - value % Int64SignificantDigitsPowers[countToCut];
        }
        /// <summary>
        /// Count decimal digits.
        /// </summary>
        /// <param name="value">Value to count its decimal digits.</param>
        /// <returns>Count of decimal digits in the specifed value.</returns>
        public static int CountDigits(this Int64 value)
        {
            // Int32.MinValue should be checked separately because Int64.MinValue == -Int64.MinValue == -9_223_372_036_854_775_808 == 0x8000000000000000.
            // Also Math.Abs(Int64.MinValue) throws OverflowException.
            if (value == Int64.MinValue) return 19;

            if (value < 0) value = -value;
                                                                        // Count ranges:
            if (value < 100_000_000)                                    // 1-8
                if (value < 10_000)                                     //     1-4
                    if (value < 100)                                    //         1-2
                        if (value < 10) return 1;                       //             1
                        else return 2;                                  //             2
                    else                                                //         3-4
                        if (value < 1000) return 3;                     //             3
                        else return 4;                                  //             4
                else                                                    //     5-8
                    if (value < 1_000_000)                              //         5-6
                        if (value < 100_000) return 5;                  //             5
                        else return 6;                                  //             6
                    else                                                //         7-8
                        if (value < 10_000_000) return 7;               //             7
                        else return 8;                                  //             8
            else                                                        // 9-19
                if (value < 1_000_000_000_000)                          //     9-12
                    if (value < 10_000_000_000)                         //         9-10
                        if (value < 1_000_000_000) return 9;            //             9
                        else return 10;                                 //             10
                    else                                                //         11-12
                        if (value < 100_000_000_000) return 11;         //             11
                        else return 12;                                 //             12
                else if (value < 10_000_000_000_000_000)                //     13-16
                    if (value < 100_000_000_000_000)                    //         13-14
                        if (value < 10_000_000_000_000) return 13;      //             13
                        else return 14;                                 //             14
                    else                                                //         15-16
                        if (value < 1_000_000_000_000_000) return 15;   //             15
                        else return 16;                                 //             16
                else                                                    //     17-19
                    if (value < 1_000_000_000_000_000_000)              //         17-18
                        if (value < 100_000_000_000_000_000) return 17; //             17
                        else return 18;                                 //             18
                    else return 19;                                     //         19

            //
            // Sequential algorithm is faster only for -999..999 numbers
            // and is kept here for historical purpose only:
            //
            //if (value < 10) return 1;
            //if (value < 100) return 2;
            //if (value < 1000) return 3;
            //if (value < 10000) return 4;
            //if (value < 100000) return 5;
            //if (value < 1000000) return 6;
            //if (value < 10000000) return 7;
            //if (value < 100000000) return 8;
            //if (value < 1000000000) return 9;
            //if (value < 10000000000) return 10;
            //if (value < 100000000000) return 11;
            //if (value < 1000000000000) return 12;
            //if (value < 10000000000000) return 13;
            //if (value < 100000000000000) return 14;
            //if (value < 1000000000000000) return 15;
            //if (value < 10000000000000000) return 16;
            //if (value < 100000000000000000) return 17;
            //if (value < 1000000000000000000) return 18;
            //return 19;
        }
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this Int64 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF] +
                BitsCountInByte[(value >> 16) & 0xFF] +
                BitsCountInByte[(value >> 24) & 0xFF] +
                BitsCountInByte[(value >> 32) & 0xFF] +
                BitsCountInByte[(value >> 40) & 0xFF] +
                BitsCountInByte[(value >> 48) & 0xFF] +
                BitsCountInByte[(value >> 56) & 0xFF]                ;
        }
        public static int HighestBitIndex(this Int64 value)
        {
            if (value < 0)
                return 63;
            if (value == 0)
                return 0;

            if (value < 1 << 32)
                if (value < 1 << 16)
                    if (value < 1 << 8)
                        if (value < 1 << 4)
                            if (value < 1 << 2)
                                if (value < 1 << 1) return  0;
                                else return  1;
                            else
                                if (value < 1 << 3) return  2;
                                else return  3;
                        else
                            if (value < 1 << 6)
                                if (value < 1 << 5) return  4;
                                else return  5;
                            else
                                if (value < 1 << 7) return  6;
                                else return  7;
                    else
                        if (value < 1 << 12)
                            if (value < 1 << 10)
                                if (value < 1 << 9) return  8;
                                else return  9;
                            else
                                if (value < 1 << 11) return  10;
                                else return  11;
                        else
                            if (value < 1 << 14)
                                if (value < 1 << 13) return  12;
                                else return  13;
                            else
                                if (value < 1 << 15) return  14;
                                else return  15;
                else
                    if (value < 1 << 24)
                        if (value < 1 << 20)
                            if (value < 1 << 18)
                                if (value < 1 << 17) return  16;
                                else return  17;
                            else
                                if (value < 1 << 19) return  18;
                                else return  19;
                        else
                            if (value < 1 << 22)
                                if (value < 1 << 21) return  20;
                                else return  21;
                            else
                                if (value < 1 << 23) return  22;
                                else return  23;
                    else
                        if (value < 1 << 28)
                            if (value < 1 << 26)
                                if (value < 1 << 25) return  24;
                                else return  25;
                            else
                                if (value < 1 << 27) return  26;
                                else return  27;
                        else
                            if (value < 1 << 30)
                                if (value < 1 << 29) return  28;
                                else return  29;
                            else
                                if (value < 1 << 31) return  30;
                                else return  31;
            else
                if (value < 1 << 48)
                    if (value < 1 << 40)
                        if (value < 1 << 36)
                            if (value < 1 << 34)
                                if (value < 1 << 33) return  32;
                                else return  33;
                            else
                                if (value < 1 << 35) return  34;
                                else return  35;
                        else
                            if (value < 1 << 38)
                                if (value < 1 << 37) return  36;
                                else return  37;
                            else
                                if (value < 1 << 39) return  38;
                                else return  39;
                    else
                        if (value < 1 << 44)
                            if (value < 1 << 42)
                                if (value < 1 << 41) return  40;
                                else return  41;
                            else
                                if (value < 1 << 43) return  42;
                                else return  43;
                        else
                            if (value < 1 << 46)
                                if (value < 1 << 45) return  44;
                                else return  45;
                            else
                                if (value < 1 << 47) return  46;
                                else return  47;
                else
                    if (value < 1 << 56)
                        if (value < 1 << 52)
                            if (value < 1 << 50)
                                if (value < 1 << 49) return  48;
                                else return  49;
                            else
                                if (value < 1 << 51) return  50;
                                else return  51;
                        else
                            if (value < 1 << 54)
                                if (value < 1 << 53) return  52;
                                else return  53;
                            else
                                if (value < 1 << 55) return  54;
                                else return  55;
                    else
                        if (value < 1 << 60)
                            if (value < 1 << 58)
                                if (value < 1 << 57) return  56;
                                else return  57;
                            else
                                if (value < 1 << 59) return  58;
                                else return  59;
                        else
                            if (value < 1 << 62)
                                if (value < 1 << 61) return  60;
                                else return  61;
                            else
                                if (value == 1 << 62) return  62;
                                else return  63;

            throw new Exception();
        }
        /// <summary>
        /// Limit the value from above.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Minimum of <paramref name="value"/> and <paramref name="max"/>.</returns>
        public static void Max(this ref long value, long max)
        {
            if (value > max)
            {
                value = max;
            }
        }
        /// <summary>
        /// Limit the value from below.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <returns>Maximum of <paramref name="value"/> and <paramref name="min"/>.</returns>
        public static void Min(this ref long value, long min)
        {
            if (value < min)
            {
                value = min;
            }
        }
        /// <summary>
        /// Limit the value.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Value limited to the interval [<paramref name="min"/>, <paramref name="max"/>].</returns>
        public static void Limit(this ref long value, long min, long max)
        {
            value.Min(min);
            value.Max(max);
        }
        /// <summary>
        /// Make the value positive.
        /// </summary>
        /// <param name="value">The value to make positive.</param>
        /// <returns>The absolute value.</returns>
        public static void Abs(this ref long value)
        {
            if (value < 0)
            {
                value = -value;
            }
        }
        /// <summary>
        /// Loop the value in the [0, <paramref name="threshold"/>) range for positiive <paramref name="threshold"/>
        /// or in the (<paramref name="threshold"/>, 0] range for negative one.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="threshold">The threshold to limit the value.</param>
        public static void Loop(this ref long value, long threshold)
        {
            if (threshold == 0)
            {
                value = 0;
            }

            if (threshold > 0)
            {
                value.Loop(0, threshold);
            }
            else
            {
                value.Loop(threshold, 0);

                // Change the behaviour for the border value to fit the positive threshold behaviour.
                if (value == threshold)
                {
                    value = 0;
                }
            }
        }
        /// <summary>
        /// Loop the value in the [<paramref name="min"/>, <paramref name="max"/>) range.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="min">The minimum threshold to limit the value.</param>
        /// <param name="max">The maximum threshold to limit the value.</param>
        public static void Loop(this ref long value, long min, long max)
        {
            if (min == max)
            {
                value = min;
                return;
            }

            // Fix the reverse
            if (min > max)
            {
                Utiler.Swap(ref min, ref max);
            }

            long length = max - min;

            // Go to relative coordinates
            value -= min;
            if (value < 0)
            {
                value = length - value;
            }

            // Clamp
            value %= length;

            // Go back to absolute coordinates
            value += min;
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static long Reverse(this long value)
        {
            return (long)(
                (((long)ReverseTable[value       & 0xFF]) << 56) |
                (((long)ReverseTable[value >> 8  & 0xFF]) << 48) |
                (((long)ReverseTable[value >> 16 & 0xFF]) << 40) |
                (((long)ReverseTable[value >> 24 & 0xFF]) << 32) |
                (((long)ReverseTable[value >> 32 & 0xFF]) << 24) |
                (((long)ReverseTable[value >> 40 & 0xFF]) << 16) |
                (((long)ReverseTable[value >> 48 & 0xFF]) <<  8) |
                (((long)ReverseTable[value >> 56 & 0xFF])      ));
        }
    }
}
