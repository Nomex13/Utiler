﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Get the full message for the exception, including all the inner exceptions messages.
        /// </summary>
        /// <param name="exception"></param>
        public static string GetFullStackTrace(this Exception exception)
        {
            StringBuilder stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.Insert(0, "\r\n");
                stringBuilder.Insert(0, "\r\n");
                stringBuilder.Insert(0, exception.StackTrace);
                stringBuilder.Insert(0, "\r\n");
                stringBuilder.Insert(0, exception.Message);

                exception = exception.InnerException;
            }
            return stringBuilder.ToString();
        }
        /// <summary>
        /// Get the full message for the exception, including all the inner exceptions messages.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns>The full message for the exception.</returns>
        public static string GetFullMessage(this Exception exception)
        {
            // Collect all the messages
            var messages = new List<string>();
            while (exception != null)
            {
                messages.Add(exception.Message);
                exception = exception.InnerException;
            }
            // Compose the full message
            StringBuilder stringBuilder = new StringBuilder();
            for (var i = messages.Count - 1; i >= 0; i--)
            {
                stringBuilder.Append($"\r\n{messages.Count - i}. {messages[i]}");
            }
            // Append the stack trace
            stringBuilder.Append($"\r\n\r\nStack trace:\r\n{exception.StackTrace}\r\n");

            return stringBuilder.ToString();
        }
    }
}