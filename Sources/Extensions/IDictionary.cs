﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static IEnumerable<T> Select<T>(this IDictionary dictionary, Func<object, object, T> function)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }
            if (function == null)
            {
                throw new ArgumentNullException(nameof(function));
            }

            List<T> list = new List<T>(dictionary.Count);
            foreach (object key in dictionary.Keys)
            {
                list.Add(function(key, dictionary[key]));
            }
            return list;
        }
        private static bool TryGetKeys<T1, T2>(this Dictionary<T1, T2> dictionary, T2 value, out List<T1> keys)
        {
            if (dictionary == null)
            {
                throw new ArgumentNullException(nameof(dictionary));
            }

            keys = new List<T1>();
            if (value == null)
            {
                foreach (var pair in dictionary)
                {
                    if (pair.Value == null)
                    {
                        keys.Add(pair.Key);
                    }
                }
            }
            else
            {
                foreach (var pair in dictionary)
                {
                    if (value.Equals(pair.Value))
                    {
                        keys.Add(pair.Key);
                    }
                }
            }
            return keys.Count > 0;
        }
    }
}