﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Invoke specified action for every element on the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="action">Action to invoke for every element on the list.</param>
        public static void ForEach<T>(this IList<T> list, Action<T> action)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            foreach (T item in list)
            {
                action.Invoke(item);
            }
        }
        /// <summary>
        /// Invert the list.
        /// </summary>
        /// <typeparam name="T">List items type.</typeparam>
        /// <param name="list">Original list to invert.</param>
        /// <returns>A new inverted list.</returns>
        public static List<T> Invert<T>(this IList<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            List<T> listInverted = new List<T>(list.Count);
            for (int index = list.Count - 1; index >= 0; index--)
            {
                listInverted.Add(list[index]);
            }
            return listInverted;
        }
        /// <summary>
        /// Move specified item to specified index.
        /// If the item is not presented in the list it will be inserted into it.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public static void Move<T>(this IList<T> list, int index, T item)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            list.Remove(item);
            list.Insert(index, item);
        }
        /// <summary>
        /// Remove the first element in the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The removed element.</returns>
        public static T RemoveFirst<T>(this IList<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                throw new IndexOutOfRangeException("IList is empty.");
            }
            T first = list[0];
            list.RemoveAt(0);
            return first;
        }
        /// <summary>
        /// Remove the last element in the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns>The removed element.</returns>
        public static T RemoveLast<T>(this IList<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                throw new IndexOutOfRangeException("IList is empty.");
            }

            T last = list[list.Count - 1];
            list.RemoveAt(list.Count - 1);
            return last;
        }
        /// <summary>
        /// Get the element of the list at the specified index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index">The index.</param>
        /// <returns>The element at the specified index if the index falls in the correct range.
        /// First or last element if it does not, depending if the index is lower than zero or greater or equal to the list length.</returns>
        public static T ElementAtClamped<T>(this IList<T> list, int index)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                throw new IndexOutOfRangeException("IList is empty.");
            }

            if (index < 0)
            {
                return list[0];
            }
            if (index >= list.Count)
            {
                return list[list.Count - 1];
            }
            return list[index];
        }
        /// <summary>
        /// Returns the element at the specified index in the list.
        /// Index is cycled.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <param name="index">The index.</param>
        /// <returns>The element at the specified index if the index falls in the correct range.
        /// If the index exceeds that range then it is cycled.</returns>
        public static T ElementAtCycled<T>(this IList<T> list, int index)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                throw new IndexOutOfRangeException("IList is empty.");
            }

            if (index < 0)
            {
                index = list.Count - ((-index) % list.Count);
                return list[index];
            }

            index %= list.Count;
            return list[index];
        }
        //public static void Removet<T>(this IList<T> list, IEnumerable<T> items)
        //{
        //    foreach (T item in items)
        //    {
        //        for (int index = 0; index < list.Count; index++)
        //        {
        //            if ()
        //        }
        //    }
        //}
        //public static void Merge<T>(this IList list, IList<T> listToMerge, Func<T, string> toString, string eventName, bool sorted = false) where T : class
        //{
        //    Merge(list, listToMerge, toString, new [] { eventName });
        //}
        //public static void Merge<T>(this IList list, IList<T> listToMerge, Func<T, string> toString = null, string[] eventNames = null, bool sorted = false) where T : class
        //{
        //    // TODO: Implemet sorting

        //    if (listToMerge == null || listToMerge.Count == 0)
        //    {
        //        list.Clear();
        //        return;
        //    }
        //    // Remove
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        T item = ((ControlAdapter<T>)list[i]).Object;
        //        if (!listToMerge.Contains(item))
        //        {
        //            list.RemoveAt(i);
        //            i--;
        //        }
        //        //bool isInTheList = false;
        //        //for (int j = 0; j < list.Count; j++)
        //        //{
        //        //    if (item == list[j])
        //        //    {
        //        //        isInTheList = true;
        //        //        break;
        //        //    }
        //        //}
        //        //if (!isInTheList)
        //    }
        //    // Add
        //    for (int i = 0; i < listToMerge.Count; i++)
        //    {
        //        T item = listToMerge[i];
        //        bool isInTheList = false;
        //        for (int j = 0; j < list.Count; j++)
        //        {
        //            if (((ControlAdapter<T>)list[j]).Object == listToMerge[i])
        //            {
        //                isInTheList = true;
        //                break;
        //            }
        //        }
        //        if (!isInTheList)
        //        {
        //            ControlAdapter<T> adapter = new ControlAdapter<T>(item, toString, eventNames);
        //            list.Add(adapter);
        //        }
        //    }
        //}

        /// <summary>
        /// Reverse the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns>The input list.</returns>
        public static IEnumerable<T> Reverse<T>(this List<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            for (int i = 0; i < list.Count; i++)
            {
                yield return list[list.Count - 1 - i];
            }
        }
        /// <summary>
        /// Sorts the list in place.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparer"></param>
        /// <param name="ascending"></param>
        /// <returns>The input list.</returns>
        public static IList<T> Sort<T>(this List<T> list, Func<T, T, int> comparer, bool ascending = true)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            list.Sort((_item1, _item2) => ascending ? comparer(_item1, _item2) : comparer(_item2, _item1));
            return list;
        }
        /// <summary>
        /// Sorts the list in place.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="getter"></param>
        /// <param name="ascending"></param>
        /// <returns>The input list.</returns>
        public static IList<T> Sort<T>(this List<T> list, Func<T, IComparable> getter, bool ascending = true)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            list.Sort((_item1, _item2) => ascending ? getter(_item1).CompareTo(getter(_item2)) : getter(_item2).CompareTo(getter(_item1)));
            return list;
        }
        /// <summary>
        /// Return the item to the right of the specified one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="item"></param>
        /// <returns>Item to the right of the specified one or null if there are none left.</returns>
        public static T Next<T>(this IList<T> list, T item) where T : class
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                return null;
            }

            int index = list.IndexOf(item);
            if (index == list.Count - 1)
            {
                return null;
            }
            return list[index + 1];
        }
        /// <summary>
        /// Return the item to the left of the specified one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="item"></param>
        /// <returns>Item to the left of the specified one or null if there are none left.</returns>
        public static T Previous<T>(this IList<T> list, T item) where T : class
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }
            if (list.Count == 0)
            {
                return null;
            }

            int index = list.IndexOf(item);
            if (index == 0)
            {
                return null;
            }
            return list[index - 1];
        }
        /// <summary>
        /// Move an item from one index to another.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="indexSource">The index to move the item from.</param>
        /// <param name="indexDestination">The index to move the item to.</param>
        public static void Move<T>(this IList<T> list, int indexSource, int indexDestination)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            T item = list[indexSource];
            list.RemoveAt(indexSource);
            list.Insert(indexDestination <= indexSource ? indexDestination : indexDestination + 1, item);
        }
        /// <summary>
        /// Move the item to the specified index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="item">The item to move.</param>
        /// <param name="index">The index to move the item to.</param>
        public static void Move<T>(this IList<T> list, T item, int index)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            int indexSource = list.IndexOf(item);
            list.Move(indexSource, index);
        }
        /// <summary>
        /// Swap the items at specified indexes.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index1">The index to swap.</param>
        /// <param name="index2">The index to swap.</param>
        public static void Swap<T>(this IList<T> list, int index1, int index2)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            T item = list[index1];
            list[index1] = list[index2];
            list[index2] = item;
        }
    }
}