﻿using System;
using System.IO;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public static byte[] ReadAllBytes(this Stream stream)
        {
            byte[] buffer = new byte[4096];

            using (MemoryStream memoryStream = new MemoryStream(Math.Max((int)stream.Length, buffer.Length)))
            {
                int count;
                while ((count = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    memoryStream.Write(buffer, 0, count);
                }
                return memoryStream.ToArray();
            }
        }
    }
}
