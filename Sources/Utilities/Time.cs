﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
		/// <summary>
		/// Returns current date and time in YYYYMMDDhhmmss format.
		/// </summary>
		/// <returns>DateTime.Now in YYYYMMDDHHmmss format.</returns>
        public static string UnixTimeStamp()
        {
            return String.Format("{0:D4}{1:D2}{2:D2}{3:D2}{4:D2}{5:D2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
		/// <summary>
		/// Seconds from the beginning of the epoch.
		/// </summary>
        public static long TimeInSeconds
        {
            get
            {
                return DateTime.Now.Ticks / 10000000;
            }
        }
		/// <summary>
		/// Milliseconds from the beginning of the epoch.
		/// </summary>
        public static long TimeInMilliseconds
        {
            get
            {
                return DateTime.Now.Ticks / 10000;
            }
        }
		/// <summary>
		/// Nanoseconds from the beginning of the epoch.
		/// </summary>
        public static long TimeInNanoseconds
        {
            get
            {
                return DateTime.Now.Ticks / 10;
            }
        }
        private static long StartTimeInTicks = DateTime.Now.Ticks;
		/// <summary>
		/// Seconds from the start of the program.
		/// </summary>
        public static long RunTimeInSeconds
        {
            get
            {
                return (DateTime.Now.Ticks - StartTimeInTicks) / 10000000;
            }
        }
		/// <summary>
		/// Milliseconds from the start of the program.
		/// </summary>
        public static long RunTimeInMilliseconds
        {
            get
            {
                return (DateTime.Now.Ticks - StartTimeInTicks) / 10000;
            }
        }
		/// <summary>
		/// Nanoseconds from the start of the program.
		/// </summary>
        public static long RunTimeInNanoseconds
        {
            get
            {
                return (DateTime.Now.Ticks - StartTimeInTicks) / 10;
            }
        }
    }
}
