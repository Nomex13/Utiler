﻿using System;
using System.Linq;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
	    /// <summary>
	    /// Creates a new array and sequentially copies all arrays into it.
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <param name="arrays"></param>
	    /// <exception cref="ArgumentNullException"></exception>
	    /// <returns>New merged array.</returns>
	    public static T[] ArrayMerge<T>(params T[][] arrays)
        {
	        if (arrays == null)
	        {
		        throw new ArgumentNullException(nameof(arrays));
	        }

            T[] arrayMerged = new T[arrays.Select(_array => _array.Length).Sum()];
			int index = 0;
			foreach (T[] array in arrays)
			{
				Array.Copy(array, 0, arrayMerged, index, array.Length);
				index += array.Length;
			}

            return arrayMerged;
        }
        /// <summary>
        /// Create an array of the specified size and initialize it with the provided default value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The length of the array to create.</param>
        /// <param name="value">The default value to initialize the array with.</param>
        /// <returns>A new array of the specified size initialized with the provided default value.</returns>
        public static T[] Initialize<T>(int length, T value)
        {
            T[] array = new T[length];
            for (int x = 0; x < length; x++)
            {
                array[x] = value;
            }
            return array;
        }
        /// <summary>
        /// Create an array of the specified size and initialize it with the provided default value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The 1st dimension of the array to create.</param>
        /// <param name="width">The 2nd dimension of the array to create.</param>
        /// <returns>A new array of the specified size initialized with the provided default value.</returns>
        public static T[][] Initialize<T>(int length, int width)
        {
            T[][] array = new T[length][];
            for (int x = 0; x < length; x++)
            {
                array[x] = new T[width];
            }
            return array;
        }
        /// <summary>
        /// Create an array of the specified size and initialize it with the provided default value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The 1st dimension of the array to create.</param>
        /// <param name="width">The 2nd dimension of the array to create.</param>
        /// <param name="value">The default value to initialize the array with.</param>
        /// <returns>A new array of the specified size initialized with the provided default value.</returns>
        public static T[][] Initialize<T>(int length, int width, T value)
        {
            T[][] array = new T[length][];
            for (int x = 0; x < length; x++)
            {
                array[x] = new T[width];
                for (int y = 0; y < width; y++)
                {
                    array[x][y] = value;
                }
            }
            return array;
        }
        /// <summary>
        /// Create an array of the specified size and initialize it with the provided default value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The 1st dimension of the array to create.</param>
        /// <param name="width">The 2nd dimension of the array to create.</param>
        /// <param name="height">The 3rd dimension of the array to create.</param>
        /// <returns>A new array of the specified size initialized with the provided default value.</returns>
        public static T[][][] Initialize<T>(int length, int width, int height)
        {
            T[][][] array = new T[length][][];
            for (int x = 0; x < length; x++)
            {
                array[x] = new T[width][];
                for (int y = 0; y < width; y++)
                {
                    array[x][y] = new T[height];
                }
            }
            return array;
        }
        /// <summary>
        /// Create an array of the specified size and initialize it with the provided default value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="length">The 1st dimension of the array to create.</param>
        /// <param name="width">The 2nd dimension of the array to create.</param>
        /// <param name="height">The 3rd dimension of the array to create.</param>
        /// <param name="value">The default value to initialize the array with.</param>
        /// <returns>A new array of the specified size initialized with the provided default value.</returns>
        public static T[][][] Initialize<T>(int length, int width, int height, T value)
        {
            T[][][] array = new T[length][][];
            for (int x = 0; x < length; x++)
            {
                array[x] = new T[width][];
                for (int y = 0; y < width; y++)
                {
                    array[x][y] = new T[height];
                    for (int z = 0; z < height; z++)
                    {
                        array[x][y][z] = value;
                    }
                }
            }
            return array;
        }
        /// <summary>
        /// Find the index of the sequence in the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="sequence">The sequence to search for.</param>
        /// <param name="start">The index to start from.</param>
        /// <returns>The index of the sequence in case of success or -1 otherwise.</returns>
	    public static int IndexOf<T>(this T[] array, T[] sequence, int start = 0)
        {
            if (sequence == null)
            {
                throw new ArgumentNullException(nameof(sequence));
            }
            if (sequence.Length == 0)
            {
                throw new ArgumentException("Provided sequence is empty.", nameof(sequence));
            }

            int arrayIndexMaximum = array.Length - sequence.Length + 1;
            for (int arrayIndex = start; arrayIndex < arrayIndexMaximum; arrayIndex++)
            {
                bool match = true;
                for (int sequenceIndex = 0; sequenceIndex < sequence.Length; sequenceIndex++)
                {
                    if (!array[arrayIndex + sequenceIndex].Equals(sequence[sequenceIndex]))
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    return arrayIndex;
                }
            }

            return -1;
        }
    }
}
