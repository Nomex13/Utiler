﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        #region CRC32
        // CRC32 Integer
        private static Dictionary<UInt32, UInt32[]> Crc32ChecksumTables = new Dictionary<UInt32, UInt32[]>();
        public static UInt32 BytesHashCrc32Integer(byte[] bytes, UInt32 polynom = 0xEDB88320, UInt32 initialValue = 0xFFFFFFFFu, UInt32 finalXor = 0xFFFFFFFFu, bool isInputReflected = true, bool isOutputReflected = true)
        {
            UInt32[] table;
            lock(Crc32ChecksumTables)
            {
                // Lazy initialize the lookup table
                if (!Crc32ChecksumTables.TryGetValue(polynom, out table))
                {
                    table = new UInt32[256];
                    for (UInt32 tableIndex = 0; tableIndex < 256; tableIndex++)
                    {
                        UInt32 item = tableIndex;
                        for (var i = 0; i < 8; i++)
                        {
                            item = ((item & 1) == 1) ? (polynom ^ (item >> 1)) : (item >> 1);
                        }
                        table[tableIndex] = item;
                    }
                    Crc32ChecksumTables.Add(polynom, table);
                }
            }
            UInt32 crc32 = initialValue;
            for (int i = 0; i < bytes.Length; i++)
            {
                crc32 = (crc32 >> 8) ^ table[(crc32 ^ bytes[i]) & 0xFF];
            }
            crc32 ^= finalXor;
            return crc32;
        }
        public static UInt32 StringHashCrc32Integer(string @string, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(@string);
            return BytesHashCrc32Integer(bytes, polynom, initialization);
        }
        public static UInt32 FileHashCrc32Integer(string path, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Cannot calculate hash of a file that does not exist.", path);
            }
            byte[] bytes = FileReadBytes(path);
            return BytesHashCrc32Integer(bytes, polynom, initialization);
        }
        // CRC32 Bytes
        public static byte[] BytesHashCrc32(byte[] bytes, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            UInt32 hash = BytesHashCrc32Integer(bytes, polynom, initialization);
            return new byte[4] { (byte)(hash >> 24), (byte)(hash >> 16), (byte)(hash >> 8), (byte)hash };
        }
        public static byte[] StringHashCrc32(string @string, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            UInt32 hash = StringHashCrc32Integer(@string, polynom, initialization);
            return new byte[4] { (byte)(hash >> 24), (byte)(hash >> 16), (byte)(hash >> 8), (byte)hash };
        }
        public static byte[] FileHashCrc32(string path, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            UInt32 hash = FileHashCrc32Integer(path, polynom, initialization);
            return new byte[4] { (byte)(hash >> 24), (byte)(hash >> 16), (byte)(hash >> 8), (byte)hash };
        }
        // CRC32 Hex
        public static string BytesHashCrc32Hex(byte[] bytes, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToHex(BytesHashCrc32(bytes, polynom, initialization));
        }
        public static string StringHashCrc32Hex(string @string, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToHex(StringHashCrc32(@string, polynom, initialization));
        }
        public static string FileHashCrc32Hex(string path, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToHex(FileHashCrc32(path, polynom, initialization));
        }
        // CRC32 Base64
        public static string BytesHashCrc32Base64(byte[] bytes, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToBase64(BytesHashCrc32(bytes, polynom, initialization));
        }
        public static string StringHashCrc32Base64(string @string, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToBase64(StringHashCrc32(@string, polynom, initialization));
        }
        public static string FileHashCrc32Base64(string path, UInt32 polynom = 0xEDB88320, UInt32 initialization = 0xFFFFFFFF)
        {
            return BytesToBase64(FileHashCrc32(path, polynom, initialization));
        }
        #endregion

        #region MD5
        // MD5
        public static byte[] BytesHashMd5(byte[] bytes)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] hash = md5.ComputeHash(bytes);
                return hash;
            }
        }
        public static byte[] StringHashMd5(string @string)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(@string);
            return BytesHashMd5(bytes);
        }
        public static byte[] FileHashMd5(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Cannot calculate hash of a file that does not exist.", path);
            }
            byte[] bytes = FileReadBytes(path);
            return BytesHashMd5(bytes);
        }
        // MD5 Hex
        public static string BytesHashMd5Hex(byte[] bytes)
        {
            return BytesToHex(BytesHashMd5(bytes));
        }
        public static string StringHashMd5Hex(string @string)
        {
            return BytesToHex(StringHashMd5(@string));
        }
        public static string FileHashMd5Hex(string path)
        {
            return BytesToHex(FileHashMd5(path));
        }
        // MD5 Base64
        public static string BytesHashMd5Base64(byte[] bytes)
        {
            return BytesToBase64(BytesHashMd5(bytes));
        }
        public static string StringHashMd5Base64(string @string)
        {
            return BytesToBase64(StringHashMd5(@string));
        }
        public static string FileHashMd5Base64(string path)
        {
            return BytesToBase64(FileHashMd5(path));
        }
        #endregion

        #region SHA1
        // SHA1
        public static byte[] BytesHashSha1(byte[] bytes)
        {
            using (System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create())
            {
                byte[] hash = sha1.ComputeHash(bytes);
                return hash;
            }
        }
        public static byte[] StringHashSha1(string @string)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(@string);
            return BytesHashSha1(bytes);
        }
        public static byte[] FileHashSha1(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Cannot calculate hash of a file that does not exist.", path);
            }
            byte[] bytes = FileReadBytes(path);
            return BytesHashSha1(bytes);
        }
        // SHA1 Hex
        public static string BytesHashSha1Hex(byte[] bytes)
        {
            return BytesToHex(BytesHashSha1(bytes));
        }
        public static string StringHashSha1Hex(string @string)
        {
            return BytesToHex(StringHashSha1(@string));
        }
        public static string FileHashSha1Hex(string path)
        {
            return BytesToHex(FileHashSha1(path));
        }
        // SHA1 Base64
        public static string BytesHashSha1Base64(byte[] bytes)
        {
            return BytesToBase64(BytesHashSha1(bytes));
        }
        public static string StringHashSha1Base64(string @string)
        {
            return BytesToBase64(StringHashSha1(@string));
        }
        public static string FileHashSha1Base64(string path)
        {
            return BytesToBase64(FileHashSha1(path));
        }
        #endregion

        #region SHA256
        // SHA256
        public static byte[] BytesHashSha256(byte[] bytes)
        {
            using (System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create())
            {
                byte[] hash = sha256.ComputeHash(bytes);
                return hash;
            }
        }
        public static byte[] StringHashSha256(string @string)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(@string);
            return BytesHashSha256(bytes);
        }
        public static byte[] FileHashSha256(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Cannot calculate hash of a file that does not exist.", path);
            }
            byte[] bytes = FileReadBytes(path);
            return BytesHashSha256(bytes);
        }
        // SHA256 Hex
        public static string BytesHashSha256Hex(byte[] bytes)
        {
            return BytesToHex(BytesHashSha256(bytes));
        }
        public static string StringHashSha256Hex(string @string)
        {
            return BytesToHex(StringHashSha256(@string));
        }
        public static string FileHashSha256Hex(string path)
        {
            return BytesToHex(FileHashSha256(path));
        }
        // SHA256 Base64
        public static string BytesHashSha256Base64(byte[] bytes)
        {
            return BytesToBase64(BytesHashSha256(bytes));
        }
        public static string StringHashSha256Base64(string @string)
        {
            return BytesToBase64(StringHashSha256(@string));
        }
        public static string FileHashSha256Base64(string path)
        {
            return BytesToBase64(FileHashSha256(path));
        }
        #endregion
    }
}
