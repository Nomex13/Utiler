﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public static T EnumGetValue<T>(byte value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue(value);
        }
        public static T EnumGetValue<T>(short value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue(value);
        }
        public static T EnumGetValue<T>(int value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue(value);
        }
        public static T EnumGetValue<T>(long value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue(value);
        }
        public static T EnumGetValue<T>(float value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue((int)value);
        }
        public static T EnumGetValue<T>(double value) where T : Enum
        {
            return (T)Enum.GetValues(typeof(T)).GetValue((long)value);
        }
    }
}
