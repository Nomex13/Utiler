﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public const string DoubleFixedPointFormat = "0.###################################################################################################################################################################################################################################################################################################################################################";

        /// <summary>
        /// Check if two doubles are equal. See <see cref="https://floating-point-gui.de/errors/comparison/"/>.
        /// </summary>
        /// <param name="one">First.</param>
        /// <param name="two">Second.</param>
        /// <param name="epsilon">Minimal difference.</param>
        /// <returns>If doubles are equal within epsilon.</returns>
        public static bool Equals(double one, double two, double epsilon = Double.Epsilon)
        {
            // Shortcut, handles infinities
		    if (one == two)
            {
                return true;
            }
		    double diff = Math.Abs(one - two);

			// One or two is zero or both are extremely close to it, relative error is less meaningful here
		    if (one == 0 || two == 0 || diff < Double.MinValue)
            {
			    return diff < (epsilon * Double.MinValue);
		    }

            // Use relative error
		    double absA = Math.Abs(one);
            double absB = Math.Abs(two);
			return diff / Math.Min(absA + absB, Double.MaxValue) < epsilon;
	    }
        /// <summary>
        /// Check if two doubles are equal.
        /// </summary>
        /// <param name="one">First.</param>
        /// <param name="two">Second.</param>
        /// <returns>If doubles are bitwise equal.</returns>
        public static bool Equals(double one, double two)
        {
            return BitConverter.DoubleToInt64Bits(one) == BitConverter.DoubleToInt64Bits(two);
	    }
        /// <summary>
        /// Check if the looped value is inside the provided range.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="loopStart">The start of the loop. In most usecases it is 0.</param>
        /// <param name="loopEnd">The end of the loop.</param>
        /// <param name="rangeStart">The start of the range. If greater than the end of the range then the range is considered to be looped.</param>
        /// <param name="rangeEnd">The end of the range. If lesser than the start of the range then the range is considered to be looped.</param>
        /// <returns></returns>
        public static bool IsInRange(ref double value, double loopStart, double loopEnd, double rangeStart, double rangeEnd)
        {
            if (loopStart == loopEnd)
            {
                return true;
            }

            if (loopStart > loopEnd)
            {
                Swap(ref loopStart, ref loopEnd);
            }

            double loopLength = loopEnd - loopStart;

            double valueInLoop = Mathematics.Modulo(value - loopStart, loopLength) + loopStart;
            double rangeStartInLoop = Mathematics.Modulo(rangeStart - loopStart, loopLength) + loopStart;
            double rangeEndInLoop = Mathematics.Modulo(rangeEnd - loopStart, loopLength) + loopStart;

            if (rangeStartInLoop < rangeEndInLoop)
            {
                return rangeStartInLoop <= valueInLoop && valueInLoop <= rangeEndInLoop;
            }
            else
            {
                return valueInLoop <= rangeEndInLoop || rangeStartInLoop <= valueInLoop;
            }
        }
    }
}
