﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        /// <summary>
        /// Check if the looped value is inside the provided range.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="loopStart">The start of the loop. In most usecases it is 0.</param>
        /// <param name="loopEnd">The end of the loop.</param>
        /// <param name="rangeStart">The start of the range. If greater than the end of the range then the range is considered to be looped.</param>
        /// <param name="rangeEnd">The end of the range. If lesser than the start of the range then the range is considered to be looped.</param>
        /// <returns></returns>
        public static bool IsInRange(ref int value, int loopStart, int loopEnd, int rangeStart, int rangeEnd)
        {
            if (loopStart == loopEnd)
            {
                return true;
            }

            if (loopStart > loopEnd)
            {
                Swap(ref loopStart, ref loopEnd);
            }

            int loopLength = loopEnd - loopStart;

            int valueInLoop = Mathematics.Modulo(value - loopStart, loopLength) + loopStart;
            int rangeStartInLoop = Mathematics.Modulo(rangeStart - loopStart, loopLength) + loopStart;
            int rangeEndInLoop = Mathematics.Modulo(rangeEnd - loopStart, loopLength) + loopStart;

            if (rangeStartInLoop < rangeEndInLoop)
            {
                return rangeStartInLoop <= valueInLoop && valueInLoop <= rangeEndInLoop;
            }
            else
            {
                return valueInLoop <= rangeEndInLoop || rangeStartInLoop <= valueInLoop;
            }
        }
    }
}
