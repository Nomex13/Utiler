﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
	    /// <summary>
	    /// Recursive lambda through Y combinator.
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <typeparam name="R"></typeparam>
	    /// <param name="function"></param>
	    /// <returns>Recursive lambda.</returns>
        public static Func<T, R> Y<T, R>(Func<Func<T, R>, Func<T, R>> function)
        {
            Func<T, R> recursive = null;
            recursive = function(argument => recursive(argument));
            return recursive;
        }
	    /// <summary>
	    /// Recursive lambda through Y combinator.
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <param name="function"></param>
	    /// <returns>Recursive lambda.</returns>
        public static Func<T, T> Y<T>(Func<Func<T, T>, Func<T, T>> function)
        {
            Func<T, T> recursive = null;
            recursive = function(argument => recursive(argument));
            return recursive;
        }
    }
}
