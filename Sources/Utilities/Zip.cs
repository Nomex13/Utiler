﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        //private static HashSet<string> ZipGetDirectories(IEnumerable<string> paths)
        //{
        //    HashSet<string> directories = new HashSet<string>();

        //    foreach (string path in paths)
        //    {
        //        directories.AddRange(PathGetDirectorySequence(path));
        //    }

        //    return directories;
        //}
        //public static void Zip(string directorySource, string pathDestination)
        //{
        //    using (StreamWriter archiveStream = new StreamWriter(pathDestination))
        //    {
        //        using (ZipArchive archive = new ZipArchive(archiveStream, ZipArchiveMode.Create, true))
        //        {
        //            // Create directories
        //            if (directories != null)
        //            {
        //                foreach (string directory in directories)
        //                {
        //                    archive.CreateEntry(directory);
        //                }
        //            }
        //            // Create files
        //            if (files != null)
        //            {
        //                // Create files
        //                foreach (KeyValuePair<string, byte[]> relativePathAndContent in files)
        //                {
        //                    string path = relativePathAndContent.Key;
        //                    byte[] content = relativePathAndContent.Value;

        //                    using (Stream archiveEntryStream = archive.CreateEntry(path).Open())
        //                    using (BinaryWriter archiveEntryBinaryWriter = new BinaryWriter(archiveEntryStream))
        //                    {
        //                        archiveEntryBinaryWriter.Write(content);
        //                    }
        //                }
        //            }
        //        }

        //        archiveStream.Seek(0, SeekOrigin.Begin);
        //        return archiveStream.ToArray();
        //    }
        //}


        private static void ZipToPngFixOffsets(byte[] bytes, int offsetStart, int offsetShift)
        {
	        // Find the "end of central directory" marker
            byte[] endOfCentralDirectorySignature = new byte[] { 0x50, 0x4b, 0x05, 0x06 }; // PK\x05\x06
            byte[] localFileHeaderSignature       = new byte[] { 0x50, 0x4b, 0x01, 0x02 }; // PK\x01\x02

            // EOCD (End of central directory) record header offset
	        int endOfCentralDirectoryOffset = bytes.IndexOf(endOfCentralDirectorySignature, offsetStart);
            // EOCD (End of central directory) record fields offsets
	        int totalNumberOfCentralDirectoryRecordsOffset = endOfCentralDirectoryOffset + 10;
	        int offsetOfStartOfCentralDirectoryOffset      = endOfCentralDirectoryOffset + 16;

            // EOCD (End of central directory) record field values (only ones that we need)
	        short totalNumberOfCentralDirectoryRecordsValue = bytes.ReadLittleEndianShort(totalNumberOfCentralDirectoryRecordsOffset);
	        int offsetOfStartOfCentralDirectoryValue = bytes.ReadLittleEndianShort(offsetOfStartOfCentralDirectoryOffset);

	        // Fix the offset of the central directory entries
            offsetOfStartOfCentralDirectoryValue += offsetShift;
            bytes.WriteLittleEndianInteger(offsetOfStartOfCentralDirectoryOffset, offsetOfStartOfCentralDirectoryValue);

            int offset = 0;
	        // Iterate over the central directory entries
	        for (int i = 0; i < totalNumberOfCentralDirectoryRecordsValue; i++)
            {
	            offset = bytes.IndexOf(localFileHeaderSignature, offset);

		        // Fix the offset that points to the local file header
                int relativeOffsetOfLocalFileHeaderOffset = offset + 42;
                int relativeOffsetOfLocalFileHeaderValue = bytes.ReadLittleEndianInteger(relativeOffsetOfLocalFileHeaderOffset);
                relativeOffsetOfLocalFileHeaderValue += offsetShift;
                bytes.WriteLittleEndianInteger(relativeOffsetOfLocalFileHeaderOffset, relativeOffsetOfLocalFileHeaderValue);

                // Shift a bit so the next record will be found
		        offset++;
            }
        }

        /// <summary>
        /// Append a ZIP file to a PNG file.
        /// The resulting file will act as both: as an image and as an archive.
        /// </summary>
        /// <param name="pathZip">Path to the ZIP file.</param>
        /// <param name="pathPng">Path to the PNG file.</param>
        /// <param name="pathHybrid">Path to the new hybrid file.</param>
        public static void AppendZipToPng(string pathZip, string pathPng, string pathHybrid)
        {
            if (!File.Exists(pathZip))
            {
                throw new FileNotFoundException("The ZIP file not found.", pathZip);
            }
            if (!File.Exists(pathPng))
            {
                throw new FileNotFoundException("The PNG file not found.", pathPng);
            }

            byte[] pngBytes = File.ReadAllBytes(pathPng);
            byte[] zipBytes = File.ReadAllBytes(pathZip);
            byte[] hybridBytes = Utiler.AppendZipToPng(zipBytes, pngBytes);

            File.WriteAllBytes(pathHybrid, hybridBytes);
        }
        /// <summary>
        /// Append a ZIP file to a PNG file.
        /// The resulting file will act as both: as an image and as an archive.
        /// </summary>
        /// <param name="zip">ZIP file contents.</param>
        /// <param name="png">PNG file contents.</param>
        /// <returns>New hybrid file contents.</returns>
        public static byte[] AppendZipToPng(byte[] zip, byte[] png)
        {
            byte[] bytes = new byte[png.Length + zip.Length];

            Array.Copy(png, 0, bytes, 0, png.Length);
            Array.Copy(zip, 0, bytes, png.Length, zip.Length);
            ZipToPngFixOffsets(bytes, png.Length, png.Length);

            return bytes;
        }
        /// <summary>
        /// Embed a ZIP file into a PNG file.
        /// The resulting file will act as both: as an image and as an archive.
        /// Embedding modifies the resulting PNG contents in the following manner: all headers but IHDR, PLTE, IDAT and IEND are removed; IDAT chunks are merged.
        /// </summary>
        /// <param name="pathZip">Path to the ZIP file.</param>
        /// <param name="pathPng">Path to the PNG file.</param>
        /// <param name="pathHybrid">Path to the new hybrid file.</param>
        public static void EmbedZipIntoPng(string pathZip, string pathPng, string pathHybrid)
        {
            if (!File.Exists(pathZip))
            {
                throw new FileNotFoundException("The ZIP file not found.", pathZip);
            }
            if (!File.Exists(pathPng))
            {
                throw new FileNotFoundException("The PNG file not found.", pathPng);
            }

            byte[] pngBytes = File.ReadAllBytes(pathPng);
            byte[] zipBytes = File.ReadAllBytes(pathZip);
            byte[] hybridBytes = Utiler.EmbedZipIntoPng(zipBytes, pngBytes);

            File.WriteAllBytes(pathHybrid, hybridBytes);
        }
        /// <summary>
        /// Embed a ZIP file into a PNG file.
        /// The resulting file will act as both: as an image and as an archive.
        /// Embedding modifies the resulting PNG contents in the following manner: all headers but IHDR, PLTE, IDAT and IEND are removed; IDAT chunks are merged.
        /// </summary>
        /// <param name="zip">ZIP file contents.</param>
        /// <param name="png">PNG file contents.</param>
        /// <returns>New hybrid file contents.</returns>
        public static byte[] EmbedZipIntoPng(byte[] zip, byte[] png)
        {
            const string pngPrefix = "\x89PNG\r\n\x1a\n";

            // IDAT chunks bodies
            List<byte[]> idatChunkBodies = new List<byte[]>();

            // PNG chunks offsets
            int chunkStartOffset = 0;
            int chunkEndOffset = pngPrefix.Length;

            // PLTE chunk parameters
            int plteChunkOffset = -1;
            int plteChunkLength = -1;

            // IHDR chunk parameters
            int ihdrChunkOffset = -1;
            int ihdrChunkLength = -1;

            // Iterate through the chunks of the PNG file
            while (chunkEndOffset < png.Length)
            {
                chunkStartOffset = chunkEndOffset;
                int offset = chunkStartOffset;

                // Chunk length
                int chunkBodyLength = png.ReadBigEndianInteger(offset);
                offset += 4;

                // Chunk type
                byte[] chunkType = new byte[4];
                Array.Copy(png, offset, chunkType, 0, chunkType.Length);
                offset += 4;

                // Chunk body
                byte[] chunkBody = new byte[chunkBodyLength];
                Array.Copy(png, offset, chunkBody, 0, chunkBody.Length);
                offset += chunkBody.Length;

                // Chunk CRC
                int chunkCrc = png.ReadBigEndianInteger(offset);
                offset += 4;

                chunkEndOffset = offset;
                int chunkLength = chunkEndOffset - chunkStartOffset;

                // Collect IDATs
                if (chunkType.SequenceEqual(new byte[] { 73, 68, 65, 84 } /* IDAT */))
                {
                    idatChunkBodies.Add(chunkBody);
                    continue;
                }
                else if (chunkType.SequenceEqual(new byte[] { 73, 72, 68, 82 } /* IHDR */))
                {
                    ihdrChunkOffset = chunkStartOffset;
                    ihdrChunkLength = chunkLength;
                }
                else if (chunkType.SequenceEqual(new byte[] { 80, 76, 84, 69 } /* PLTE */))
                {
                    plteChunkOffset = chunkStartOffset;
                    plteChunkLength = chunkLength;
                }
                else if (chunkType.SequenceEqual(new byte[] { 73, 69, 78, 68 } /* IEND */))
                {
                    // Add the zip file
                    idatChunkBodies.Add(zip);

                    // Merge all IDAT chunks into one
                    byte[] idatChunkBody = new byte[idatChunkBodies.Sum(_idatBody => _idatBody.Length)];
                    int idatChunkBodyOffset = 0;
                    for (int i = 0; i < idatChunkBodies.Count; i++)
                    {
                        int length = idatChunkBodies[i].Length;
                        Array.Copy(idatChunkBodies[i], 0, idatChunkBody, idatChunkBodyOffset, length);
                        idatChunkBodyOffset += length;
                    }

                    // Create the resulting array
                    int bytesLength =
                        pngPrefix.Length + // PNG prefix
                        (ihdrChunkOffset >= 0 ? ihdrChunkLength : 0) + // IHDR chunk
                        (plteChunkOffset >= 0 ? plteChunkLength : 0) + // PLTE chunk
                        idatChunkBody.Length + 12 /* IDAT length, type and CRC32 */ + // IDAT chunk
                        chunkLength; // IEND chunk
                    byte[] bytes = new byte[bytesLength];
                    int bytesOffset = 0;

                    // Copy the prefix
                    Array.Copy(png, 0, bytes, 0, pngPrefix.Length);
                    bytesOffset += pngPrefix.Length;

                    // Copy the IHDR chunk
                    if (ihdrChunkOffset >= 0)
                    {
                        Array.Copy(png, ihdrChunkOffset, bytes, bytesOffset, ihdrChunkLength);
                        bytesOffset += ihdrChunkLength;
                    }
                    // Copy the PLTE chunk
                    if (plteChunkOffset >= 0)
                    {
                        Array.Copy(png, plteChunkOffset, bytes, bytesOffset, plteChunkLength);
                        bytesOffset += plteChunkLength;
                    }
                    // Copy the IDAT chunk
                    {
                        // Write IDAT length
                        bytes.WriteBigEndianInteger(bytesOffset, idatChunkBody.Length);
                        bytesOffset += 4;

                        // Save offset to calculate CRC32 later
                        int bytesOffsetBeforeIdatHeader = bytesOffset;

                        // Write IDAT type
                        Array.Copy(new byte[] { 73, 68, 65, 84 }, 0, bytes, bytesOffset, 4);
                        bytesOffset += 4;

                        // Fix offsets in the ZIP array
                        int zipOffset = idatChunkBody.Length - zip.Length + bytesOffset;
                        ZipToPngFixOffsets(idatChunkBody, idatChunkBody.Length - zip.Length, zipOffset);

                        // Write IDAT body
                        Array.Copy(idatChunkBody, 0, bytes, bytesOffset, idatChunkBody.Length);
                        bytesOffset += idatChunkBody.Length;

                        // Write IDAT CRC32
                        byte[] bytesToCrc = new byte[idatChunkBody.Length + 4];
                        Array.Copy(bytes, bytesOffsetBeforeIdatHeader, bytesToCrc, 0, bytesToCrc.Length);
                        uint crc32 = BytesHashCrc32Integer(bytesToCrc);
                        bytes.WriteBigEndianUnsignedInteger(bytesOffset, crc32);
                        bytesOffset += 4;
                    }
                    // Copy the whole IEND chunk
                    {
                        Array.Copy(png, chunkStartOffset, bytes, bytesOffset, chunkLength);
                        bytesOffset += chunkLength;
                    }

                    return bytes;
                }
                else
                {
                    // Skip all other chunks
                }
            }

            throw new Exception("EOF");
        }
        public static byte[] Zip(Dictionary<string, byte[]> files, IEnumerable<string> directories = null)
        {
            //HashSet<string> uniqueDirectories = ZipGetDirectories(files.Keys);
            //if (directories != null)
            //{
            //    foreach (string directory in directories)
            //    {
            //        uniqueDirectories.Add((directory.EndsWith("/") || directory.EndsWith("\\")) ? directory : (directory + "/"));
            //    }
            //}
            using (MemoryStream archiveStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(archiveStream, ZipArchiveMode.Create, true))
                {
                    // Create directories
                    if (directories != null)
                    {
                        foreach (string directory in directories)
                        {
                            archive.CreateEntry(directory);
                        }
                    }
                    // Create files
                    if (files != null)
                    {
                        // Create files
                        foreach (KeyValuePair<string, byte[]> relativePathAndContent in files)
                        {
                            string path = relativePathAndContent.Key;
                            byte[] content = relativePathAndContent.Value;

                            using (Stream archiveEntryStream = archive.CreateEntry(path).Open())
                            using (BinaryWriter archiveEntryBinaryWriter = new BinaryWriter(archiveEntryStream))
                            {
                                archiveEntryBinaryWriter.Write(content);
                            }
                        }
                    }
                }

                archiveStream.Seek(0, SeekOrigin.Begin);
                return archiveStream.ToArray();
            }
        }
        public static byte[] Zip(Dictionary<string, string> files, IEnumerable<string> directories = null)
        {
            //HashSet<string> uniqueDirectories = ZipGetDirectories(files.Keys);
            //if (directories != null)
            //{
            //    foreach (string directory in directories)
            //    {
            //        uniqueDirectories.Add((directory.EndsWith("/") || directory.EndsWith("\\")) ? directory : (directory + "/"));
            //    }
            //}
            using (MemoryStream archiveStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(archiveStream, ZipArchiveMode.Create, true))
                {
                    // Create directories
                    if (directories != null)
                    {
                        foreach (string directory in directories)
                        {
                            archive.CreateEntry(directory.Replace('\\', '/'));
                        }
                    }
                    // Create files
                    if (files != null)
                    {
                        foreach (KeyValuePair<string, string> relativePathAndContent in files)
                        {
                            string path = relativePathAndContent.Key.Replace('\\', '/');
                            string content = relativePathAndContent.Value;

                            using (Stream archiveEntryStream = archive.CreateEntry(path).Open())
                            using (StreamWriter archiveEntryStreamWriter = new StreamWriter(archiveEntryStream))
                            {
                                archiveEntryStreamWriter.Write(content);
                            }
                        }
                    }
                }

                archiveStream.Seek(0, SeekOrigin.Begin);
                return archiveStream.ToArray();
            }
            //Dictionary<string, byte[]> relativePathToBytes = new Dictionary<string, byte[]>();
            //foreach (KeyValuePair<string, string> relativePathAndContent in relativePathToContent)
            //{
            //    relativePathToBytes.Add(relativePathAndContent.Key, Encoding.UTF8.GetBytes(relativePathAndContent.Value));
            //}
            //return Zip(relativePathToBytes);
        }
        public static Dictionary<string, byte[]> UnZip(byte[] bytes)
        {
            Dictionary<string, byte[]> relativePathToContent = new Dictionary<string, byte[]>();

            using (MemoryStream archiveStream = new MemoryStream(bytes))
            using (ZipArchive archive = new ZipArchive(archiveStream, ZipArchiveMode.Read))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    using (Stream archiveEntryStream = entry.Open())
                    {
                        byte[] entryBytes = archiveEntryStream.ReadAllBytes();
                        relativePathToContent.Add(entry.FullName, entryBytes);
                    }
                }
            }

            return relativePathToContent;
        }
    }
}
