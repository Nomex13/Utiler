﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public enum UnderlineStyle
        {
            NONE,
            CONTINIOUS,
            DOTTED,
            DASH,
            DOT_DASH,
            DOT_DOT_DASH,
            DOUBLE,
            THICK,
            WORD,
            WAVE,
        }
        public class RtfFontUnderline : RtfElement
        {
            public UnderlineStyle Style;
            public RtfFontUnderline(RtfDocument document, UnderlineStyle style)
                : base(document)
            {
                Style = style;
            }

            public override string ToString()
            {
                switch (Style)
                {
                    case UnderlineStyle.NONE:
                        return @"\ulnone";
                    case UnderlineStyle.CONTINIOUS:
                        return @"\ul";
                    case UnderlineStyle.DOTTED:
                        return @"\uld";
                    case UnderlineStyle.DASH:
                        return @"\uldash";
                    case UnderlineStyle.DOT_DASH:
                        return @"\uldashd";
                    case UnderlineStyle.DOT_DOT_DASH:
                        return @"\uldashdd";
                    case UnderlineStyle.DOUBLE:
                        return @"\uldb";
                    case UnderlineStyle.THICK:
                        return @"\ulth";
                    case UnderlineStyle.WORD:
                        return @"\ulw";
                    case UnderlineStyle.WAVE:
                        return @"\ulwave";
                    default:
                        throw new NotImplementedException($"{nameof(UnderlineStyle)}.{Style} is not supported.");
                }
            }
        }

        /// <summary>
        /// Set font underlining.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="style">The font underlining.</param>
        public static RtfGroup Underlined(this RtfGroup group, UnderlineStyle style)
        {
            group.Add(new RtfFontUnderline(group.Document, style));
            return group;
        }
    }
}
