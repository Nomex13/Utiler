﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfFontSize : RtfElement
        {
            /// <summary>
            /// Font size.
            /// </summary>
            public int Size;
            public RtfFontSize(RtfDocument document, int size)
                : base(document)
            {
                Size = size;
            }

            public override string ToString()
            {
                return $@"\fs{Size}";
            }
        }

        /// <summary>
        /// Set font size.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="size">The font size.</param>
        public static RtfGroup SetFontSize(this RtfGroup group, int size)
        {
            group.Add(new RtfFontSize(group.Document, size));
            return group;
        }
    }
}
