﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfFontBold : RtfElement
        {
            /// <summary>
            /// Font is bold.
            /// </summary>
            public bool IsBold;

            public RtfFontBold(RtfDocument document, bool isBold = true)
                : base(document)
            {
                IsBold = isBold;
            }

            public override string ToString()
            {
                return IsBold ? @"\b" : @"\b0";
            }
        }

        /// <summary>
        /// Set font boldness.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="isBold">Is font bold.</param>
        public static RtfGroup Bold(this RtfGroup group, bool isBold = true)
        {
            group.Add(new RtfFontBold(group.Document, isBold));
            return group;
        }
    }
}
