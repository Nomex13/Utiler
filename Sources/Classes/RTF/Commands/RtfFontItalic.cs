﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfFontItalic : RtfElement
        {
            /// <summary>
            /// Font is bold.
            /// </summary>
            public bool IsBold;

            public RtfFontItalic(RtfDocument document, bool isItalic = true)
                : base(document)
            {
                IsBold = isItalic;
            }

            public override string ToString()
            {
                return IsBold ? @"\i" : @"\i0";
            }
        }

        /// <summary>
        /// Set font italicness.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="isItalic">Is font italic.</param>
        public static RtfGroup Italic(this RtfGroup group, bool isItalic = true)
        {
            group.Add(new RtfFontItalic(group.Document, isItalic));
            return group;
        }
    }
}
