﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfFont : RtfElement
        {
            /// <summary>
            /// Font size.
            /// </summary>
            public int FontSize;
            /// <summary>
            /// Font family name.
            /// </summary>
            public string FontFamily;

            public RtfFont(RtfDocument document)
                : base(document)
            {
                ;
            }

            public override string ToString()
            {
                int index = Document.GetFontIndex(this);
                return $@"\f{index + 1}";
            }
        }
    }
}
