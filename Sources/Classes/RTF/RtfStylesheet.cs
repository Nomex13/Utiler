﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfStylesheet
        {
            /// <summary>
            /// Stylesheet index.
            /// </summary>
            public int Index;
            public int? FontSize;
            /// <summary>
            /// Indicates that character style attributes are to be added to the current paragraph style attributes, rather than setting the paragraph attributes to only those defined in the character style definition.
            /// </summary>
            public bool IsAdditive;
            /// <summary>
            /// Defines the number of the style on which the current style is based (the default is 222 -- no style).
            /// </summary>
            public int StylesheetBasedOn = 222;

            public RtfStylesheet()
            {
                ;
            }

            public override string ToString()
            {
                return $@"{{\s{Index}\sbasedon{StylesheetBasedOn}{(FontSize.HasValue ? $@"\{FontSize.Value}" : "")}{(IsAdditive ? @"\additive" : "")}}}";
            }
        }
    }
}
