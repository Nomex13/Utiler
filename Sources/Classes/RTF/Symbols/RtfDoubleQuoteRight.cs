﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A right double quote \rdblquote command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup DoubleQuoteRight(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\rdblquote"));
            return group;
        }
    }
}
