﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A short dash \endash command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup DashShort(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\endash"));
            return group;
        }
    }
}
