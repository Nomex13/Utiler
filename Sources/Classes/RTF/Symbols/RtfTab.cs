﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A tab \tab command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup Tab(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\tab"));
            return group;
        }
    }
}
