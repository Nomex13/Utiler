﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A left double quote \ldblquote command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup DoubleQouteLeft(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\ldblquote"));
            return group;
        }
    }
}
