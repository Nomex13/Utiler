﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// An optional hyphen \- command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup HyphenOptional(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\-"));
            return group;
        }
    }
}
