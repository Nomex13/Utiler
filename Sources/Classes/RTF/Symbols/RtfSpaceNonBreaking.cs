﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A non-breaking space \~ command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup NonBreakingSpace(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\~"));
            return group;
        }
    }
}
