﻿//namespace Iodynis.Libraries.Utility
//{
//    public class LinkedListItem<T>
//    {
//        private LinkedListState<T> State;

//        public LinkedList<T> List;
//        public T Value;
//        public LinkedListItem<T> Previous;
//        public LinkedListItem<T> Next;
//        public LinkedListItem(LinkedListState<T> state, T item)
//        {
//            State = state;
//            Value = item;
//        }
//        private static int CountPrevious(LinkedListItem<T> item)
//        {
//            int count = 0;
//            while ((item = item.Previous) != null)
//            {
//                count++;
//            }
//            return count;
//        }
//        private static int CountNext(LinkedListItem<T> item)
//        {
//            int count = 0;
//            while ((item = item.Next) != null)
//            {
//                count++;
//            }
//            return count;
//        }
//        public void Remove()
//        {
//            if (State.First == this)
//            {
//                State.First = Next;
//            }
//            if (State.Last == this)
//            {
//                State.Last = Next;
//            }

//            Previous.Next = Next;
//            Next.Previous = Previous;
//            State.Count--;
//        }
//        public void RemovePrevious()
//        {
//            if (Previous == null)
//            {
//                return;
//            }

//            Previous.Remove();
//        }
//        public void RemoveNext()
//        {
//            if (Next == null)
//            {
//                return;
//            }

//            Next.Remove();
//        }
//        public void RemoveAllPrevious()
//        {
//            State.Count -= CountPrevious(this);

//            Previous = null;
//            State.First = this;
//        }
//        public void RemoveAllNext()
//        {
//            State.Count -= CountNext(this);

//            Next = null;
//            State.Last = this;
//        }
//        public LinkedListItem<T> AddPrevious(T value)
//        {
//            return AddPrevious(new LinkedListItem<T>(State, value));
//        }
//        public LinkedListItem<T> AddNext(T value)
//        {
//            return AddNext(new LinkedListItem<T>(State, value));
//        }
//        public LinkedListItem<T> AddPrevious(LinkedListItem<T> item)
//        {
//            item.Next = this;
//            item.Previous = Previous;

//            if (Previous == null)
//            {
//                State.First = item;
//            }
//            else
//            {
//                Previous.Next = item;
//            }
//            Previous = item;
//            State.Count++;

//            return item;
//        }
//        public LinkedListItem<T> AddNext(LinkedListItem<T> item)
//        {
//            item.Next = Next;
//            item.Previous = this;

//            if (Next == null)
//            {
//                State.Last = item;
//            }
//            else
//            {
//                Next.Previous = item;
//            }
//            Next = item;
//            State.Count++;

//            return item;
//        }
//        //private void Swap(ref T one, ref T two)
//        //{
//        //    T temp = one;
//        //    one = two;
//        //    two = temp;
//        //}
//    }
//}
