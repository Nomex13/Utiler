﻿using System;

namespace Iodynis.Libraries.Utility
{
    /// <summary>
    /// Countable flag.
    /// </summary>
    public sealed class FlagCountable
    {
        private sealed class Wrapper : IDisposable
        {
            private FlagCountable Flag;

            /// <summary>
            /// Save and set the flag
            /// </summary>
            public Wrapper(FlagCountable flag)
            {
                if (flag == null)
                {
                    throw new ArgumentNullException(nameof(flag));
                }

                Flag = flag;
                Flag.Set();
            }

            /// <summary>
            /// Unset the flag and dispose
            /// </summary>
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Dispose
            /// </summary>
            public void Dispose(bool disposing)
            {
                if (disposing)
                {
                    Flag.Unset();
                }
            }
        }

        /// <summary>
        /// Flag set/unset counter.
        /// </summary>
        public int Counter { get; private set; } = 0;
        private bool AllowCountGoNegative;

        /// <summary>
        /// Create a countable flag.
        /// </summary>
        /// <param name="allowCountGoNegative">Allow the <see cref="Counter"/> to be negative.</param>
        public FlagCountable(bool allowCountGoNegative = false)
        {
            AllowCountGoNegative = allowCountGoNegative;
        }

        /// <summary>
        /// Use the flag in a scope.
        /// </summary>
        public IDisposable Using
        {
            get
            {
                return new Wrapper(this);
            }
        }

        /// <summary>
        /// Set the flag.
        /// </summary>
        public void Set()
        {
            Counter++;
        }

        /// <summary>
        /// Unset the flag.
        /// </summary>
        public void Unset()
        {
            if (!AllowCountGoNegative && Counter == 0)
            {
                return;
            }

            Counter--;

            if (!AllowCountGoNegative && Counter < 0)
            {
                Counter = 0;
            }
        }

        /// <summary>
        /// Reset the flag.
        /// </summary>
        public void Reset()
        {
            if (Counter == 0)
            {
                return;
            }

            Counter = 0;
        }

        /// <summary>
        /// If the <see cref="Counter"/> is 0.
        /// </summary>
        /// <param name="flag">The flag.</param>
        public static implicit operator bool(FlagCountable flag)
        {
            return flag.Counter > 0;
        }
    }
}
