﻿// by Vladimir Iodynis //

using System;

namespace Iodynis.Libraries.Utility.Graphs
{
    public class GraphSubgraph<TNode, TEdge>
    {
        protected internal Graph<TNode, TEdge> Graph;

        /// <summary>
        /// The root of the subgraph.
        /// </summary>
        public GraphNode<TNode, TEdge> Root { get; private set; }

        protected string _Name;
        /// <summary>
        /// The name of the subgraph.
        /// </summary>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (value != null)
                {
                    // Check the name is not used
                    for (int subgraphIndex = 0; subgraphIndex < Graph.Subgraphs.Count; subgraphIndex++)
                    {
                        if (Graph.Subgraphs[subgraphIndex] == this)
                        {
                            continue;
                        }
                        if (Graph.Subgraphs[subgraphIndex].Name == value)
                        {
                            throw new Exception($"Subgraph with the name {value} already exists.");
                        }
                    }
                }

                _Name = value;
            }
        }

        public GraphSubgraph(Graph<TNode, TEdge> graph)
            : this(graph, null) { }
        public GraphSubgraph(Graph<TNode, TEdge> graph, string name)
        {
            Graph = graph;
            Name = Name;
        }

        protected internal virtual bool Has(GraphNode<TNode, TEdge> node)
        {
            return node.Subgraph == this;
        }

        protected internal virtual GraphNode<TNode, TEdge> Add(GraphNode<TNode, TEdge> node)
        {
            if (node.Subgraph != null && node.Subgraph != this)
            {
                node.Subgraph.Remove(node);
            }

            return node;
        }

        protected internal virtual GraphSubgraph<TNode, TEdge> Remove(GraphNode<TNode, TEdge> node)
        {
            if (node.Subgraph == null)
            {
                throw new ArgumentException($"Node {node} is not owned by any subgraph.", nameof(node));
            }
            if (node.Subgraph != this)
            {
                throw new ArgumentException($"Node {node} is owned by the {node.Subgraph} subgraph, not the {this} subgraph.", nameof(node));
            }

            node.Subgraph = null;

            // Remove all input edges to the node
            for (int inputIndex = 0; inputIndex < node._Inputs.Count; inputIndex++)
            {
                GraphEdge<TNode, TEdge> edge = node._Inputs[inputIndex];
                edge.Output._Outputs.Remove(edge);
            }
            // Remove all output edges from the node
            for (int outputIndex = 0; outputIndex < node._Outputs.Count; outputIndex++)
            {
                GraphEdge<TNode, TEdge> edge = node._Outputs[outputIndex];
                edge.Input._Inputs.Remove(edge);
            }

            // TODO: Check if subgraph was broken into 2 subgraphs
            // TODO: Return the new subgraph if any
            return null;
        }

        protected internal virtual GraphSubgraph<TNode, TEdge> TransferTo(GraphSubgraph<TNode, TEdge> subgraph)
        {
            throw new NotImplementedException();
        }
    }
}
