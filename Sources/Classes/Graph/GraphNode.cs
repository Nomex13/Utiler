﻿using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Graphs
{
    public class GraphNode<TNode, TEdge>
    {
        protected internal GraphSubgraph<TNode, TEdge> Subgraph;

        protected internal List<GraphEdge<TNode, TEdge>> _Inputs = new List<GraphEdge<TNode, TEdge>>();
        public IReadOnlyList<GraphEdge<TNode, TEdge>> Inputs => _Inputs;
        protected internal List<GraphEdge<TNode, TEdge>> _Outputs = new List<GraphEdge<TNode, TEdge>>();
        public IReadOnlyList<GraphEdge<TNode, TEdge>> Outputs => _Outputs;

        public virtual TNode Value { get; set; }

        protected internal GraphNode(GraphSubgraph<TNode, TEdge> subgraph)
            : this(subgraph, default) { }
        protected internal GraphNode(GraphSubgraph<TNode, TEdge> subgraph, TNode value)
        {
            Subgraph = subgraph;
            Value = value;
        }

        public virtual void Connect(TNode value, GraphEdgeDirection direction = GraphEdgeDirection.OUTPUT)
        {
            GraphNode<TNode, TEdge> node = new GraphNode<TNode, TEdge>(Subgraph, value);
            Connect(node, direction);
        }
        public virtual void Connect(GraphNode<TNode, TEdge> node, GraphEdgeDirection direction = GraphEdgeDirection.OUTPUT)
        {
            if (node.Subgraph != Subgraph)
            {
                return;
            }

            if (direction.HasFlag(GraphEdgeDirection.INPUT))
            {
                GraphEdge<TNode, TEdge> edge = new GraphEdge<TNode, TEdge>(node, this);
                _Inputs.Add(edge);
                node._Outputs.Add(edge);
            }
            if (direction.HasFlag(GraphEdgeDirection.OUTPUT))
            {
                GraphEdge<TNode, TEdge> edge = new GraphEdge<TNode, TEdge>(this, node);
                _Outputs.Add(edge);
                node._Inputs.Add(edge);
            }
        }

        public virtual void Disconnect(GraphNode<TNode, TEdge> node, GraphEdgeDirection direction = GraphEdgeDirection.BOTH)
        {
            if (direction.HasFlag(GraphEdgeDirection.INPUT))
            {
                for (int inputIndex = 0; inputIndex < _Inputs.Count; inputIndex++)
                {
                    if (_Inputs[inputIndex].Output == node)
                    {
                        node._Outputs.Remove(_Inputs[inputIndex]);
                        _Inputs.RemoveAt(inputIndex--);
                    }
                }
            }
            if (direction.HasFlag(GraphEdgeDirection.OUTPUT))
            {
                for (int outputIndex = 0; outputIndex < _Outputs.Count; outputIndex++)
                {
                    if (_Outputs[outputIndex].Input == node)
                    {
                        node._Inputs.Remove(_Outputs[outputIndex]);
                        _Outputs.RemoveAt(outputIndex--);
                    }
                }
            }
        }
    }
}
