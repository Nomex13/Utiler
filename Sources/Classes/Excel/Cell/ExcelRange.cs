﻿#if !UNITY_ENGINE

using System;
using System.Collections.Generic;
using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public class ExcelRange
        {
            public int RowStart { get; private set; }
            public int RowEnd { get; private set; }
            public int ColumnStart { get; private set; }
            public int ColumnEnd { get; private set; }
            private string _AlphanumericIndexStart;
            public string AlphanumericIndexStart
            {
                get
                {
                    return _AlphanumericIndexStart ?? (_AlphanumericIndexStart = ExcelCell.GetAlphanumericIndex(RowStart, ColumnStart));
                }
            }
            private string _AlphanumericIndexEnd;
            public string AlphanumericIndexEnd
            {
                get
                {
                    return _AlphanumericIndexEnd ?? (_AlphanumericIndexEnd = ExcelCell.GetAlphanumericIndex(RowEnd, ColumnEnd));
                }
            }
            public string Text
            {
                get
                {
                    return Cell.Text;
                }
                set
                {
                    Cell.Text = value;
                }
            }
            public FontStyle FontStyle
            {
                get
                {
                    return Cell.FontStyle;
                }
                set
                {
                    Cell.FontStyle = value;
                }
            }
            public string FontFamily
            {
                get
                {
                    return Cell.FontFamily;
                }
                set
                {
                    Cell.FontFamily = value;
                }
            }
            public int FontSize
            {
                get
                {
                    return Cell.FontSize;
                }
                set
                {
                    Cell.FontSize = value;
                }
            }
            public TextAlignmentHorizontal AlignmentHorizontal
            {
                get
                {
                    return Cell.AlignmentHorizontal;
                }
                set
                {
                    Cell.AlignmentHorizontal = value;
                }
            }
            public TextAlignmentVertical AlignmentVertical
            {
                get
                {
                    return Cell.AlignmentVertical;
                }
                set
                {
                    Cell.AlignmentVertical = value;
                }
            }
            public bool Wrap
            {
                get
                {
                    return Cell.Wrap;
                }
                set
                {
                    Cell.Wrap = value;
                }
            }
            public Color Foreground
            {
                get
                {
                    return Cell.Foreground;
                }
                set
                {
                    Cell.Foreground = value;
                }
            }
            public Color BackgroundColor
            {
                get
                {
                    return Cell.BackgroundColor;
                }
                set
                {
                    Cell.BackgroundColor = value;
                }
            }
            public ExcelBackgroundPattern BackgroundPattern
            {
                get
                {
                    return Cell.BackgroundPattern;
                }
                set
                {
                    Cell.BackgroundPattern = value;
                }
            }
            public ExcelBorderLine Border
            {
                set
                {
                    for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                    {
                        Sheet[rowIndex, ColumnStart].BorderLeft = value;
                        Sheet[rowIndex, ColumnEnd].BorderRight = value;
                    }
                    for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                    {
                        Sheet[RowStart, columnIndex].BorderTop = value;
                        Sheet[RowEnd, columnIndex].BorderBottom = value;
                    }
                }
            }
            public ExcelBorderLine BorderLeft
            {
                set
                {
                    for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                    {
                        Sheet[rowIndex, ColumnStart].BorderLeft = value;
                    }
                }
            }
            public ExcelBorderLine BorderTop
            {
                set
                {
                    for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                    {
                        Sheet[RowStart, columnIndex].BorderTop = value;
                    }
                }
            }
            public ExcelBorderLine BorderRight
            {
                set
                {
                    for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                    {
                        Sheet[rowIndex, ColumnEnd].BorderRight = value;
                    }
                }
            }
            public ExcelBorderLine BorderBottom
            {
                set
                {
                    for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                    {
                        Sheet[RowEnd, columnIndex].BorderBottom = value;
                    }
                }
            }
            //public int Width { get; }
            //public int Height { get; }
            public ExcelSheet Sheet { get; }
            internal ExcelRange(ExcelSheet sheet, int rowStart, int columnStart, int rowEnd, int columnEnd)
            {
                Sheet = sheet;

                if (rowStart < 0)
                {
                    throw new ArgumentException($"The row start cannot be negative.", nameof(rowStart));
                }
                if (columnStart < 0)
                {
                    throw new ArgumentException($"The column start cannot be negative.", nameof(columnStart));
                }
                if (rowEnd < 0)
                {
                    throw new ArgumentException($"The row end cannot be negative.", nameof(rowEnd));
                }
                if (columnEnd < 0)
                {
                    throw new ArgumentException($"The column end cannot be negative.", nameof(columnEnd));
                }
                if (rowStart > rowEnd)
                {
                    throw new ArgumentException($"The row start should be lower or equal to the row end, but the row start {rowStart} is greater than the row end of {rowEnd}.");
                }
                if (columnStart > columnEnd)
                {
                    throw new ArgumentException($"The column start should be lower or equal to the row end, but the column start {columnStart} is greater than the column end of {columnEnd}.");
                }

                RowStart = rowStart;
                ColumnStart = columnStart;
                RowEnd = rowEnd;
                ColumnEnd = columnEnd;
                //Width = ColumnEnd - ColumnStart + 1;
                //Height = RowEnd - RowStart + 1;
            }
            // The main cell that defines the text and its style
            // Borders are defined separately
            private ExcelCell Cell
            {
                get
                {
                    return Sheet[RowStart, ColumnStart];
                }
            }
            private IEnumerable<ExcelCell> Cells
            {
                get
                {
                    for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                    {
                        //if (!Sheet.RowToColumnToCell.TryGetValue(rowIndex, out Dictionary<int, ExcelCell> ColumnToCell))
                        //{
                        //    continue;
                        //}
                        for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                        {
                            //if (!ColumnToCell.TryGetValue(columnIndex, out ExcelCell cell))
                            //{
                            //    continue;
                            //}
                            //yield return cell;
                            yield return Sheet[rowIndex, columnIndex];
                        }
                    }
                }
            }
            public ExcelRange SetBorder(ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnStart].SetBorderLeft(style);
                    Sheet[rowIndex, ColumnEnd].SetBorderRight(style);
                }
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowStart, columnIndex].SetBorderTop(style);
                    Sheet[RowEnd, columnIndex].SetBorderBottom(style);
                }

                return this;
            }
            public ExcelRange SetBorder(Color color, ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnStart].SetBorderLeft(color, style);
                    Sheet[rowIndex, ColumnEnd].SetBorderRight(color, style);
                }
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowStart, columnIndex].SetBorderTop(color, style);
                    Sheet[RowEnd, columnIndex].SetBorderBottom(color, style);
                }

                //for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                //{
                //    Sheet.SetBorderVertical(rowIndex, ColumnStart, color, style);
                //    Sheet.SetBorderVertical(rowIndex, ColumnEnd + 1, color, style);
                //}
                //for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                //{
                //    Sheet.SetBorderHorizontal(RowStart, columnIndex, color, style);
                //    Sheet.SetBorderHorizontal(RowEnd + 1, columnIndex, color, style);
                //}

                return this;
            }
            public ExcelRange SetBorderLeft(ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnStart].SetBorderLeft(style);
                }

                return this;
            }
            public ExcelRange SetBorderLeft(Color color, ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnStart].SetBorderLeft(color, style);
                }

                return this;
            }
            public ExcelRange SetBorderTop(ExcelBorderLineStyle style)
            {
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowStart, columnIndex].SetBorderTop(style);
                }

                return this;
            }
            public ExcelRange SetBorderTop(Color color, ExcelBorderLineStyle style)
            {
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowStart, columnIndex].SetBorderTop(color, style);
                }

                return this;
            }
            public ExcelRange SetBorderRight(ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnEnd].SetBorderRight(style);
                }

                return this;
            }
            public ExcelRange SetBorderRight(Color color, ExcelBorderLineStyle style)
            {
                for (int rowIndex = RowStart; rowIndex <= RowEnd; rowIndex++)
                {
                    Sheet[rowIndex, ColumnEnd].SetBorderRight(color, style);
                }

                return this;
            }
            public ExcelRange SetBorderBottom(ExcelBorderLineStyle style)
            {
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowEnd, columnIndex].SetBorderBottom(style);
                }

                return this;
            }
            public ExcelRange SetBorderBottom(Color color, ExcelBorderLineStyle style)
            {
                for (int columnIndex = ColumnStart; columnIndex <= ColumnEnd; columnIndex++)
                {
                    Sheet[RowEnd, columnIndex].SetBorderBottom(color, style);
                }

                return this;
            }
            public ExcelRange Merge()
            {
                HashSet<ExcelRange> uniteContained    = new HashSet<ExcelRange>();
                HashSet<ExcelRange> uniteHorizontally = new HashSet<ExcelRange>();
                HashSet<ExcelRange> uniteVertically   = new HashSet<ExcelRange>();

                // Check existing merges for conflicts
                foreach (ExcelCell cell in Cells)
                {
                    // If cell is already merged
                    if (cell.Merge != null)
                    {
                        // If the existing merge can be united with this one -- stack it for later bulk merging
                        if (Contains(cell.Merge))
                        {
                            uniteContained.Add(cell.Merge);
                        }
                        else if (UnitableHorizontally(cell.Merge))
                        {
                            uniteHorizontally.Add(cell.Merge);
                        }
                        else if (UnitableVertically(cell.Merge))
                        {
                            uniteVertically.Add(cell.Merge);
                        }
                        // If the existing merge can be united with this one -- remove it
                        else
                        {
                            throw new Exception($"Cannot merge {this} because it intersects with an existing merging {cell.Merge} and cannot be united with it.");
                            //cell.Merge.Unmerge();
                        }
                    }
                }

                // Check only one merging direction is presented
                if (uniteHorizontally.Count > 0 && uniteVertically.Count > 0)
                {
                    throw new Exception($"Cannot merge {this} because it cannot be simultaneously united horizontally with {String.Join(", ", uniteHorizontally)} and vertically with {String.Join(", ", uniteVertically)}.");
                }

                // Unite the ranges
                foreach (ExcelRange merge in uniteContained)
                {
                    Unite(merge);
                    Sheet.Merges.Remove(merge);
                }
                foreach (ExcelRange merge in uniteHorizontally)
                {
                    Unite(merge);
                    Sheet.Merges.Remove(merge);
                }
                foreach (ExcelRange merge in uniteVertically)
                {
                    Unite(merge);
                    Sheet.Merges.Remove(merge);
                }

                // Merge
                foreach (ExcelCell cell in Cells)
                {
                    cell.Merge = this;
                }

                Sheet.Merges.Add(this);

                return this;
            }
            public ExcelRange Unmerge()
            {
                foreach (ExcelCell cell in Cells)
                {
                    cell.Merge = null;
                }

                Sheet.Merges.Remove(this);

                return this;
            }
            public bool Contains(ExcelCell cell)
            {
                return Contains(cell.Row, cell.Column);
            }
            public bool Contains(int row, int column)
            {
                return RowStart <= row && row <= RowEnd && ColumnStart <= column && column <= ColumnEnd;
            }
            public bool Contains(ExcelRange merge)
            {
                return RowStart <= merge.RowStart && merge.RowEnd <= RowEnd && ColumnStart <= merge.ColumnStart && merge.ColumnEnd <= ColumnEnd;
            }
            public bool Intersects(ExcelRange range)
            {
                return !(RowStart > range.RowEnd || RowEnd < range.RowStart || ColumnStart > range.ColumnEnd || ColumnEnd < range.ColumnStart);
            }
            public bool Unitable(ExcelRange range)
            {
                return
                    (RowStart    == range.RowStart    && RowEnd    == range.RowEnd    && ColumnStart <= range.ColumnEnd && range.ColumnStart <= ColumnEnd) ||//!(ColumnStart > merge.ColumnEnd || merge.ColumnStart > ColumnEnd)) ||
                    (ColumnStart == range.ColumnStart && ColumnEnd == range.ColumnEnd && RowStart    <= range.RowEnd    && range.RowStart    <= RowEnd);// !(RowStart > merge.RowEnd || merge.RowStart > RowEnd));
            }
            public bool UnitableHorizontally(ExcelRange range)
            {
                return RowStart    == range.RowStart    && RowEnd    == range.RowEnd    && ColumnStart <= range.ColumnEnd && range.ColumnStart <= ColumnEnd;
            }
            public bool UnitableVertically(ExcelRange range)
            {
                return ColumnStart == range.ColumnStart && ColumnEnd == range.ColumnEnd && RowStart    <= range.RowEnd    && range.RowStart    <= RowEnd;
            }
            public void Unite(ExcelRange range)
            {
                RowStart    = Math.Min(RowStart,    range.RowStart);
                RowEnd      = Math.Max(RowEnd,      range.RowEnd);
                ColumnStart = Math.Min(ColumnStart, range.ColumnStart);
                ColumnEnd   = Math.Max(ColumnEnd,   range.ColumnEnd);

                //if (RowStart == merge.RowStart && RowEnd == merge.RowEnd)
                //{
                //    if (ColumnStart <= merge.ColumnEnd && merge.ColumnStart <= ColumnEnd)
                //    {
                //    }
                //    else if (RowStart <= merge.RowEnd && merge.RowStart <= RowEnd)
                //    {
                //    }
                //    else
                //    {
                //        throw new Exception($"Cannot unite {this} merge with {merge} merge.");
                //    }
                //}
                //else if (ColumnStart == merge.ColumnStart && ColumnEnd == merge.ColumnEnd)
                //{
                //}
                //else
                //{
                //    throw new Exception($"Cannot unite {this} merge with {merge} merge.");
                //}
            }
            public override string ToString()
            {
                return $"{ExcelCell.GetAlphanumericIndex(RowStart, ColumnStart)}:{ExcelCell.GetAlphanumericIndex(RowEnd, ColumnEnd)}";
            }
        }
    }
}


#endif