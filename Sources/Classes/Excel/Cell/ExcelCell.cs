﻿#if !UNITY_ENGINE

using System;
using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        //public class ExcelCellInternal : ExcelCell
        //{

        //    //public ExcelCellInternal(ExcelSheet sheet, int row, int column)
        //    //    : base(sheet, row, column)
        //    //{
        //    //    ;
        //    //}

        //}
        public class ExcelCell
        {
            //internal int TextSharedIndex { get; set; }
            //internal int StyleSharedIndex { get; set; }

            // Contents
            private ExcelSharedString _SharedText;
            internal ExcelSharedString SharedText
            {
                get
                {
                    if (_Text == null)
                    {
                        return null;
                    }
                    return _SharedText ?? (_SharedText = Sheet.Book.GetSharedText(_Text));
                }
                set
                {
                    _SharedText = value;
                }
            }
            private string _Text;
            public string Text
            {
                get
                {
                    return _Text;
                }
                set
                {
                    if (_Text == value)
                    {
                        return;
                    }
                    _Text = value;
                    if (SharedText != null)
                    {
                        Sheet.Book.UngetSharedText(SharedText);
                    }
                    SharedText = null;
                }
            }
            //public string Formula { get; set; }
            private ExcelSharedStyle _SharedStyle;
            internal ExcelSharedStyle SharedStyle
            {
                get
                {
                    //if (_Text == null)
                    //{
                    //    return null;
                    //}
                    return _SharedStyle ?? (_SharedStyle = Sheet.Book.GetSharedStyle(SharedFont, SharedBackground, SharedBorder));
                }
                set
                {
                    if (_SharedStyle == value)
                    {
                        return;
                    }
                    if (_SharedStyle != null)
                    {
                        Sheet.Book.UngetSharedStyle(_SharedStyle);
                    }
                    _SharedStyle = value;
                }
            }

            // Font
            private ExcelSharedFont _SharedFont;
            internal ExcelSharedFont SharedFont
            {
                get
                {
                    return _SharedFont ?? (_SharedFont = Sheet.Book.GetSharedFont(FontFamily, FontSize, Foreground, FontStyle, AlignmentHorizontal, AlignmentVertical, Wrap));
                }
                set
                {
                    if (_SharedFont == value)
                    {
                        return;
                    }
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    _SharedFont = value;
                    SharedStyle = null;
                }
            }
            public FontStyle _FontStyle;
            public FontStyle FontStyle
            {
                get
                {
                    return _FontStyle;
                }
                set
                {
                    if (_FontStyle == value)
                    {
                        return;
                    }
                    _FontStyle = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            //{
            //    get
            //    {
            //        return Font.Style;
            //    }
            //    set
            //    {
            //        Font.Counter--;
            //        if (Font.Counter <= 0)
            //        {
            //            Sheet.Book.RemoveSharedFont(Font);
            //        }
            //        Font = Sheet.Book.GetSharedFont(FontFamily, FontSize, Foreground, FontStyle, AlignmentHorizontal, AlignmentVertical, Wrap);
            //        Font.Counter++;
            //    }
            //}
            public string _FontFamily = "Times New Roman";
            public string FontFamily
            {
                get
                {
                    return _FontFamily;
                }
                set
                {
                    if (_FontFamily == value)
                    {
                        return;
                    }
                    _FontFamily = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            public int _FontSize = 16;
            public int FontSize
            {
                get
                {
                    return _FontSize;
                }
                set
                {
                    if (_FontSize == value)
                    {
                        return;
                    }
                    _FontSize = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            public TextAlignmentHorizontal _AlignmentHorizontal = TextAlignmentHorizontal.LEFT;
            public TextAlignmentHorizontal AlignmentHorizontal
            {
                get
                {
                    return _AlignmentHorizontal;
                }
                set
                {
                    if (_AlignmentHorizontal == value)
                    {
                        return;
                    }
                    _AlignmentHorizontal = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            public TextAlignmentVertical _AlignmentVertical = TextAlignmentVertical.TOP;
            public TextAlignmentVertical AlignmentVertical
            {
                get
                {
                    return _AlignmentVertical;
                }
                set
                {
                    if (_AlignmentVertical == value)
                    {
                        return;
                    }
                    _AlignmentVertical = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            public bool _Wrap = false;
            public bool Wrap
            {
                get
                {
                    return _Wrap;
                }
                set
                {
                    if (_Wrap == value)
                    {
                        return;
                    }
                    _Wrap = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            public Color _Foreground = Color.Black;
            public Color Foreground
            {
                get
                {
                    return _Foreground;
                }
                set
                {
                    if (_Foreground == value)
                    {
                        return;
                    }
                    _Foreground = value;
                    if (_SharedFont != null)
                    {
                        Sheet.Book.UngetSharedFont(_SharedFont);
                    }
                    SharedFont = null;
                }
            }
            // Background
            private ExcelSharedBackground _SharedBackground;
            internal ExcelSharedBackground SharedBackground
            {
                get
                {
                    return _SharedBackground ?? (_SharedBackground = Sheet.Book.GetSharedBackground(BackgroundColor, BackgroundPattern));
                }
                set
                {
                    if (_SharedBackground == value)
                    {
                        return;
                    }
                    if (_SharedBackground != null)
                    {
                        Sheet.Book.UngetSharedBackground(_SharedBackground);
                    }
                    _SharedBackground = value;
                    SharedStyle = null;
                }
            }
            public Color _BackgroundColor;
            public Color BackgroundColor
            {
                get
                {
                    return _BackgroundColor;
                }
                set
                {
                    if (_BackgroundColor == value)
                    {
                        return;
                    }

                    // If color is set with no pattern -- set the pattern to the simpliest one
                    if (_BackgroundColor == Color.Transparent && value != Color.Transparent && _BackgroundPattern == ExcelBackgroundPattern.NONE)
                    {
                        _BackgroundPattern = ExcelBackgroundPattern.SOLID;
                    }

                    _BackgroundColor = value;
                    SharedBackground = null;
                }
            }
            public ExcelBackgroundPattern _BackgroundPattern;
            public ExcelBackgroundPattern BackgroundPattern
            {
                get
                {
                    return _BackgroundPattern;
                }
                set
                {
                    if (_BackgroundPattern == value)
                    {
                        return;
                    }
                    _BackgroundPattern = value;
                    SharedBackground = null;
                }
            }
            // Borders

            private ExcelSharedBorder _SharedBorder;
            internal ExcelSharedBorder SharedBorder
            {
                get
                {
                    return _SharedBorder ?? (_SharedBorder = Sheet.Book.GetSharedBorder(BorderLeft, BorderTop, BorderRight, BorderBottom));
                }
                set
                {
                    if (_SharedBorder == value)
                    {
                        return;
                    }
                    if (_SharedBorder != null)
                    {
                        Sheet.Book.UngetSharedBorder(_SharedBorder);
                    }
                    _SharedBorder = value;
                    SharedStyle = null;
                }
            }
            /// <summary>
            /// Set All 4 borders. Assign null to unset.
            /// </summary>
            public ExcelBorderLine Border
            {
                set
                {
                    BorderLeft = value;
                    BorderTop = value;
                    BorderRight = value;
                    BorderBottom = value;
                }
            }
            private ExcelBorderLine _BorderLeft;
            public ExcelBorderLine BorderLeft
            {
                get
                {
                    return _BorderLeft;// ?? (_BorderLeft = Sheet.TryBorderVertical(Row, Column));
                }
                set
                {
                    if (_BorderLeft == value)
                    {
                        return;
                    }

                    _BorderLeft = value;
                    SharedBorder = null;

                    if (Column > 0)
                    {
                        ExcelCell cellLeft = Sheet.TryCell(Row, Column - 1);
                        if (cellLeft != null)
                        {
                            cellLeft._BorderRight = value;
                            cellLeft.SharedBorder = null;
                        }
                    }
                }
            }
            private ExcelBorderLine _BorderTop;
            public ExcelBorderLine BorderTop
            {
                get
                {
                    return _BorderTop;// ?? (_BorderTop = Sheet.GetBorderHorizontal(Row, Column));
                }
                set
                {
                    if (_BorderTop == value)
                    {
                        return;
                    }

                    _BorderTop = value;
                    SharedBorder = null;

                    if (Row > 0)
                    {
                        ExcelCell cellTop = Sheet.TryCell(Row - 1, Column);
                        if (cellTop != null)
                        {
                            cellTop._BorderBottom = value;
                            cellTop.SharedBorder = null;
                        }
                    }
                }
            }
            private ExcelBorderLine _BorderRight;
            public ExcelBorderLine BorderRight
            {
                get
                {
                    return _BorderRight;// ?? (_BorderRight = Sheet.GetBorderVertical(Row, Column + 1));
                }
                set
                {
                    if (_BorderRight == value)
                    {
                        return;
                    }

                    _BorderRight = value;
                    SharedBorder = null;

                    ExcelCell cellRight = Sheet.TryCell(Row, Column + 1);
                    if (cellRight != null)
                    {
                        cellRight._BorderLeft = value;
                        cellRight.SharedBorder = null;
                    }
                }
            }
            private ExcelBorderLine _BorderBottom;
            public ExcelBorderLine BorderBottom
            {
                get
                {
                    return _BorderBottom;// ?? (_BorderBottom = Sheet.GetBorderHorizontal(Row + 1, Column));
                }
                set
                {
                    if (_BorderBottom == value)
                    {
                        return;
                    }

                    _BorderBottom = value;
                    SharedBorder = null;

                    ExcelCell cellBottom = Sheet.TryCell(Row + 1, Column);
                    if (cellBottom != null)
                    {
                        cellBottom._BorderTop = value;
                        cellBottom.SharedBorder = null;
                    }
                }
            }
            //public ExcelBorderLine TryGetBorderLeft()
            //{
            //    //get
            //    //{
            //        return Sheet.TryBorderVertical(Row, Column);
            //    //}
            //}
            //public ExcelBorderLine TryGetBorderTop()
            //{
            //    //get
            //    //{
            //        return Sheet.TryBorderHorizontal(Row, Column);
            //    //}
            //}
            //public ExcelBorderLine TryGetBorderRight()
            //{
            //    //get
            //    //{
            //        return Sheet.TryBorderVertical(Row, Column + 1);
            //    //}
            //}
            //public ExcelBorderLine TryGetBorderBottom()
            //{
            //    //get
            //    //{
            //        return Sheet.TryBorderHorizontal(Row, Column + 1);
            //    //}
            //}
            private ExcelRange _Merge;
            public ExcelRange Merge
            {
                get
                {
                    return _Merge;// Sheet.GetMerge(Row, Column);
                }
                internal set
                {
                    _Merge = value;
                }
            }

            public int Row { get; }
            public int Column { get; }
            private string _AlphanumericIndex;
            public string AlphanumericIndex
            {
                get
                {
                    return _AlphanumericIndex ?? (_AlphanumericIndex = GetAlphanumericIndex(Row, Column));
                }
            }
            //protected ExcelCellMerge _Merge { get; }
            public ExcelSheet Sheet { get; }

            internal ExcelCell(ExcelSheet sheet, int row, int column)
            {
                Sheet = sheet;
                Row = row;
                Column = column;

                _SharedFont = Sheet.SharedFontDefault;
                _SharedBackground = Sheet.SharedBackgroundDefault;
                _SharedBorder = null; // This cells intersect with other cells around it that may have their own borders already specified.
                _SharedStyle = null;  // Border is defined by the cells around, so style may be not the default one

                _SharedFont.Counter++;
                _SharedBackground.Counter++;

                _FontFamily = _SharedFont.Family;
                _FontSize = _SharedFont.Size;
                _FontStyle = _SharedFont.Style;
                _Foreground = _SharedFont.Color;
                _AlignmentHorizontal = _SharedFont.AlignmentHorizontal;
                _AlignmentVertical = _SharedFont.AlignmentVertical;
                _Wrap = _SharedFont.Wrap;

                _BackgroundColor = _SharedBackground.Color;
                _BackgroundPattern = _SharedBackground.Pattern;

                if (Column > 0)
                {
                    ExcelCell cellLeft = Sheet.TryCell(Row, Column - 1);
                    if (cellLeft != null)
                    {
                        _BorderLeft = cellLeft._BorderRight;
                    }
                }
                if (Row > 0)
                {
                    ExcelCell cellTop = Sheet.TryCell(Row - 1, Column);
                    if (cellTop != null)
                    {
                        _BorderTop = cellTop._BorderBottom;
                    }
                }
                ExcelCell cellRight = Sheet.TryCell(Row, Column + 1);
                if (cellRight != null)
                {
                    _BorderRight = cellRight._BorderLeft;
                }
                ExcelCell cellBottom = Sheet.TryCell(Row + 1, Column);
                if (cellBottom != null)
                {
                    _BorderBottom = cellBottom._BorderTop;
                }

                //_BorderLeft = Sheet.GetBorderVertical(Row, Column);
                //_BorderTop = Sheet.GetBorderHorizontal(Row, Column);
                //_BorderRight = Sheet.GetBorderVertical(Row, Column + 1);
                //_BorderBottom = Sheet.GetBorderHorizontal(Row + 1, Column);
            }
            //public ExcelBorder GetBorder()
            //{
            //    return new ExcelBorder(GetBorderLeft(), GetBorderTop(), GetBorderRight(), GetBorderBottom());
            //}
            //public ExcelBorder TryGetBorder()
            //{
            //    return new ExcelBorder(TryGetBorderLeft(), TryGetBorderTop(), TryGetBorderRight(), TryGetBorderBottom());
            //}
            public float GetWidth()
            {
                return Sheet.GetWidth(Column);
            }
            public float GetHeight()
            {
                return Sheet.GetHeight(Row);
            }
            public void SetBackground(Color color, ExcelBackgroundPattern pattern)
            {
                _BackgroundColor = color;
                _BackgroundPattern = pattern;
                SharedBackground = null;
            }
            public void SetBorder(ExcelBorderLineStyle style)
            {
                SetBorderLeft(style);
                SetBorderTop(style);
                SetBorderRight(style);
                SetBorderBottom(style);
            }
            public void SetBorderLeft(ExcelBorderLineStyle style)
            {
                BorderLeft = new ExcelBorderLine(BorderLeft != null ? BorderLeft.Color : ExcelBorderLine.ColorDefault, style);
            }
            public void SetBorderTop(ExcelBorderLineStyle style)
            {
                BorderTop = new ExcelBorderLine(BorderTop != null ? BorderTop.Color : ExcelBorderLine.ColorDefault, style);
            }
            public void SetBorderRight(ExcelBorderLineStyle style)
            {
                BorderRight = new ExcelBorderLine(BorderRight != null ? BorderRight.Color : ExcelBorderLine.ColorDefault, style);
            }
            public void SetBorderBottom(ExcelBorderLineStyle style)
            {
                BorderBottom = new ExcelBorderLine(BorderBottom != null ? BorderBottom.Color : ExcelBorderLine.ColorDefault, style);
            }
            public void SetBorder(Color color)
            {
                SetBorderLeft(color);
                SetBorderTop(color);
                SetBorderRight(color);
                SetBorderBottom(color);
            }
            public void SetBorderLeft(Color color)
            {
                BorderLeft = new ExcelBorderLine(color, BorderLeft != null ? BorderLeft.Style : ExcelBorderLine.StyleDefault);
            }
            public void SetBorderTop(Color color)
            {
                BorderTop = new ExcelBorderLine(color, BorderTop != null ? BorderTop.Style : ExcelBorderLine.StyleDefault);
            }
            public void SetBorderRight(Color color)
            {
                BorderRight = new ExcelBorderLine(color, BorderRight != null ? BorderRight.Style : ExcelBorderLine.StyleDefault);
            }
            public void SetBorderBottom(Color color)
            {
                BorderBottom = new ExcelBorderLine(color, BorderBottom != null ? BorderBottom.Style : ExcelBorderLine.StyleDefault);
            }
            public void SetBorder(Color color, ExcelBorderLineStyle style)
            {
                SetBorderLeft(color, style);
                SetBorderTop(color, style);
                SetBorderRight(color, style);
                SetBorderBottom(color, style);
            }
            public void SetBorderLeft(Color color, ExcelBorderLineStyle style)
            {
                BorderLeft = new ExcelBorderLine(color, style);
            }
            public void SetBorderTop(Color color, ExcelBorderLineStyle style)
            {
                BorderTop = new ExcelBorderLine(color, style);
            }
            public void SetBorderRight(Color color, ExcelBorderLineStyle style)
            {
                BorderRight = new ExcelBorderLine(color, style);
            }
            public void SetBorderBottom(Color color, ExcelBorderLineStyle style)
            {
                BorderBottom = new ExcelBorderLine(color, style);
            }
            //public void SetBorderLeft(ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine left = BorderLeft;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        left.Style = style;
            //    }
            //}
            //public void SetBorderLeft(Color color, ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine left = BorderLeft;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        left.Color = color;
            //        left.Style = style;
            //    }
            //}
            //public void SetBorderTop(ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine top = BorderTop;

            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        top.Style = style;
            //    }
            //}
            //public void SetBorderTop(Color color, ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine top = BorderTop;

            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        top.Color = color;
            //        top.Style = style;
            //    }
            //}
            //public void SetBorderRight(ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine right = BorderRight;

            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        right.Style = style;
            //    }
            //}
            //public void SetBorderRight(Color color, ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine right = BorderRight;

            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        right.Color = color;
            //        right.Style = style;
            //    }
            //}
            //public void SetBorderBottom(ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        bottom.Style = style;
            //    }
            //}
            //public void SetBorderBottom(Color color, ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        bottom.Color = color;
            //        bottom.Style = style;
            //    }
            //}
            //public void SetBorder(ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine left   = BorderLeft;
            //    ExcelBorderLine top    = BorderTop;
            //    ExcelBorderLine right  = BorderRight;
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        left.Style = style;
            //    }
            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        top.Style = style;
            //    }
            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        right.Style = style;
            //    }
            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(Color.Black, style);
            //    }
            //    else
            //    {
            //        bottom.Style = style;
            //    }
            //}
            //public void SetBorder(Color color, ExcelBorderLineStyle style)
            //{
            //    ExcelBorderLine left   = BorderLeft;
            //    ExcelBorderLine top    = BorderTop;
            //    ExcelBorderLine right  = BorderRight;
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        left.Color = color;
            //        left.Style = style;
            //    }
            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        top.Color = color;
            //        top.Style = style;
            //    }
            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        right.Color = color;
            //        right.Style = style;
            //    }
            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(color, style);
            //    }
            //    else
            //    {
            //        bottom.Color = color;
            //        bottom.Style = style;
            //    }
            //}
            //public void SetBorder(Color colorHorizontal, ExcelBorderLineStyle styleHorizontal, Color colorVertical, ExcelBorderLineStyle styleVertical)
            //{
            //    ExcelBorderLine left   = BorderLeft;
            //    ExcelBorderLine top    = BorderTop;
            //    ExcelBorderLine right  = BorderRight;
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(colorVertical, styleVertical);
            //    }
            //    else
            //    {
            //        left.Color = colorVertical;
            //        left.Style = styleVertical;
            //    }
            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(colorHorizontal, styleHorizontal);
            //    }
            //    else
            //    {
            //        top.Color = colorHorizontal;
            //        top.Style = styleHorizontal;
            //    }
            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(colorVertical, styleVertical);
            //    }
            //    else
            //    {
            //        right.Color = colorVertical;
            //        right.Style = styleVertical;
            //    }
            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(colorHorizontal, styleHorizontal);
            //    }
            //    else
            //    {
            //        bottom.Color = colorHorizontal;
            //        bottom.Style = styleHorizontal;
            //    }
            //}
            //public void SetBorder(Color colorLeft, ExcelBorderLineStyle styleLeft, Color colorTop, ExcelBorderLineStyle styleTop, Color colorRight, ExcelBorderLineStyle styleRight, Color colorBottom, ExcelBorderLineStyle styleBottom)
            //{
            //    ExcelBorderLine left   = BorderLeft;
            //    ExcelBorderLine top    = BorderTop;
            //    ExcelBorderLine right  = BorderRight;
            //    ExcelBorderLine bottom = BorderBottom;

            //    if (left == null)
            //    {
            //        BorderLeft = new ExcelBorderLine(colorLeft, styleLeft);
            //    }
            //    else
            //    {
            //        left.Color = colorLeft;
            //        left.Style = styleLeft;
            //    }
            //    if (top == null)
            //    {
            //        BorderTop = new ExcelBorderLine(colorTop, styleTop);
            //    }
            //    else
            //    {
            //        top.Color = colorTop;
            //        top.Style = styleTop;
            //    }
            //    if (right == null)
            //    {
            //        BorderRight = new ExcelBorderLine(colorRight, styleRight);
            //    }
            //    else
            //    {
            //        right.Color = colorRight;
            //        right.Style = styleRight;
            //    }
            //    if (bottom == null)
            //    {
            //        BorderBottom = new ExcelBorderLine(colorBottom, styleBottom);
            //    }
            //    else
            //    {
            //        bottom.Color = colorBottom;
            //        bottom.Style = styleBottom;
            //    }
            //}
            public static string GetAlphanumericIndex(int row, int column)
            {
                if (row < 0)
                {
                    throw new ArgumentException($"The row cannot be negative.", nameof(row));
                }
                if (column < 0)
                {
                    throw new ArgumentException($"The column cannot be negative.", nameof(column));
                }

                const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string index = "";
                do
                {
                    index = alphabet[column % alphabet.Length] + index;
                    column /= alphabet.Length;
                    column--;
                } while (column >= 0);
                index += (row + 1);
                return index;
                //if (column < alphabet.Length)
                //{
                //    return $"{alphabet[column]}";
                //}

                //StringBuilder stringBuilder = new StringBuilder(4);

                //while (column != 0)
                //{
                //    stringBuilder.Append(alphabet[column % alphabet.Length]);
                //    column /= alphabet.Length;
                //}

                //return stringBuilder.ToString();
            }
        }
    }
}

#endif