﻿#if !UNITY_ENGINE

using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public class ExcelBorderLine
        {
            public Color Color { get; set; }
            public ExcelBorderLineStyle Style { get; set; }

            public static readonly Color ColorDefault = Color.Black;
            public static readonly ExcelBorderLineStyle StyleDefault = ExcelBorderLineStyle.THIN;

            //public ExcelSheet Sheet { get; }
            //public int Row { get; }
            //public int Column { get; }
            public ExcelBorderLine()
                : this (ColorDefault, StyleDefault) { }
            public ExcelBorderLine(ExcelBorderLineStyle style)
                : this (ColorDefault, style) { }
            public ExcelBorderLine(Color color, ExcelBorderLineStyle style)
            {
                Style = style;
                Color = color;
            }
            //public ExcelBorderLine Clone()
            //{
            //    return new ExcelBorderLine(Color, Style);
            //}
            public static bool operator == (ExcelBorderLine one, ExcelBorderLine two)
            {
                if (ReferenceEquals(one, null))
                {
                    return ReferenceEquals(two, null);
                }
                else if (ReferenceEquals(two, null))
                {
                    return false;
                }

                return
                    one.Color == two.Color &&
                    one.Style == two.Style;
            }
            public static bool operator != (ExcelBorderLine one, ExcelBorderLine two)
            {
                return !(one == two);
            }
            public override bool Equals(object @object)
            {
                return @object is ExcelBorderLine && (ExcelBorderLine)@object == this;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public override string ToString()
            {
                return Style == ExcelBorderLineStyle.NONE ? $"{Style}" : $"{Style} (#{ToHexArgb(Color)})";
            }
            private static string ToHexArgb(Color color)
            {
                return color.A.ToString("X2") + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
            }
        }
    }
}


#endif