﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Iodynis.Libraries.Utilities
//{
//    public partial class Excel
//    {
//        public class ExcelCellTree
//        {
//            //private int[/*columns*/][/*rows*/] cells;

//            int root = 0;
//            int counter = 0;
//            int capacity = 16;
//            int[] lefts;
//            int[] rights;
//            int[] rows;
//            int[] columns;
//            int[] counts;
//            ExcelCell[] cells;

//            // Recycling
//            private int[] trash;
//            private int trashCounter = 0;

//            //private ExcellCellTreeNode Root;
//            public ExcelCellTree()
//            {
//                lefts = new int[capacity];
//                rights = new int[capacity];
//                counts = new int[capacity];
//                cells = new ExcelCell[capacity];
//            }
//            private void Expand()
//            {
//                int[] newLefts = new int[capacity];
//                int[] newRights = new int[capacity];
//                int[] newCounts = new int[capacity];
//                ExcelCell[] newCells = new ExcelCell[capacity];

//                lefts.CopyTo(newLefts, 0);
//                rights.CopyTo(newRights, 0);
//                counts.CopyTo(newCounts, 0);
//                cells.CopyTo(newCells, 0);

//                lefts = newLefts;
//                rights = newRights;
//                counts = newCounts;
//                cells = newCells;
//            }
//            public void Add(ExcelCell cell)
//            {
//                int endIndex;
//                // Check if can reuse memory
//                if (trashCounter > 0)
//                {
//                    endIndex = trash[--trashCounter];
//                }
//                else
//                {
//                    // Ensure there is enough space
//                    while (counter >= capacity)
//                    {
//                        Expand();
//                    }
//                    endIndex = counter++;
//                }

//                int currentIndex = root;

//                while (true)
//                {
//                    // Go left
//                    if (cell.Row < rows[currentIndex])
//                    {
//                        // Go
//                        if (lefts[currentIndex] != 0)
//                        {
//                            currentIndex = lefts[currentIndex];
//                        }
//                        // Nowhere to go
//                        else
//                        {
//                            lefts[currentIndex] = endIndex;
//                            break;
//                        }
//                    }
//                    // Go right
//                    else if (cell.Row > rows[currentIndex])
//                    {
//                        // Go
//                        if (rights[currentIndex] != 0)
//                        {
//                            currentIndex = rights[currentIndex];
//                        }
//                        // Nowhere to go
//                        else
//                        {
//                            rights[currentIndex] = endIndex;
//                            break;
//                        }
//                    }
//                    // On the spot
//                    else
//                    {
//                        break;
//                    }
//                }

//                cells[endIndex] = cell;
//            }
//            public void Remove(ExcelCell cell)
//            {
//                Remove(cell.Row, cell.Column);
//            }
//            public void Remove(int row, int column)
//            {
//                ;
//            }
//            public IEnumerable<ExcelCell> Range(int row, int column, int width, int height)
//            {
//                return null;
//            }
//        }
//    }
//}
