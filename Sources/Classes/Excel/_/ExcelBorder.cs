﻿//using System;
//using System.Collections.Generic;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Iodynis.Libraries.Utilities
//{
//    public partial class Excel
//    {
//        public class ExcelBorder
//        {
//            public ExcelBorderLine Left { get; set; }
//            public ExcelBorderLine Right { get; set; }
//            public ExcelBorderLine Top { get; set; }
//            public ExcelBorderLine Bottom { get; set; }
//            public ExcelBorder()
//            {
//                ;
//            }
//            public ExcelBorder(ExcelBorder border)
//            {
//                Left   = border.Left;
//                Top    = border.Top;
//                Right  = border.Right;
//                Bottom = border.Bottom;
//            }
//            public ExcelBorder(ExcelBorderLine left, ExcelBorderLine top, ExcelBorderLine right, ExcelBorderLine bottom)
//            {
//                Left   = left;
//                Top    = top;
//                Right  = right;
//                Bottom = bottom;
//            }
//            public static bool operator == (ExcelBorder one, ExcelBorder two)
//            {
//                if (ReferenceEquals(one, null))
//                {
//                    return ReferenceEquals(two, null);
//                }
//                else if (ReferenceEquals(two, null))
//                {
//                    return false;
//                }

//                return
//                    one.Left   == two.Left &&
//                    one.Top    == two.Top &&
//                    one.Right  == two.Right &&
//                    one.Bottom == two.Bottom;
//            }
//            public static bool operator != (ExcelBorder one, ExcelBorder two)
//            {
//                return !(one == two);
//            }
//            public override bool Equals(object @object)
//            {
//                return @object is ExcelBorder && (ExcelBorder)@object == this;
//            }
//        }
//    }
//}
