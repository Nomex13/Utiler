﻿#if !UNITY_ENGINE

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelSharedStyle : ExcelSharedItem
        {
            internal ExcelSharedFont Font;
            internal ExcelSharedBackground Background;
            internal ExcelSharedBorder Border;

            internal ExcelSharedStyle(ExcelSharedFont font, ExcelSharedBackground background, ExcelSharedBorder border)
            {
                Font = font;
                Background = background;
                Border = border;
            }
        }
    }
}

#endif