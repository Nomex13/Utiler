﻿#if !UNITY_ENGINE

using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public enum TextAlignmentHorizontal
        {
            GENERAL,
            LEFT,
            CENTER,
            RIGHT,
            FILL,
            CENTER_CONTINUOUS,
            JUSTIFY,
            DISTRIBUTED,
        }
        public enum TextAlignmentVertical
        {
            TOP,
            CENTER,
            BOTTOM,
            JUSTIFY,
            DISTRIBUTED,
        }

        internal class ExcelSharedFont : ExcelSharedItem
        {
            private Font _Font;
            internal Font Font
            {
                get
                {
                    return _Font ?? (_Font = new Font(Family, Size, Style, GraphicsUnit.Point));
                }
            }
            internal string Family;
            internal int Size;
            internal Color Color;
            internal FontStyle Style;
            internal TextAlignmentHorizontal AlignmentHorizontal;
            internal TextAlignmentVertical AlignmentVertical;
            internal bool Wrap;

            internal ExcelSharedFont(string family, int size, Color color, FontStyle style, TextAlignmentHorizontal alignmentHorizontal, TextAlignmentVertical alignmentVertical, bool wrap)
            {
                Family = family;
                Size = size;
                Color = color;
                Style = style;
                AlignmentHorizontal = alignmentHorizontal;
                AlignmentVertical = alignmentVertical;
                Wrap = wrap;
            }
            public static bool operator == (ExcelSharedFont one, ExcelSharedFont two)
            {
                if (ReferenceEquals(one, null))
                {
                    return ReferenceEquals(two, null);
                }
                else if (ReferenceEquals(two, null))
                {
                    return false;
                }

                return
                    one.Family == two.Family &&
                    one.Size == two.Size &&
                    one.Color == two.Color &&
                    one.Style == two.Style &&
                    one.AlignmentHorizontal == two.AlignmentHorizontal &&
                    one.AlignmentVertical == two.AlignmentVertical &&
                    one.Wrap == two.Wrap;
            }
            public static bool operator != (ExcelSharedFont one, ExcelSharedFont two)
            {
                return !(one == two);
            }
            public override bool Equals(object @object)
            {
                return @object is ExcelSharedFont && (ExcelSharedFont)@object == this;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public override string ToString()
            {
                return $"{Family}";
            }
        }
    }
}

#endif