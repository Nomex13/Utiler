﻿#if !UNITY_ENGINE

using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelSharedBorder : ExcelSharedItem
        {
            internal ExcelBorderLine Left { get; set; }
            internal ExcelBorderLine Right { get; set; }
            internal ExcelBorderLine Top { get; set; }
            internal ExcelBorderLine Bottom { get; set; }
            internal ExcelSharedBorder()
            {
                ;
            }
            internal ExcelSharedBorder(ExcelSharedBorder border)
            {
                Left   = border.Left;
                Top    = border.Top;
                Right  = border.Right;
                Bottom = border.Bottom;
            }
            internal ExcelSharedBorder(ExcelBorderLine left, ExcelBorderLine top, ExcelBorderLine right, ExcelBorderLine bottom)
            {
                Left   = left;
                Top    = top;
                Right  = right;
                Bottom = bottom;
            }
            public static bool operator == (ExcelSharedBorder one, ExcelSharedBorder two)
            {
                if (ReferenceEquals(one, null))
                {
                    return ReferenceEquals(two, null);
                }
                else if (ReferenceEquals(two, null))
                {
                    return false;
                }

                return
                    one.Left   == two.Left &&
                    one.Top    == two.Top &&
                    one.Right  == two.Right &&
                    one.Bottom == two.Bottom;
            }
            public static bool operator != (ExcelSharedBorder one, ExcelSharedBorder two)
            {
                return !(one == two);
            }
            public override bool Equals(object @object)
            {
                return @object is ExcelSharedBorder && (ExcelSharedBorder)@object == this;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public override string ToString()
            {
                return $"{Left}, {Top}, {Right}, {Bottom}";
            }
        }
    }
}

#endif