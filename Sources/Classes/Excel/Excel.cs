﻿#if !UNITY_ENGINE

using System;
using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public static partial class Excel
    {
        //public ExcelBook Book { get; }

        //public Excel()
        //{
        //    Book = new ExcelBook();
        //}
        public static ExcelBook Create()
        {
            return new ExcelBook();
        }
        public static ExcelBook Open(string path)
        {
            throw new NotImplementedException();
        }
        private static Graphics _MeasureStringGraphics;
        private static Graphics MeasureStringGraphics
        {
            get
            {
                if (_MeasureStringGraphics == null)
                {
                    Bitmap bitmap = new Bitmap(64, 64);
                    bitmap.SetResolution(96, 96);
                    _MeasureStringGraphics = Graphics.FromImage(bitmap);
                    _MeasureStringGraphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                }

                return _MeasureStringGraphics;
            }
        }
        private static SizeF MeasureStringInPixels(string @string, Font font)
        {
            return MeasureStringInPixels(@string, font, Int32.MaxValue - 100);
        }
        private static SizeF MeasureStringInPixels(string @string, Font font, int widthMaximumInPixels)//, out int charactersFitted, out int linesFilled)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            //Size textSize = TextRenderer.MeasureText(text, stringFont);
            //double width = (double)(((textSize.Width / (double)7) * 256) - (128 / 7)) / 256;
            //width = (double)decimal.Round((decimal)width + 0.2M, 2);

            return MeasureStringGraphics.MeasureString(@string, font, widthMaximumInPixels, StringFormat.GenericTypographic);//, out charactersFitted, out linesFitted);
        }
        private static string ToString(TextAlignmentHorizontal alignment)
        {
            switch (alignment)
            {
                case TextAlignmentHorizontal.LEFT:
                    return "left";
                case TextAlignmentHorizontal.CENTER:
                    return "center";
                case TextAlignmentHorizontal.CENTER_CONTINUOUS:
                    return "centerContinuous";
                case TextAlignmentHorizontal.RIGHT:
                    return "right";
                case TextAlignmentHorizontal.GENERAL:
                    return "general";
                case TextAlignmentHorizontal.DISTRIBUTED:
                    return "distributed";
                case TextAlignmentHorizontal.FILL:
                    return "fill";
                case TextAlignmentHorizontal.JUSTIFY:
                    return "justify";
                default:
                    throw new Exception($"Horizontal alignment {alignment} is not supported.");
            }
        }
        private static string ToString(TextAlignmentVertical alignment)
        {
            switch (alignment)
            {
                case TextAlignmentVertical.TOP:
                    return "top";
                case TextAlignmentVertical.CENTER:
                    return "center";
                case TextAlignmentVertical.BOTTOM:
                    return "bottom";
                case TextAlignmentVertical.DISTRIBUTED:
                    return "distributed";
                case TextAlignmentVertical.JUSTIFY:
                    return "justify";
                default:
                    throw new Exception($"Vertical alignment {alignment} is not supported.");
            }
        }
    }
}


#endif