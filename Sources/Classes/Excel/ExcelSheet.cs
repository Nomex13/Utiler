﻿#if !UNITY_ENGINE

using System;
using System.Collections.Generic;
using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public class ExcelSheet
        {
            public string Name { get; set; } = "Sheet";
            private Dictionary<int, Dictionary<int, ExcelBorderLine>> _RowToColumnToBorderHorizontal = new Dictionary<int, Dictionary<int, ExcelBorderLine>>();
            private Dictionary<int, Dictionary<int, ExcelBorderLine>> _RowToColumnToBorderVertical = new Dictionary<int, Dictionary<int, ExcelBorderLine>>();
            //public Dictionary<int, Dictionary<int, ExcelBorder>> RowToColumnToBorder
            //{
            //    get
            //    {
            //        return _RowToColumnToBorderVertical;
            //    }
            //}
            private Dictionary<int, Dictionary<int, ExcelCell>> _RowToColumnToCell = new Dictionary<int, Dictionary<int, ExcelCell>>();
            public Dictionary<int, Dictionary<int, ExcelCell>> RowToColumnToCell
            {
                get
                {
                    return _RowToColumnToCell;
                }
            }
            //private List<ExcelCell> _Cells = new List<ExcelCell>();
            //public IReadOnlyList<ExcelCell> Cells
            //{
            //    get
            //    {
            //        return _Cells;
            //    }
            //}
            private List<ExcelRange> _Merges { get; } = new List<ExcelRange>();
            internal List<ExcelRange> Merges
            {
                get
                {
                    return _Merges;
                }
            }
            private Dictionary<int, float> _Widths { get; } = new Dictionary<int, float>();
            public IReadOnlyDictionary<int, float> Widths
            {
                get
                {
                    return _Widths;
                }
            }
            public readonly float WidthDefault = 8.43f;
            private Dictionary<int, float> _Height { get; } = new Dictionary<int, float>();
            public IReadOnlyDictionary<int, float> Height
            {
                get
                {
                    return _Height;
                }
            }
            public readonly float HeightDefault = 15f;
            public bool OrientationIsPortrait { get; set; } = true;
            public int RowMin { get; private set; } = -1;
            public int RowMax { get; private set; } = -1;
            public int ColumnMin { get; private set; } = -1;
            public int ColumnMax { get; private set; } = -1;

            internal ExcelSharedFont SharedFontDefault { get; set; }
            internal ExcelSharedBackground SharedBackgroundDefault { get; set; }
            internal ExcelSharedBorder SharedBorderDefault { get; set; }
            internal ExcelSharedStyle SharedStyleDefault { get; set; }

            public ExcelBook Book { get; }
            public ExcelSheet(ExcelBook book)
            {
                Book = book;

                SharedFontDefault = Book.SharedFontDefault;
                SharedBackgroundDefault = Book.SharedBackgroundDefaultNone;
                SharedBorderDefault = Book.SharedBorderDefaultNone;
                SharedStyleDefault = Book.SharedStyleDefault;
            }
            public ExcelCell this[int row, int column]
            {
                get
                {
                    return GetCell(row, column);
                }
            }
            public ExcelCell GetCell(int row, int column)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }

                if (!_RowToColumnToCell.TryGetValue(row, out Dictionary<int, ExcelCell> columnToCell))
                {
                    columnToCell = new Dictionary<int, ExcelCell>();
                    _RowToColumnToCell.Add(row, columnToCell);
                }
                if (!columnToCell.TryGetValue(column, out ExcelCell cell))
                {
                    cell = new ExcelCell(this, row, column);
                    columnToCell.Add(column, cell);
                    //_Cells.Add(cell);
                }
                if (RowMin < 0 || row < RowMin)
                {
                    RowMin = row;
                }
                if (RowMax < 0 || row > RowMax)
                {
                    RowMax = row;
                }
                if (ColumnMin < 0 || column < ColumnMin)
                {
                    ColumnMin = column;
                }
                if (ColumnMax < 0 || column > ColumnMax)
                {
                    ColumnMax = column;
                }
                return cell;
            }
            public ExcelCell TryCell(int row, int column)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }

                if (!_RowToColumnToCell.TryGetValue(row, out Dictionary<int, ExcelCell> columnToCell))
                {
                    return null;
                }
                if (!columnToCell.TryGetValue(column, out ExcelCell cell))
                {
                    return null;
                }
                return cell;
            }

            #region Borders
            //internal ExcelBorder GetBorder(ExcelCell cell)
            //{
            //    return GetBorder(cell.Row, cell.Column);
            //}
            //internal ExcelBorder GetBorder(int row, int column)
            //{
            //    return new ExcelBorder
            //    (
            //        GetBorderVertical(row, column),
            //        GetBorderHorizontal(row, column),
            //        GetBorderVertical(row, column + 1),
            //        GetBorderHorizontal(row + 1, column)
            //    );
            //}

            //internal ExcelBorderLine GetBorderVertical(int row, int column)
            //{
            //    return GetBorder(_RowToColumnToBorderVertical, row, column);
            //}
            //internal ExcelBorderLine GetBorderHorizontal(int row, int column)
            //{
            //    return GetBorder(_RowToColumnToBorderHorizontal, row, column);
            //}
            //internal void SetBorderVertical(int row, int column, ExcelBorderLine line)
            //{
            //    SetBorder(_RowToColumnToBorderVertical, row, column, line);
            //    this[row, column].SharedBorder = null;
            //    if (column > 0)
            //    {
            //        this[row, column - 1].SharedBorder = null;
            //    }
            //}
            //internal void SetBorderVertical(int row, int column, Color color, ExcelBorderLineStyle style)
            //{
            //    SetBorder(_RowToColumnToBorderVertical, row, column, new ExcelBorderLine(color, style));
            //    this[row, column].SharedBorder = null;
            //    if (column > 0)
            //    {
            //        this[row, column - 1].SharedBorder = null;
            //    }
            //}
            //internal void SetBorderHorizontal(int row, int column, ExcelBorderLine line)
            //{
            //    SetBorder(_RowToColumnToBorderHorizontal, row, column, line);
            //    this[row, column].SharedBorder = null;
            //    if (row > 0)
            //    {
            //        this[row - 1, column].SharedBorder = null;
            //    }
            //}
            //internal void SetBorderHorizontal(int row, int column, Color color, ExcelBorderLineStyle style)
            //{
            //    SetBorder(_RowToColumnToBorderHorizontal, row, column, new ExcelBorderLine(color, style));
            //    this[row, column].SharedBorder = null;
            //    if (row > 0)
            //    {
            //        this[row - 1, column].SharedBorder = null;
            //    }
            //}
            //internal void UnsetBorderVertical(int row, int column)
            //{
            //    SetBorder(_RowToColumnToBorderVertical, row, column, null);
            //    this[row, column].SharedBorder = null;
            //    if (column > 0)
            //    {
            //        this[row, column - 1].SharedBorder = null;
            //    }
            //}
            //internal void UnsetBorderHorizontal(int row, int column)
            //{
            //    SetBorder(_RowToColumnToBorderHorizontal, row, column, null);
            //    this[row, column].SharedBorder = null;
            //    if (row > 0)
            //    {
            //        this[row - 1, column].SharedBorder = null;
            //    }
            //}
            //private void SetBorder(Dictionary<int, Dictionary<int, ExcelBorderLine>> rowToColumnToBorder, int row, int column, ExcelBorderLine line)
            //{
            //    if (row < 0)
            //    {
            //        throw new ArgumentException("Row index cannot be negative.", nameof(row));
            //    }
            //    if (column < 0)
            //    {
            //        throw new ArgumentException("Column index cannot be negative.", nameof(column));
            //    }

            //    if (!rowToColumnToBorder.TryGetValue(row, out Dictionary<int, ExcelBorderLine> columnToBorder))
            //    {
            //        if (!line.HasValue)
            //        {
            //            return;
            //        }

            //        columnToBorder = new Dictionary<int, ExcelBorderLine>();
            //        rowToColumnToBorder.Add(row, columnToBorder);
            //    }

            //    if (line.HasValue)
            //    {
            //        columnToBorder[column] = line.Value.Clone();
            //    }
            //    else
            //    {
            //        columnToBorder.Remove(column);
            //        if (columnToBorder.Count == 0)
            //        {
            //            rowToColumnToBorder.Remove(row);
            //        }
            //    }

            //    //if (RowMin < 0 || row < RowMin)
            //    //{
            //    //    RowMin = row;
            //    //}
            //    //if (RowMax < 0 || row > RowMax)
            //    //{
            //    //    RowMax = row;
            //    //}
            //    //if (ColumnMin < 0 || column < ColumnMin)
            //    //{
            //    //    ColumnMin = column;
            //    //}
            //    //if (ColumnMax < 0 || column > ColumnMax)
            //    //{
            //    //    ColumnMax = column;
            //    //}
            //}
            //private ExcelBorderLine GetBorder(Dictionary<int, Dictionary<int, ExcelBorderLine>> rowToColumnToBorder, int row, int column)
            //{
            //    if (row < 0)
            //    {
            //        throw new ArgumentException("Row index cannot be negative.", nameof(row));
            //    }
            //    if (column < 0)
            //    {
            //        throw new ArgumentException("Column index cannot be negative.", nameof(column));
            //    }

            //    if (!rowToColumnToBorder.TryGetValue(row, out Dictionary<int, ExcelBorderLine> columnToBorder))
            //    {
            //        columnToBorder = new Dictionary<int, ExcelBorderLine>();
            //        rowToColumnToBorder.Add(row, columnToBorder);
            //    }
            //    if (!columnToBorder.TryGetValue(column, out ExcelBorderLine border))
            //    {
            //        border = new ExcelBorderLine();
            //        columnToBorder.Add(column, border);
            //    }
            //    if (RowMin < 0 || row < RowMin)
            //    {
            //        RowMin = row;
            //    }
            //    if (RowMax < 0 || row > RowMax)
            //    {
            //        RowMax = row;
            //    }
            //    if (ColumnMin < 0 || column < ColumnMin)
            //    {
            //        ColumnMin = column;
            //    }
            //    if (ColumnMax < 0 || column > ColumnMax)
            //    {
            //        ColumnMax = column;
            //    }
            //    return border;
            //}
            //public ExcelBorderLine TryBorderVertical(int row, int column)
            //{
            //    return TryBorder(_RowToColumnToBorderVertical, row, column);
            //}
            //public ExcelBorderLine TryBorderHorizontal(int row, int column)
            //{
            //    return TryBorder(_RowToColumnToBorderHorizontal, row, column);
            //}
            //private ExcelBorderLine TryBorder(Dictionary<int, Dictionary<int, ExcelBorderLine>> rowToColumnToBorder, int row, int column)
            //{
            //    if (row < 0)
            //    {
            //        throw new ArgumentException("Row index cannot be negative.", nameof(row));
            //    }
            //    if (column < 0)
            //    {
            //        throw new ArgumentException("Column index cannot be negative.", nameof(column));
            //    }

            //    if (!rowToColumnToBorder.TryGetValue(row, out Dictionary<int, ExcelBorderLine> columnToBorder))
            //    {
            //        return null;
            //    }
            //    if (!columnToBorder.TryGetValue(column, out ExcelBorderLine border))
            //    {
            //        return null;
            //    }
            //    return border;
            //}
            #endregion

            //public ExcelCellMerge GetMerge(int row, int column)
            //{
            //    foreach (ExcelCellMerge merge in Merges)
            //    {
            //        if (merge.Contains(row, column))
            //        {
            //            return merge;
            //        }
            //    }
            //    return null;
            //}
            //public void Merge(int rowStart, int columnStart, int rowEnd, int columnEnd)
            //{
            //    if (rowStart < 0)
            //    {
            //        throw new ArgumentException("Row start index cannot be negative.", nameof(rowStart));
            //    }
            //    if (columnStart < 0)
            //    {
            //        throw new ArgumentException("Column start index cannot be negative.", nameof(columnStart));
            //    }
            //    if (rowEnd < 0)
            //    {
            //        throw new ArgumentException("Row end index cannot be negative.", nameof(rowEnd));
            //    }
            //    if (columnEnd < 0)
            //    {
            //        throw new ArgumentException("Column end index cannot be negative.", nameof(columnEnd));
            //    }

            //    foreach (ExcelCell cell in Range)
            //    _Merges.Add(new ExcelCellMerge(rowStart, columnStart, rowEnd, columnEnd));
            //}
            public ExcelRange Range(int rowStart, int columnStart, int rowEnd, int columnEnd)
            {
                return new ExcelRange(this, rowStart, columnStart, rowEnd, columnEnd);
            }
            public float GetWidth(int column)
            {
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }
                if (_Widths.TryGetValue(column, out float width))
                {
                    return width;
                }
                return WidthDefault;
            }
            public void SetWidth(int column, float width)
            {
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }
                if (width < 0)
                {
                    throw new ArgumentException("Column width cannot be negative.", nameof(width));
                }
                _Widths[column] = width;
            }
            public void UnsetWidth(int column)
            {
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }
                _Widths.Remove(column);
            }
            public void SetWidthAuto(int column)
            {
                if (column < 0)
                {
                    throw new ArgumentException("Column index cannot be negative.", nameof(column));
                }
                float width = 0;
                foreach (KeyValuePair<int, Dictionary<int, ExcelCell>> RowAndColumnToCell in RowToColumnToCell)
                {
                    if (!RowAndColumnToCell.Value.TryGetValue(column, out ExcelCell cell))
                    {
                        continue;
                    }
                    SizeF sizeInPixels = Excel.MeasureStringInPixels(cell.Text, cell.SharedFont.Font, Int32.MaxValue);
                    //SizeF sizeInPixels = Excel.MeasureStringInPixels(cell.Text, SharedFontDefault.Font, Int32.MaxValue);
                    //float widthInUnits = (sizeInPixels.Width + 7) / Book.ColumnWidthPixelsInOneUnit;
                    //float widthInUnits = Truncate(((256 * sizeInPixels.Width + Truncate(128 / Book.ColumnWidthPixelsInOneUnit)) / 256) * Book.ColumnWidthPixelsInOneUnit);
                    //float widthInUnits = ((sizeInPixels.Width - 5) / Book.MaximumDigitWidth * 100 + 0.5f) / 100;
                    //float widthInUnits = ((sizeInPixels.Width - 5) / Book.MaximumDigitWidth * 100 + 0.5f) / 100;
                    float widthInUnits = ((sizeInPixels.Width + 10) / Book.MaximumDigitWidth * 100 + 0.5f) / 100;
                    //int widthInPixels = (int)Math.Truncate((256 * widthInUnits + Math.Truncate(128 / Book.MaximumDigitWidth)) / 256 * Book.MaximumDigitWidth);
                    width = Math.Max(width, widthInUnits);
                }

                _Widths[column] = width;
            }
            public float GetHeight(int row)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                if (_Height.TryGetValue(row, out float height))
                {
                    return height;
                }
                return HeightDefault;
            }
            public void SetHeight(int row, float height)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                if (height < 0)
                {
                    throw new ArgumentException("Row height cannot be negative.", nameof(height));
                }
                _Height[row] = height;
            }
            public void UnsetHeight(int row)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                _Height.Remove(row);
            }
            public void SetHeightAuto(int row)
            {
                if (row < 0)
                {
                    throw new ArgumentException("Row index cannot be negative.", nameof(row));
                }
                if (!RowToColumnToCell.TryGetValue(row, out Dictionary<int, ExcelCell> ColumnToCell))
                {
                    return;
                }
                float height = 0;
                foreach (KeyValuePair<int, ExcelCell> ColumnAndCell in ColumnToCell)
                {
                    SizeF sizeInPixels = Excel.MeasureStringInPixels(ColumnAndCell.Value.Text, ColumnAndCell.Value.SharedFont.Font, (int)(GetWidth(row) * Book.MaximumDigitWidth + 1));
                    height = Math.Max(height, (sizeInPixels.Height) * 72 /* points in one inch */ / 96 /* pixels in one inch (dpi) */);
                }

                _Height[row] = height;
            }
        }
    }
}

#endif