﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Text;
//using System.Threading.Tasks;

//namespace Iodynis.Libraries.Utilities
//{
//    public partial class Excel
//    {
//        public class BinaryTree2D<TKey1, TKey2, TValue> where TKey1 : struct, IComparable
//        {
//            // Node connections
//            private int[] NodeNextOneGreaterTwoGreater = new int[1024];
//            private int[] NodeNextOneGreaterTwoLess    = new int[1024];
//            private int[] NodeNextOneLessTwoGreater    = new int[1024];
//            private int[] NodeNextOneLessTwoLess       = new int[1024];
//            private int[] NodeNextCount                = new int[1024];
//            private int[] NodeIndexOneBalance          = new int[1024];
//            private int[] NodeIndexTwoBalance          = new int[1024];
//            // Node contents
//            private TKey1[] NodeIndexOne               = new TKey1[1024];
//            private TKey2[] NodeIndexTwo               = new TKey2[1024];
//            private TValue[] NodeValue                 = new TValue[1024];
//            // ...
//            private int Root = 0;
//            private int Counter = 0;
//            private int Capacity;
//            private int[] Stack;
//            private int Depth;
//            // Recycling
//            private int[] Trash;
//            private int TrashCounter = 0;

//            public BinaryTree2D(int capacity = 1024)
//            {
//                Capacity = capacity;
//                // Capacity cannot be lower than 2 -- the zero and the root elements are always presented
//                // But there is no sense in allocating that small amount of memory, start with a bit more reasonable value
//                if (Capacity < 1024)
//                {
//                    Capacity = 1024;
//                }
//                Depth = (int)Math.Log(capacity, 2) + 1 /* due to double-to-int cast */ + 2 /* due to possible unbalanced state */;
//                Stack = new int[Depth];
//            }
//            /// <summary>
//            /// Allocate a new node.
//            /// </summary>
//            /// <returns>Position of the new node.</returns>
//            private int Allocate()
//            {
//                int nodeNew;
//                // Check if can reuse memory
//                if (TrashCounter > 0)
//                {
//                    nodeNew = Trash[--TrashCounter];

//                    // Zero
//                    NodeNextOneGreaterTwoGreater[nodeNew] = 0;
//                    NodeNextOneGreaterTwoLess[nodeNew] = 0;
//                    NodeNextOneLessTwoGreater[nodeNew] = 0;
//                    NodeNextOneLessTwoLess[nodeNew] = 0;
//                }
//                else
//                {
//                    // Ensure there is enough space
//                    while (Counter >= Capacity)
//                    {
//                        Expand();
//                    }
//                    nodeNew = Counter++;
//                }
//                return nodeNew;
//            }
//            /// <summary>
//            /// Add a new node or replace the existing one.
//            /// </summary>
//            /// <param name="indexOne">Node first index.</param>
//            /// <param name="indexTwo">Node second index.</param>
//            /// <param name="value">Node value.</param>
//            public void Add(TKey1 indexOne, TKey2 indexTwo, TValue value)
//            {
//                int nodeCurrent = Root;
//                bool alreadyExists = false;

//                while (true)
//                {
//                    if (indexOne >= NodeIndexOne[nodeCurrent])
//                    {
//                        if (indexTwo > NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneGreaterTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = Allocate();
//                                NodeNextOneGreaterTwoGreater[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else if (indexTwo < NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneGreaterTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = Allocate();
//                                NodeNextOneGreaterTwoLess[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            // Already exists
//                            alreadyExists = true;
//                            break;
//                        }
//                    }
//                    else
//                    {
//                        if (indexTwo > NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneLessTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = Allocate();
//                                NodeNextOneLessTwoGreater[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            if (NodeNextOneLessTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = Allocate();
//                                NodeNextOneLessTwoLess[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                    }
//                }

//                // Set the node
//                NodeIndexOne[nodeCurrent] = indexOne;
//                NodeIndexTwo[nodeCurrent] = indexTwo;
//                NodeValue[nodeCurrent] = value;
//                NodeNextCount[nodeCurrent]++;

//                if (!alreadyExists)
//                {
//                    Rebalance();
//                }
//            }
//            private int Get(int indexOne, int indexTwo)
//            {
//                int nodeCurrent = Root;

//                while (true)
//                {
//                    if (indexOne >= NodeIndexOne[nodeCurrent])
//                    {
//                        if (indexTwo > NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneGreaterTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                return -1;
//                            }
//                        }
//                        else if (indexTwo < NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneGreaterTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                return -1;
//                            }
//                        }
//                        else
//                        {
//                            return nodeCurrent;
//                        }
//                    }
//                    else
//                    {
//                        if (indexTwo > NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneLessTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                return -1;
//                            }
//                        }
//                        else
//                        {
//                            if (NodeNextOneLessTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                return -1;
//                            }
//                        }
//                    }
//                }
//            }
//            [MethodImpl(MethodImplOptions.AggressiveInlining)]
//            public void Remove(int indexOne, int indexTwo)
//            {
//                int nodeToDelete = Get(indexOne, indexTwo);
//                if (nodeToDelete >= 0)
//                {
//                    if (TrashCounter >= Trash.Length)
//                    {
//                        ExpandTrash();
//                    }
//                    Trash[TrashCounter++] = nodeToDelete;
//                }
//            }
//            public TValue GetValue(int indexOne, int indexTwo)
//            {
//                int node = Get(indexOne, indexTwo);
//                return node < 0 ? default(TValue) : NodeValue[node];
//            }
//            public IEnumerable<TValue> Get(int indexOneStart, int indexOneEnd, int indexTwoStart, int indexTwoEnd)
//            {
//                int indexOneCurrent = indexOneStart;
//                int indexTwoCurrent = indexTwoStart;
//                int nodeCurrent = Root;
//                int stackCounter = 0;

//                while (true)
//                {
//                    if (indexOneCurrent < NodeIndexOne[nodeCurrent]) // Need node with lower index one
//                    {
//                        if (indexTwoCurrent < NodeIndexTwo[nodeCurrent]) // Need node with lower index two
//                        {
//                            if (NodeNextOneLessTwoLess[nodeCurrent] > 0)
//                            {
//                                Stack[stackCounter]
//                                nodeCurrent = NodeNextOneLessTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                ; // Found the min
//                            }
//                        }
//                        else // Need node with greater index two
//                        {
//                            if (NodeNextOneLessTwoGreater[nodeCurrent] > 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                ; // Found the min
//                            }
//                        }
//                    }
//                    else // Need node with greater index one
//                    {
//                        ;
//                    }
//                }



//                            if (NodeNextOneGreaterTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = Allocate();
//                                NodeNextOneGreaterTwoGreater[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else if (indexTwo < NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneGreaterTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneGreaterTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = New();
//                                NodeNextOneGreaterTwoLess[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            // Already exists
//                            alreadyExists = true;
//                            break;
//                        }
//                    }
//                    else
//                    {
//                        if (indexTwo > NodeIndexTwo[nodeCurrent])
//                        {
//                            if (NodeNextOneLessTwoGreater[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoGreater[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = New();
//                                NodeNextOneLessTwoGreater[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            if (NodeNextOneLessTwoLess[nodeCurrent] != 0)
//                            {
//                                nodeCurrent = NodeNextOneLessTwoLess[nodeCurrent];
//                            }
//                            else
//                            {
//                                int nodeNew = New();
//                                NodeNextOneLessTwoLess[nodeCurrent] = nodeNew;
//                                nodeCurrent = nodeNew;
//                                break;
//                            }
//                        }
//                    }
//                }
//            }
//            [MethodImpl(MethodImplOptions.AggressiveInlining)]
//            private void ExpandTrash()
//            {
//                // Allocate memory
//                int[] trashExpanded = new int[Trash.Length * 2 /* expand twice */];
//                // Copy data
//                Trash.CopyTo(trashExpanded, 0);
//                // Assign
//                Trash = trashExpanded;
//            }
//            [MethodImpl(MethodImplOptions.AggressiveInlining)]
//            private void Expand()
//            {
//                // Expand twice
//                Capacity *= 2;
//                // Allocate memory
//                int[] newOneGreaterTwoGreater = new int[Capacity];
//                int[] newOneGreaterTwoLess    = new int[Capacity];
//                int[] newOneLessTwoGreater    = new int[Capacity];
//                int[] newOneLessTwoLess       = new int[Capacity];
//                // Node contents
//                int[] newIndexOne             = new int[Capacity];
//                int[] newIndexTwo             = new int[Capacity];
//                T[]   newValue                = new T[Capacity];
//                // Copy data
//                NodeNextOneGreaterTwoGreater.CopyTo(newOneGreaterTwoGreater, 0);
//                NodeNextOneGreaterTwoLess.CopyTo(newOneGreaterTwoLess, 0);
//                NodeNextOneLessTwoGreater.CopyTo(newOneLessTwoGreater, 0);
//                NodeNextOneLessTwoLess.CopyTo(newOneLessTwoLess, 0);
//                NodeIndexOne.CopyTo(newIndexOne, 0);
//                NodeIndexTwo.CopyTo(newIndexTwo, 0);
//                NodeValue.CopyTo(newValue, 0);
//                // Replace
//                NodeNextOneGreaterTwoGreater = newOneGreaterTwoGreater;
//                NodeNextOneGreaterTwoLess = newOneGreaterTwoLess;
//                NodeNextOneLessTwoGreater = newOneLessTwoGreater;
//                NodeNextOneLessTwoLess = newOneLessTwoLess;
//                NodeIndexOne = newIndexOne;
//                NodeIndexTwo = newIndexTwo;
//                NodeValue = newValue;
//            }
//            //public void Add(int indexOne, int indexTwo, T value)
//            //{
//            //    Root.Add(indexOne, indexTwo, value);
//            //}
//            //public void Remove(int indexOne, int indexTwo)
//            //{
//            //    Root.Remove(indexOne, indexTwo);
//            //}
//            //public T GetObject(int indexOne, int indexTwo)
//            //{
//            //    return Root.GetObject(indexOne, indexTwo);
//            //}
//            //public IEnumerable<T> GetRange(int indexOne, int indexTwo)
//            //{
//            //    return Root.GetRange(indexOne, indexTwo);
//            //}
//        }
//    }
//}
