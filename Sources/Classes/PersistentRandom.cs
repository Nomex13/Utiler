﻿using System;
using System.Collections.Generic;
using System.Threading;
using Iodynis.Libraries.Utility.Extensions;

namespace Iodynis.Libraries.Utility
{
    public class PersistentRandom
    {
        private readonly PersistentRandom Random;
        public readonly int Id;

        /// <summary>
        /// Internal state of the channel.
        /// It is zero at creation and is incremented with each request.
        /// Generators with same <see cref="Seed"/> and <see cref="State"/> will generate same numbers.
        /// </summary>
        private long _State = 0;
        public long State
        {
            get => _State;
            set => _State = value;
        }

        public PersistentRandom(PersistentRandom random, int id)
        {
            Random = random;
            Id = id;
        }

        //private const uint Prime1 = 43;
        //private const uint Prime2 = 719;
        //private const uint Prime3 = 2399;
        //private const uint Prime4 = 43691;
        //private const uint Prime5 = 333667;
        private const uint Prime6 = 13703077;
        private const uint Prime7 = 122959073;
        private const uint Prime8 = 1645333507;

        private const int Mask = 0x67452301;
        private const int Multiplier = 43691;
        private const int Addition = 13703078;

        public long Seed { get; set; } = 0;

        public PersistentRandom()
            : this(0L) { }
        public PersistentRandom(short seed)
            : this((long)seed) { }
        public PersistentRandom(ushort seed)
            : this((long)seed) { }
        public PersistentRandom(int seed)
            : this((long)seed) { }
        public PersistentRandom(uint seed)
            : this((long)seed) { }
        public PersistentRandom(ulong seed)
            : this((long)seed) { }
        public PersistentRandom(long seed)
        {
            Seed = seed;
            ChannelDefault = new PersistentRandom(this, 0);
            IdToChannel[0] = ChannelDefault;
        }

        private PersistentRandom ChannelDefault;
        private Dictionary<int, PersistentRandom> IdToChannel = new Dictionary<int, PersistentRandom>();
        public PersistentRandom this[int index]
        {
            get
            {
                if (!IdToChannel.TryGetValue(index, out PersistentRandom channel))
                {
                    channel = new PersistentRandom(this, index);
                    IdToChannel.Add(index, channel);
                }

                return channel;
            }
        }

        public PersistentRandom this[string name]
        {
            get
            {
                int index = StringToHash(name);

                if (!IdToChannel.TryGetValue(index, out PersistentRandom channel))
                {
                    channel = new PersistentRandom(this, index);
                    IdToChannel.Add(index, channel);
                }

                return channel;
            }
        }

        private int StringToHash(string @string)
        {
            int hash = 0;

            for (int index = 0; index < @string.Length; index++)
            {
                hash = (hash << 16) ^ (hash >> 16) ^ (index * (int)Prime6) ^ @string[index];
            }

            return hash;
        }

        /// <summary>
        /// Default random channel.
        /// </summary>
        public PersistentRandom Default => ChannelDefault;
        /// <summary>
        /// All custon channels.
        /// </summary>
        public IEnumerable<PersistentRandom> Channels => IdToChannel.Values;

        ///// <summary>
        ///// Preview a random integer in the [min, max] interval.
        ///// </summary>
        ///// <param name="min"></param>
        ///// <param name="max"></param>
        ///// <param name="shift"></param>
        ///// <returns></returns>
        //public int IntegerPreview(int min, int max, int shift = 0) => ChannelDefault
        //{
        //    return GetInteger(min, max, State + shift);
        //}

        ///// <summary>
        ///// Get a random integer in the [min, max] interval.
        ///// </summary>
        ///// <param name="min"></param>
        ///// <param name="max"></param>
        ///// <returns></returns>
        //public int Integer(int min, int max)
        //{
        //    long state = Interlocked.Increment(ref _State);
        //    return GetInteger(min, max, state);
        //}

        private long GetStateCycled(long state)
        {
            return ((Seed + Prime8 * state * 5 + Prime7 * Id + Prime6 * state * state) >> 2) & 0xFFFFFFFF;
        }
        private long GetValue(long count)
        {
            long cycled = GetStateCycled(count);
            return unchecked((cycled ^ Mask) * Multiplier + Addition) & 0xFFFFFFFF;
        }

        /// <summary>
        /// Skip <paramref name="times"/> forward or backward.
        /// </summary>
        /// <param name="times"></param>
        public void Skip(int times = 1)
        {
            _State += times;
        }

        #region Bool
        /// <summary>
        /// Preview a random bool.
        /// </summary>
        /// <returns></returns>
        public bool Preview(int shift = 0)
        {
            return (GetValue(State + shift) & 0x1) == 0;
        }
        /// <summary>
        /// Get a random bool.
        /// </summary>
        /// <returns></returns>
        public bool Take()
        {
            long state = Interlocked.Increment(ref _State);
            return (GetValue(state) & 0x1) == 0;
        }
        #endregion

        #region Integer
        private int Take(int min, int max, long count)
        {
            if (max < min)
            {
                throw new ArgumentException("Minimum is greater than maximum.");
            }
            return (int)(min + (max - min) * (uint.MaxValue - GetValue(count)) / uint.MaxValue);
        }
        /// <summary>
        /// Preview a random integer in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public int Preview(int min, int max, int shift = 0)
        {
            return Take(min, max, State + shift);
        }
        /// <summary>
        /// Get a random integer in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public int Take(int min, int max)
        {
            long state = Interlocked.Increment(ref _State);
            return Take(min, max, state);
        }
        /// <summary>
        /// Get the specified <paramref name="count"/> of random integers in the in the [0, sum] interval each that give the specified <paramref name="sum"/> in total.
        /// </summary>
        /// <param name="sum">Sum of the generated random integers.</param>
        /// <param name="count">Array length of the generated random integers.</param>
        /// <returns></returns>
        public int[] TakeSome(int sum, int count)
        {
            if (sum < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sum));
            }
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            if (count == 0)
            {
                return new int[] { };
            }
            if (count == 1)
            {
                return new int[] { sum };
            }

            int[] randoms = new int[count];

            randoms[0] = 0;
            for (int i = 1; i < randoms.Length; i++)
            {
                randoms[i] = Take(0, sum);
            }

            Array.Sort(randoms);

            for (int i = 0; i < randoms.Length - 1; i++)
            {
                randoms[i] = randoms[i + 1] - randoms[i];
            }
            randoms[randoms.Length - 1] = sum - randoms[randoms.Length - 1];

            return randoms;
        }
        #endregion

        #region Float
        private float Take(float min, float max, long count)
        {
            if (max < min)
            {
                throw new ArgumentException("Minimum is greater than maximum.");
            }
            return (int)(min + (max - min) * (uint.MaxValue - GetValue(count)) / uint.MaxValue);
        }
        /// <summary>
        /// Preview a random float in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public float Preview(float min, float max, int shift = 0)
        {
            return Take(min, max, State + shift);
        }
        /// <summary>
        /// Get a random float in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public float Take(float min, float max)
        {
            long state = Interlocked.Increment(ref _State);
            return Take(min, max, state);
        }
        #endregion

        #region Double
        private double Take(double min, double max, long count)
        {
            if (max < min)
            {
                throw new ArgumentException("Minimum is greater than maximum.");
            }
            return (int)(min + (max - min) * (uint.MaxValue - GetValue(count)) / uint.MaxValue);
        }
        /// <summary>
        /// Preview a random double in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public double Preview(double min, double max, int shift = 0)
        {
            return Take(min, max, State + shift);
        }
        /// <summary>
        /// Get a random double in the [min, max] interval.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public double Take(double min, double max)
        {
            long state = Interlocked.Increment(ref _State);
            return Take(min, max, state);
        }
        #endregion

        #region Shuffle
        /// <summary>
        /// Shuffle the list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public IList<T> Shuffle<T>(IList<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int j = Take(0, list.Count);
                if (j == i)
                {
                    continue;
                }

                list.Swap(i, j);
            }

            return list;
        }
        /// <summary>
        /// Get a random item from the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="weightGetter"></param>
        /// <returns></returns>
        public T GetWeighted<T>(IList<T> list, Func<T, float> weightGetter)
        {
            if (list.Count == 0)
            {
                return default(T);
            }
            if (list.Count == 1)
            {
                return list[0];
            }

            float weightTotal = 0;
            for (int i = 0; i < list.Count; i++)
            {
                weightTotal += weightGetter(list[i]);
            }

            if (weightTotal == 0)
            {
                return list[0];
            }

            float threshold = Take(0, weightTotal);
            float weightCollected = 0;
            for (int i = 0; i < list.Count; i++)
            {
                weightCollected += weightGetter(list[i]);
                if (weightCollected >= threshold)
                {
                    return list[i];
                }
            }

            return list[list.Count - 1];
        }
        /// <summary>
        /// Get a random item from the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="weightGetter"></param>
        /// <returns></returns>
        public T GetWeighted<T>(IList<Weighted<T>> list)
        {
            if (list.Count == 0)
            {
                return default(T);
            }
            if (list.Count == 1)
            {
                return list[0].Value;
            }

            float weightTotal = 0;
            for (int i = 0; i < list.Count; i++)
            {
                weightTotal += list[i].Weight;
            }

            if (weightTotal == 0)
            {
                return list[0].Value;
            }

            float threshold = Take(0, weightTotal);
            float weightCollected = 0;
            for (int i = 0; i < list.Count; i++)
            {
                weightCollected += list[i].Weight;
                if (weightCollected >= threshold)
                {
                    return list[i].Value;
                }
            }

            return list[list.Count - 1].Value;
        }
        #endregion
    }
}
