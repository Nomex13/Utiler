﻿using System;

namespace Iodynis.Libraries.Utility
{
    public class Finally : IDisposable
    {
        private Action Action;
        public Finally(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            Action = action;
        }
        public void Dispose()
        {
            Action.Invoke();
        }
    }
}
