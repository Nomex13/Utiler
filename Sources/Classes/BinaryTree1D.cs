﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Utility
{
    public class BinaryTree1D<TKey, TValue> where TKey : IComparable<TKey>
    {
        private int[] NodeParent;
        private int[] NodeRight;
        private int[] NodeLeft;
        /// <summary>
        /// Size of children nodes plus 1
        /// </summary>
        private int[] NodeSize;
        private int[] NodeBalance;
        //private bool[] NodeColor;
        // Node contents
        private TKey[] NodeKey;
        private TValue[] NodeValue;
        // ...
        private int Root = 0;
        private int Counter = 1; /* Skip the zero element */
        private int Capacity;
        private float CapacityMultiplier;
        //private int[] Stack;
        //private int[] Depth;
        // Recycling
        private int[] Trash;
        private int TrashCounter = 0;
        /// <summary>
        /// ...
        /// </summary>
        /// <param name="capacity">Initial capacity.</param>
        /// <param name="capacityMultiplier">Capacity multiplier.</param>
        public BinaryTree1D(int capacity = 1024, float capacityMultiplier = 2)
        {
            // Capacity cannot be lower than 2 -- the zero and the root elements are always presented
            // But there is no sense in allocating that small amount of memory, start with a bit more reasonable value
            if (capacity < 1024)
            {
                throw new ArgumentException("Initial capacity cannot be lower than 1024.", nameof(capacity));
                //Capacity = 1024;
            }
            float capacityMultiplierMin = 1 + (4f / capacity);
            if (capacityMultiplier <= capacityMultiplierMin)
            {
                throw new ArgumentException($"The capacity multiplier cannot be lower than {capacityMultiplierMin} for capacity {capacity}.", nameof(capacityMultiplier));
            }

            Capacity = capacity;
            CapacityMultiplier = capacityMultiplier;

            Init();
        }

        #region API
        public TValue this[TKey key]
        {
            get
            {
                return GetValue(key);
            }
            set
            {
                Set(key, value);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private TValue GetValue(TKey key)
        {
            int node = GetNode(key);
            if (node < 0)
            {
                throw new Exception($"Key {key} not found in the tree.");
            }

            return NodeValue[node];
        }
        public bool GetValue(TKey key, out TValue value)
        {
            int node = GetNode(key);
            if (node < 0)
            {
                value = default(TValue);
                return false;
            }
            else
            {
                value = NodeValue[node];
                return true;
            }
        }
        public IEnumerable<TValue> GetRange(TKey min, TKey max)
        {
            int nodeCurrent = GetNodeCeiling(min);
            while (max.CompareTo(NodeKey[nodeCurrent]) < 0) // While less than max
            {
                yield return NodeValue[nodeCurrent]; // Return value
                nodeCurrent = GetNodeNext(nodeCurrent); // Get next node to the right
            }
        }
        public int GetCount()
        {
            return Root == 0 ? 0 : NodeSize[Root];
        }
        public void Clear()
        {
            Root = 0;
            Counter = 1;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Set(TKey key, TValue value)
        {
            // Set the root
            if (Root == 0)
            {
                Root = 1;
                Counter = 2; /* Zero and root */
                NodeKey[Root] = key;
                NodeValue[Root] = value;
                return;
            }

            // Otherwise set a simple node
            int nodeCurrent = Root;
            while (true)
            {
                int direction = key.CompareTo(NodeKey[nodeCurrent]);
                if (direction > 0) // Go right
                {
                    if (NodeRight[nodeCurrent] <= 0) // Nowhere to go
                    {
                        int nodeNew = Allocate();
                        NodeRight[nodeCurrent] = nodeNew;
                        NodeParent[nodeNew] = nodeCurrent;
                        NodeSize[nodeNew] = 1;
                        NodeKey[nodeNew] = key;
                        NodeValue[nodeNew] = value;
                        break;
                    }
                    else
                    {
                        NodeBalance[nodeCurrent]++;
                        nodeCurrent = NodeRight[nodeCurrent];
                    }
                }
                else if (direction < 0) // Go left
                {
                    if (NodeLeft[nodeCurrent] <= 0) // Nowhere to go
                    {
                        int nodeNew = Allocate();
                        NodeLeft[nodeCurrent] = nodeNew;
                        NodeParent[nodeNew] = nodeCurrent;
                        NodeSize[nodeNew] = 1;
                        NodeKey[nodeNew] = key;
                        NodeValue[nodeNew] = value;
                        break;
                    }
                    else
                    {
                        NodeBalance[nodeCurrent]--;
                        nodeCurrent = NodeLeft[nodeCurrent];
                    }
                }
                else // Got it
                {
                    NodeValue[nodeCurrent] = value;
                    return;
                }
            }

            while (nodeCurrent > 0)
            {
                NodeSize[nodeCurrent]++;
                nodeCurrent = NodeParent[nodeCurrent];
            }
        }
        public void Remove(TKey key)
        {
            int node = GetNode(key);

            throw new NotImplementedException();
        }
        //
        // Rotate the Node to the right:
        //
        //                Root                         Root
        //               / \                          / \
        //            * /   \                      * /   \         <-- this connection switches child
        //             /     \                      /     \
        //            Node    O        -->         Left    O
        //           / \                          / \
        //        * /   \                        /   \ *           <-- this connection switches direction
        //         /     \                      /     \
        //        Left    Right                O       Node
        //       / \ *   / \                        * / \          <-- this connection switches parent
        //      O  xXx  O   O                       xXx  Right
        //                                              / \
        // * Changes made:                             O   O
        //
        //   node   direction     old       new
        //  --------------------------------------
        //   Root   left/right   Node  -->  Left
        //   Node     left       Left  -->   xXx
        //   Left     right      xXx   -->  Node
        //   Node      up        Root  -->  Left
        //   Left      up        Node  -->  Root
        //   xXx       up        Left  -->  Node
        //
        private void RotateRight(int node)
        {
            if (NodeLeft[node] <= 0)
            {
                throw new Exception($"Cannot rotate node {node} to the right as it does not have anything on its left.");
            }

            int nodeUp        = NodeParent[node];
            int nodeLeft      = NodeLeft[node];
            int nodeRight     = NodeRight[node];
            int nodeLeftRight = NodeRight[nodeLeft];

            // Left and right connections
            if (nodeUp == 0) // Rotating root
            {
                Root = nodeLeft;
            }
            else // Rotating non-root
            {
                if (NodeKey[node].CompareTo(NodeKey[nodeUp]) < 0)
                {
                    NodeLeft[nodeUp] = nodeLeft;
                }
                else
                {
                    NodeRight[nodeUp] = nodeLeft;
                }
            }
            NodeLeft[node] = nodeLeftRight;
            NodeRight[nodeLeft] = node;

            // Up connection
            NodeParent[node] = nodeLeft;
            NodeParent[nodeLeft] = nodeUp;
            if (nodeLeftRight != 0)
            {
                NodeParent[nodeLeftRight] = node;
            }
        }
        //
        // Rotate the Node to the left:
        //
        //                Root                         Root
        //               / \                          / \
        //            * /   \                      * /   \         <-- this connection switches child
        //             /     \                      /     \
        //            Node    O        -->         Right   O
        //           / \                          / \
        //          /   \ *                    * /   \             <-- this connection switches direction
        //         /     \                      /     \
        //        Left    Right                Node    O
        //       / \   * / \                  / \
        //      O   O  xXx  O                /   \ *               <-- this connection switches parent
        //                                  /     \
        //                                 Left   xXx
        //                                / \
        // * Changes made:               O   O
        //
        //   node   direction     old       new
        //  --------------------------------------
        //   Root   left/right   Node  --> Right
        //   Node     right      Right -->   xXx
        //   Right    left       xXx   -->  Node
        //   Node      up        Root  --> Right
        //   Right     up        Node  -->  Root
        //   xXx       up        Left  -->  Node
        //
        private void RotateLeft(int node)
        {
            if (NodeLeft[node] <= 0)
            {
                throw new Exception($"Cannot rotate node {node} to the left as it does not have anything on its right.");
            }

            int nodeUp        = NodeParent[node];
            int nodeLeft      = NodeLeft[node];
            int nodeRight     = NodeRight[node];
            int nodeRightLeft = NodeLeft[nodeRight];

            // Left and right connections
            if (nodeUp == 0) // Rotating root
            {
                Root = nodeRight;
            }
            else // Rotating non-root
            {
                if (NodeKey[node].CompareTo(NodeKey[nodeUp]) < 0)
                {
                    NodeRight[nodeUp] = nodeRight;
                }
                else
                {
                    NodeLeft[nodeUp] = nodeRight;
                }
            }
            NodeRight[node] = nodeRightLeft;
            NodeLeft[nodeRight] = node;

            // Up connection
            NodeParent[node] = nodeRight;
            NodeParent[nodeRight] = nodeUp;
            if (nodeRightLeft != 0)
            {
                NodeParent[nodeRightLeft] = node;
            }
        }
        public TKey GetNextKey(TKey key)
        {
            int node = GetNode(key);
            node = GetNodeNext(node);
            if (node <= 0)
            {
                return default(TKey);
            }
            else
            {
                return NodeKey[node];
            }
        }
        public TKey GetPreviousKey(TKey key)
        {
            int node = GetNode(key);
            node = GetNodePrevious(node);
            if (node <= 0)
            {
                return default(TKey);
            }
            else
            {
                return NodeKey[node];
            }
        }
        /// <summary>
        /// Get the floor of the key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The closest smaller key. In case all keys are smaller than the provided one -- the maximal key.</returns>
        public TKey GetFloor(TKey key)
        {
            return NodeKey[GetNodeFloor(key)];
        }
        /// <summary>
        /// Get the ceiling of the key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The closest greater key. In case all keys are greater than the provided one -- the minimal key.</returns>
        public TKey GetCeiling(TKey key)
        {
            return NodeKey[GetNodeCeiling(key)];
        }
        #endregion

        #region Node
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNode(TKey key)
        {
            int nodeCurrent = Root;

            while (true)
            {
                if (nodeCurrent <= 0)
                {
                    return -1;
                }

                int direction = key.CompareTo(NodeKey[nodeCurrent]);
                if (direction > 0) // Go right
                {
                    nodeCurrent = NodeRight[nodeCurrent];
                }
                else if (direction < 0) // Go left
                {
                    nodeCurrent = NodeLeft[nodeCurrent];
                }
                else // Got it
                {
                    return nodeCurrent;
                }
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNodeNext(int node)
        {
            int nodeCurrent;
            if (NodeRight[node] > 0) // Can go right
            {
                nodeCurrent = NodeRight[node]; // Go right once
                while (NodeLeft[nodeCurrent] > 0) // Go left as far as possible
                {
                    nodeCurrent = NodeLeft[nodeCurrent];
                }
            }
            else // Can't go right
            {
                nodeCurrent = NodeParent[node]; // Go up
                while (nodeCurrent != 0 && NodeKey[nodeCurrent].CompareTo(NodeKey[node]) < 0) // Go up until a greater key is reached
                {
                    nodeCurrent = NodeParent[nodeCurrent];
                }
            }
            return nodeCurrent;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNodePrevious(int node)
        {
            int nodeCurrent;
            if (NodeLeft[node] > 0) // Can go left
            {
                nodeCurrent = NodeLeft[node]; // Go left once
                while (NodeRight[nodeCurrent] > 0) // Go right as far as possible
                {
                    nodeCurrent = NodeRight[nodeCurrent];
                }
            }
            else // Can't go left
            {
                nodeCurrent = NodeParent[node]; // Go up
                while (nodeCurrent != 0 && NodeKey[nodeCurrent].CompareTo(NodeKey[node]) > 0) // Go up until a lesser key is reached
                {
                    nodeCurrent = NodeParent[nodeCurrent];
                }
            }
            return nodeCurrent;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNodeFloor(TKey key)
        {
            int nodeCurrent = Root;
            int nodeFloor = 0;

            while (true)
            {
                int direction = key.CompareTo(NodeKey[nodeCurrent]);
                if (direction > 0) // Go right
                {
                    if (NodeRight[nodeCurrent] <= 0) // Nowhere to go
                    {
                        //if (nodeFloor == 0) // All values in the tree are lower than the requested one
                        //{
                        //    return nodeCurrent;
                        //}
                        //return nodeFloor;
                        return nodeCurrent;
                    }
                    nodeFloor = nodeCurrent;
                    nodeCurrent = NodeRight[nodeCurrent];
                }
                else if (direction < 0) // Go left
                {
                    if (NodeLeft[nodeCurrent] <= 0) // Nowhere to go
                    {
                        if (nodeFloor == 0) // All values in the tree are greater than the requested one
                        {
                            return nodeCurrent;
                        }
                        return nodeFloor;
                    }
                    nodeCurrent = NodeLeft[nodeCurrent];
                }
                else // Got precise match
                {
                    return nodeCurrent;
                }
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetNodeCeiling(TKey key)
        {
            int nodeCurrent = Root;
            int nodeFloor = 0;

            while (true)
            {
                int direction = key.CompareTo(NodeKey[nodeCurrent]);
                if (direction > 0) // Go right
                {
                    if (NodeRight[nodeCurrent] <= 0) // Nowhere to go
                    {
                        if (nodeFloor == 0) // All values in the tree are lower than the requested one
                        {
                            return nodeCurrent;
                        }
                        return nodeFloor;
                    }
                    nodeCurrent = NodeRight[nodeCurrent];
                }
                else if (direction < 0) // Go left
                {
                    if (NodeLeft[nodeCurrent] <= 0) // Nowhere to go
                    {
                        //if (nodeFloor == 0) // All values in the tree are greater than the requested one
                        //{
                        //    return nodeCurrent;
                        //}
                        //return nodeFloor;
                        return nodeCurrent;
                    }
                    nodeFloor = nodeCurrent;
                    nodeCurrent = NodeLeft[nodeCurrent];
                }
                else // Got precise match
                {
                    return nodeCurrent;
                }
            }
        }
        #endregion

        #region Util
        /// <summary>
        /// Allocate a new node.
        /// </summary>
        /// <returns>Position of the new node.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Allocate()
        {
            int nodeNew;
            // Check if can reuse memory
            if (TrashCounter > 0)
            {
                nodeNew = Trash[--TrashCounter];

                // Zero connections
                NodeParent[nodeNew] = 0;
                NodeRight[nodeNew] = 0;
                NodeLeft[nodeNew] = 0;
            }
            else
            {
                // Ensure there is enough space
                while (Counter >= Capacity)
                {
                    Expand();
                }
                nodeNew = Counter++;
            }
            return nodeNew;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Init()
        {
            // Allocate memory
            NodeParent  = new int[Capacity];
            NodeRight = new int[Capacity];
            NodeLeft  = new int[Capacity];
            NodeSize = new int[Capacity];
            NodeBalance   = new int[Capacity];
            // Node contents
            NodeKey       = new TKey[Capacity];
            NodeValue     = new TValue[Capacity];

            //Depth = (int)Math.Log(Capacity, 2) + 1 /* due to double-to-int cast */ + 2 /* due to possible unbalanced state */;
            //Stack = new int[Depth];
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void Expand()
        {
            // Expand twice
            Capacity = (int)(Capacity * CapacityMultiplier);
            // Allocate memory
            int[] newNodePrevious  = new int[Capacity];
            int[] newNodeNextRight = new int[Capacity];
            int[] newNodeNextLeft  = new int[Capacity];
            int[] newNodeNextCount = new int[Capacity];
            int[] newNodeBalance   = new int[Capacity];
            // Node contents
            TKey[] newNodeKey        = new TKey[Capacity];
            TValue[] newNodeValue    = new TValue[Capacity];
            // Copy data
            NodeParent.CopyTo(newNodePrevious, 0);
            NodeRight.CopyTo(newNodeNextRight, 0);
            NodeLeft.CopyTo(newNodeNextLeft, 0);
            NodeSize.CopyTo(newNodeNextCount, 0);
            NodeBalance.CopyTo(newNodeBalance, 0);
            NodeKey.CopyTo(newNodeKey, 0);
            NodeValue.CopyTo(newNodeValue, 0);
            // Replace
            NodeParent = newNodePrevious;
            NodeRight = newNodeNextRight;
            NodeLeft = newNodeNextLeft;
            NodeSize = newNodeNextCount;
            NodeBalance = newNodeBalance;
            NodeKey = newNodeKey;
            NodeValue = newNodeValue;

            // Expand stack
            //Depth = (int)Math.Log(Capacity, 2) + 1 /* due to double-to-int cast */ + 2 /* due to possible unbalanced state */;
            //Stack = new int[Depth];
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ExpandTrash()
        {
            // Allocate memory
            int[] trashExpanded = new int[Trash.Length * 2 /* expand twice */];
            // Copy data
            Trash.CopyTo(trashExpanded, 0);
            // Assign
            Trash = trashExpanded;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Min(int one, int two)
        {
            return one > two ? one : two;
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int Max(int one, int two)
        {
            return one < two ? one : two;
        }
        #endregion
    }
}
