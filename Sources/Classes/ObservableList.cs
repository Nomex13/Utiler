﻿using Iodynis.Libraries.Utility.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;

namespace Iodynis.Libraries.Utility
{
    public class ObservableList<T> : IList<T>, INotifyCollectionChanged
    {
        public class ListChangedEventArgs : EventArgs
        {
            private readonly int _Index;
            public int Index
            {
                get
                {
                    return _Index;
                }
            }
            private readonly T _Item;
            public T Item
            {
                get
                {
                    return _Item;
                }
            }
            internal ListChangedEventArgs(int index, T item)
            {
                _Index = index;
                _Item = item;
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        private readonly IList<T> _List;
        public int Count
        {
            get
            {
                return _List.Count;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return _List.IsReadOnly;
            }
        }

        public T this[int index]
        {
            get
            {
                return _List[index];
            }
            set
            {
                if (ReferenceEquals(_List[index], value))
                {
                    return;
                }
                T item = _List[index];
                _List[index] = value;
                CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, item, index));
            }
        }

        public ObservableList()
            : this (new List<T>()) { }
        public ObservableList(IList<T> list)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "List cannot be null.");
            }

            _List = new List<T>(list);
        }
        public ObservableList(IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable), "Collection cannot be null.");
            }

            _List = new List<T>(enumerable);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _List.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)_List).GetEnumerator();
        }
        public void CopyTo(T[] array, int index)
        {
            _List.CopyTo(array, index);
        }
        public bool Contains(T item)
        {
            return _List.Contains(item);
        }
        public int IndexOf(T item)
        {
            return _List.IndexOf(item);
        }
        public void Add(T item)
        {
            _List.Add(item);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, _List.Count - 1));
        }
        public void AddRange(IEnumerable<T> items)
        {
            if (items.Count() == 0)
            {
                return;
            }

            int index = _List.Count;
            _List.AddRange(items);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, items, index));
        }
        public void Reset(IEnumerable<T> items)
        {
            int count = _List.Count;
            _List.Clear();
            _List.AddRange(items);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, _List, 0));
        }
        public void Insert(int index, T item)
        {
            _List.Insert(index, item);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }
        public bool Remove(T item)
        {
            lock (_List)
            {
                int index = _List.IndexOf(item);
                if (index >= 0)
                {
                    _List.RemoveAt(index);
                    CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                    return true;
                }
                return false;
            }
        }
        public void RemoveAt(int index)
        {
            var item = _List[index];
            _List.RemoveAt(index);
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        }
        public int RemoveAll(T item)
        {
            lock (_List)
            {
                int count = 0;
                for (int index = 0; index < _List.Count; index++)
                {
                    if (Equals(_List[index], item))
                    {
                        _List.RemoveAt(index);
                        CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                        index--;
                    }
                }
                return count;
            }
        }
        public int RemoveAll(Func<T, bool> filter)
        {
            lock (_List)
            {
                int count = 0;
                for (int index = 0; index < _List.Count; index++)
                {
                    if (filter(_List[index]))
                    {
                        T item = _List[index];
                        _List.RemoveAt(index);
                        CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
                        index--;
                    }
                }
                return count;
            }
        }
        public void Clear()
        {
            _List.Clear();
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
        public override string ToString()
        {
            return _List.ToString();
        }
        //public override bool Equals(object object)
        //{
        //    return field_list.Equals(object);
        //}
        //public override int GetHashCode()
        //{
        //    return field_list.GetHashCode();
        //}
    }
}