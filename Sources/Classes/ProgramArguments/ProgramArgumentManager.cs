﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.ProgramArguments
{
    public class ProgramArgumentManager
    {
        //private Dictionary<Enum, ProgramArgumentRule> EnumToRule = new Dictionary<Enum, ProgramArgumentRule>();
        private readonly List<ProgramArgumentRule> Rules;
        private readonly Dictionary<string, Enum> KeyToEnum;
        private readonly Dictionary<Enum, ProgramArgumentRule> EnumToRule;
        private readonly int TailArgumentsCountMinimum;
        private readonly bool IgnoreCase;

        public ProgramArgumentManager(Dictionary<string, Enum> keys, IEnumerable<ProgramArgumentRule> rules, int argumentsTrailingMinimumCount = 0, bool ignoreCase = false)
        {
            if (keys == null)
            {
                throw new ArgumentNullException(nameof(keys));
            }
            if (rules == null)
            {
                throw new ArgumentNullException(nameof(rules));
            }

            Rules = new List<ProgramArgumentRule>(rules);
            TailArgumentsCountMinimum = argumentsTrailingMinimumCount;
            IgnoreCase = ignoreCase;

            if (IgnoreCase)
            {
                KeyToEnum = new Dictionary<string, Enum>();
                foreach (var keyAndEnum in keys)
                {
                    string keyLowercase = keyAndEnum.Key.ToLower();
                    if (KeyToEnum.ContainsKey(keyLowercase))
                    {
                        throw new Exception($"Multiple {keyLowercase} keys in different cases detected. Either disable the ignore case option or change the keys.");
                    }
                    KeyToEnum.Add(keyLowercase, keyAndEnum.Value);
                }
            }
            else
            {
                KeyToEnum = new Dictionary<string, Enum>(keys);
            }

            // Move the rules to a dictionary for fast search
            EnumToRule = new Dictionary<Enum, ProgramArgumentRule>();
            foreach (ProgramArgumentRule rule in Rules)
            {
                if (EnumToRule.ContainsKey(rule.Key))
                {
                    throw new Exception($"Multiple rules are defined for the argument {rule.Key}.");
                }
                EnumToRule.Add(rule.Key, rule);
            }
            // Check that every key has a rule
            foreach (Enum key in keys.Values)
            {
                if (!EnumToRule.ContainsKey(key))
                {
                    throw new Exception($"No rule defines the argument {key}.");
                }
            }
            // Ensure compatible and incompatible lists to be consistent
            foreach (ProgramArgumentRule rule in Rules)
            {
                Enum argument = rule.Key;
                // Ensure incompatible lists to be consistent
                foreach (Enum incompatibleArgument in rule.IncompatibleKeys)
                {
                    ProgramArgumentRule incompatibleRule;
                    if (!EnumToRule.TryGetValue(incompatibleArgument, out incompatibleRule))
                    {
                        throw new Exception($"Rule for argument {rule.Key} lists a incompatible argument {incompatibleArgument} that is not defined.");
                    }
                    // One is incompatible, the other one is not determined, so determine it
                    if (!incompatibleRule.IncompatibleKeys.Contains(argument))
                    {
                        incompatibleRule.IncompatibleKeys.Add(argument);
                    }
                    // One is incompatible, the other one is compatible, so a conflict is presented
                    if (incompatibleRule.CompatibleKeys.Contains(argument))
                    {
                        throw new Exception($"Rule for argument {rule.Key} lists argument {incompatibleArgument} as incompatible, however rule for argument {incompatibleArgument} lists argument {rule.Key} as compatible.");
                    }
                }
                // Ensure compatible lists to be consistent
                foreach (Enum compatibleArgument in rule.CompatibleKeys)
                {
                    ProgramArgumentRule compatibleRule;
                    if (!EnumToRule.TryGetValue(compatibleArgument, out compatibleRule))
                    {
                        throw new Exception($"Rule for argument {rule.Key} lists a compatible argument {compatibleArgument} that is not defined.");
                    }
                    // One is compatible, the other one is not determined, so determine it
                    if (!compatibleRule.CompatibleKeys.Contains(argument))
                    {
                        compatibleRule.CompatibleKeys.Add(argument);
                    }
                    // One is compatible, the other one is incompatible, so a conflict is presented
                    if (compatibleRule.IncompatibleKeys.Contains(argument))
                    {
                        throw new Exception($"Rule for argument {rule.Key} lists argument {compatibleArgument} as compatible, however rule for argument {compatibleArgument} lists argument {rule.Key} as incompatible.");
                    }
                }
            }
        }

        /// <summary>
        /// Parse command line arguments into a collection.
        /// </summary>
        /// <param name="arguments">Arguments.</param>
        /// <param name="allowJoinedShortKeyAndValue">Allow to use joined form of short keys and values without space separation. For example -pPassword will be treated as a p key and Password value.</param>
        /// <returns>Collection of arguments.</returns>
        public ProgramArgumentCollection Parse(IEnumerable<string> arguments, bool allowJoinedShortKeyAndValue = true)
        {
            if (arguments == null)
            {
                throw new ArgumentNullException(nameof(arguments));
            }

            if (arguments.Count() < TailArgumentsCountMinimum)
            {
                throw new ArgumentException($"At least {TailArgumentsCountMinimum} argument{(TailArgumentsCountMinimum == 1 ? "" : "s")} expected, {arguments.Count()} provided", nameof(arguments));
            }
            if (arguments.Count() == TailArgumentsCountMinimum)
            {
                return new ProgramArgumentCollection(null, null, arguments);
            }

            int argumentsCountBeforeTail = arguments.Count() - TailArgumentsCountMinimum;
            List<string> tailValues = new List<string>(arguments.Skip(argumentsCountBeforeTail));
            List<string> noseValues = new List<string>();
            Dictionary<string, List<List<string>>> keyToValues = new Dictionary<string, List<List<string>>>();

            string tempKey = null;
            List<string> tempValues = new List<string>();
            foreach (string argument in arguments.Take(argumentsCountBeforeTail))
            {
                // Empty
                if (argument == null)
                {
                    continue;
                }
                if (argument.Length == 0)
                {
                    continue;
                }
                // Value
                if (argument.Length == 1 || argument[0] != '-')
                {
                    tempValues.Add(argument);
                }
                // Key
                else
                {
                    if (tempKey == null)
                    {
                        noseValues = tempValues;
                    }
                    // Some values are already presented for the same key
                    else if (keyToValues.TryGetValue(tempKey, out List<List<string>> values))
                    {
                        values.Add(tempValues);
                    }
                    else
                    {
                        keyToValues.Add(tempKey, new List<List<string>>(){ tempValues });
                    }

                    tempValues = new List<string>();

                    // Key short
                    if (argument.Length == 2 || argument[1] != '-')
                    {
                        // Skip "--" argument
                        if (argument[1] == '-')
                        {
                            continue;
                        }

                        if (allowJoinedShortKeyAndValue)
                        {
                            tempKey = argument.Substring(1, 1);
                            if (argument.Length > 2)
                            {
                                tempValues.Add(argument.Substring(2));
                            }
                        }
                        else
                        {
                            tempKey = argument.Substring(1);
                        }
                    }
                    // Key long
                    else
                    {
                        // Skip "---" argument
                        if (argument[2] == '-')
                        {
                            continue;
                        }
                        tempKey = argument.Substring(2);
                    }
                }
            }

            if (tempKey == null)
            {
                noseValues = tempValues;
            }
            else if (keyToValues.TryGetValue(tempKey, out List<List<string>> values))
            {
                values.Add(tempValues);
            }
            else
            {
                keyToValues.Add(tempKey, new List<List<string>>(){ tempValues });
            }

            //Dictionary<string, List<ProgramArgument>> argumentsByKey = new Dictionary<string, List<ProgramArgument>>();

            //// Remove the tailing values
            //// There may be more of them, but these should be 100% presented
            //List<string> tail = new List<string>();
            //int tailIndex = arguments.Count() - argumentsTrailingMinimumCount;
            //while (tailIndex < argumentsRaw.Count)
            //{
            //    string value = argumentsRaw[tailIndex];
            //    // Ensure it is a value
            //    if (!IsValue(value))
            //    {
            //        if (argumentsTrailingMinimumCount == 1) throw new Exception($"Argument {value} at position of {(tailIndex + tail.Count)} is not a value. Last {argumentsTrailingMinimumCount} arguments should be values.");
            //        else                                    throw new Exception($"Argument {value} at last position is not a value. Last argument should be a value.");
            //    }
            //    tail.Add(ParseValue(value));
            //    argumentsRaw.RemoveAt(tailIndex);
            //}

            List<ProgramArgument> programArguments = new List<ProgramArgument>();

            // Collect all arguments
            foreach (KeyValuePair<string, List<List<string>>> keyAndValue in keyToValues)
            {
                foreach (List<string> values in keyAndValue.Value)
                {
                    string key = keyAndValue.Key;
                    if (IgnoreCase)
                    {
                        key = key.ToLower();
                    }

                    if (!KeyToEnum.TryGetValue(key, out Enum @enum))
                    {
                        programArguments.Add(new ProgramArgument(default(Enum), keyAndValue.Key, ProgramArgumentStatus.ERROR_KEY_UNKNOWN, values));
                        continue;
                    }
                    programArguments.Add(new ProgramArgument(@enum, keyAndValue.Key, ProgramArgumentStatus.OK, values));
                }
            }

            // Check all arguments
            for (int index = 0; index < programArguments.Count - 1; index++)
            {
                ProgramArgument programArgumentOne = programArguments[index];
                if (programArgumentOne == null || programArgumentOne.Enum == null || !EnumToRule.TryGetValue(programArgumentOne.Enum, out ProgramArgumentRule ruleOne))
                {
                    continue;
                }

                for (int jndex = index + 1; jndex < programArguments.Count; jndex++)
                {
                    ProgramArgument programArgumentTwo = programArguments[jndex];

                    // Check incompatibility
                    if (ruleOne.IncompatibleKeys.Contains(programArgumentTwo.Enum))
                    {
                        programArgumentOne.Status |= ProgramArgumentStatus.ERROR_KEY_INCOMPATIBLE;
                        programArgumentTwo.Status |= ProgramArgumentStatus.ERROR_KEY_INCOMPATIBLE;
                    }
                    // Check for duplications
                    if (!ruleOne.AllowMultiple && programArgumentOne.Enum == programArgumentTwo.Enum)
                    {
                        programArgumentOne.Status |= ProgramArgumentStatus.ERROR_KEY_DUPLICATE;
                        programArgumentTwo.Status |= ProgramArgumentStatus.ERROR_KEY_DUPLICATE;
                    }
                }
                // Check values count
                if (!ruleOne.IsValuesCountOk(programArgumentOne.Values.Count))
                {
                    programArgumentOne.Status |= ProgramArgumentStatus.ERROR_VALUES_INCORRECT_COUNT;
                }
            }
            return new ProgramArgumentCollection(programArguments, noseValues, tailValues);
        }

        #region Util
        private static bool IsKey(string argument)
        {
            //return (param_argument.Length == 2 && param_argument[0] == '-') || (param_argument.Length > 2 && param_argument[0] == '-' && param_argument[1] == '-');
            return argument.Length >= 2 && argument[0] == '-';
        }
        private static bool IsValue(string argument)
        {
            return !IsKey(argument);
        }
        private static List<string> ParseKey(string key)
        {
            List<string> keys = new List<string>();

            if (key == null)
            {
                throw new Exception("Argument is null.");
            }

            key = key.Trim(new char[] { ' ', '\t', '\r', '\n' });
            if (key.Length == 0)
            {
                throw new Exception($"Argument {key} is empty.");
            }
            // One-symbol arguments are not supported
            if (key.Length == 1)
            {
                throw new Exception($"Argument {key} is invalid. It is too short.");
            }
            // First symbol is always "-"
            if (key[0] != '-')
            {
                throw new Exception($"Argument {key} is invalid. The first symbol should be \"-\".");
            }

            // Long key
            if (key[1] == '-')
            {
                // Long arguments should be of minimum length of 4
                if (key.Length < 4)
                {
                    throw new Exception($"Argument {key} is invalid. It is too short.");
                }
                // Long arguments should be of minimum length of 4
                if (key[2] == '-')
                {
                    throw new Exception($"Argument {key} is invalid. Third symbol shouldn't be \"-\".");
                }
                if (key.Substring(2).Contains("--"))
                {
                    throw new Exception($"Argument {key} is invalid. Sequence \"--\" is not allowed inside tha argument.");
                }
                keys.Add(key.Substring(2));
            }
            // Short key(s)
            else
            {
                // Collect all symbols as short keys
                for (int index = 1; index < key.Length; index++)
                {
                    char subkey = key[index];
                    if (subkey == '-')
                    {
                        throw new Exception($"Argument {key} is invalid. Symbol \"-\" is not allowed inside one-symbol arguments block.");
                    }
                    keys.Add(subkey.ToString());
                }
            }
            return keys;
        }
        private static string ParseValue(string value)
        {
            if (value == null)
            {
                throw new Exception("Argument is null.");
            }

            value = value.Trim(new char[] { ' ', '\t', '\r', '\n' });
            if (value.Length == 0)
            {
                throw new Exception($"Argument {value} is empty.");
            }
            if (value.Length == 1)
            {
                return value;
            }
            // Trim quotes
            if (value[0] == '\'' && value[value.Length - 1] == '\'' ||
                value[0] == '\"' && value[value.Length - 1] == '\"')
            {
                value = value.Substring(1, value.Length - 2);
            }
            return value;
        }
        private static List<ProgramArgument> ParseArguments(IList<string> arguments)
        {
            List<ProgramArgument> programArguments = new List<ProgramArgument>();
            for (int index = 0; index < arguments.Count; index++)
            {
                // Ensure it is a key
                if (!IsKey(arguments[index]))
                {
                    throw new Exception($"Value {arguments[index]} is not tied to any key.");
                }
                List<string> keys = ParseKey(arguments[index]);
                if (keys.Count == 0)
                {
                    throw new Exception("No key was found.");
                }
                List<string> values = new List<string>();

                index++; // Take a step forward to the next argument
                for (; index < arguments.Count; index++)
                {
                    // Collect all values
                    if (IsValue(arguments[index]))
                    {
                        values.Add(ParseValue(arguments[index]));
                    }
                    // Break at any non-value
                    else
                    {
                        break;
                    }
                }
                index--; // Take a step back

                if (keys.Count == 1)
                {
                    programArguments.Add(new ProgramArgument(keys[0], ProgramArgumentStatus.OK, values));
                }
                else
                {
                    if (values.Count != 0)
                    {
                        throw new Exception("One-symbol keys block cannot be followed by values.");
                    }
                    foreach (string key in keys)
                    {
                        programArguments.Add(new ProgramArgument(key, ProgramArgumentStatus.OK, new List<string>()));
                    }
                }
            }
            return programArguments;
        }
        #endregion
        //private static Dictionary<string, List<Argument>> ParseArguments(string[] param_arguments, IEnumerable<ParseArgumentRule> param_rules)
        //{
        //    Dictionary<string, List<Argument>> argumentsByKey = new Dictionary<string, List<Argument>>();
        //    for (int argumentsIndex = 0; argumentsIndex < param_arguments.Length; argumentsIndex++)
        //    {
        //        Argument argument = ParseArgument(param_arguments, ref argumentsIndex);
        //        List<Argument> arguments;
        //        if (!argumentsByKey.TryGetValue(argument.Key, out arguments))
        //        {
        //            arguments = new List<Argument>();
        //            argumentsByKey.Add(argument.Key, arguments);
        //        }
        //    }
        //    return argumentsByKey;
        //}
        //public static Dictionary<string, List<ProgramArgument>> Parse(string[] param_arguments)
        //{
        //    Dictionary<string, List<ProgramArgument>> argumentsByKey = new Dictionary<string, List<ProgramArgument>>();
        //    for (int argumentsIndex = 0; argumentsIndex < param_arguments.Length; argumentsIndex++)
        //    {
        //        ProgramArgument argument = ProgramArgumentParserInner.ParseArgument(param_arguments, ref argumentsIndex);
        //        List<ProgramArgument> arguments;
        //        if (!argumentsByKey.TryGetValue(argument.Key, out arguments))
        //        {
        //            arguments = new List<ProgramArgument>();
        //            argumentsByKey.Add(argument.Key, arguments);
        //        }
        //    }
        //    return argumentsByKey;
        //}
        //public static Dictionary<string, List<string>> ParseSimplified(string[] param_arguments)
        //{
        //    Dictionary<string, List<string>> arguments = new Dictionary<string, List<string>>();
        //    for (int i = 0; i < param_arguments.Length; i++)
        //    {
        //        string key = ProgramArgumentParserInner.ParseKey(param_arguments[i]);
        //        string value = i + 1 < param_arguments.Length && ProgramArgumentParserInner.IsValue(param_arguments[i + 1]) ? ProgramArgumentParserInner.ParseValue(param_arguments[i + 1]) : null;

        //        if (arguments.ContainsKey(key))
        //        {
        //            throw new Exception("Argument {param_arguments[i]} is mentioned twice.");
        //        }
        //        arguments.Add(key, new List<string>(){ value });

        //        if (value != null)
        //        {
        //            i++;
        //        }
        //    }
        //    return arguments;
        //}
    }
}
