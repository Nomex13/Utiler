﻿using System;

namespace Iodynis.Libraries.Utility.Observables
{
    public class ObservableClamped<T> : Observable<T> where T : struct, IComparable<T>
    {
        public readonly T? Min;
        public readonly T? Max;

        public override T Value
        {
            get => base.Value;
            set
            {
                if (Min.HasValue && base.Value.CompareTo(Min.Value) <= 0)
                {
                    base.Value = Min.Value;
                }
                else if (Max.HasValue && base.Value.CompareTo(Max.Value) >= 0)
                {
                    base.Value = Max.Value;
                }
                else
                {
                    base.Value = value;
                }
            }
        }
        public ObservableClamped(T? min, T? max)
            : this(default(T), min, max) { }
        public ObservableClamped(T value, T? min, T? max)
            : base(value)
        {
            Min = min;
            Max = max;
        }
    }
}
