﻿using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Observables
{
    public class ObservableStack<T> : Stack<T>
    {
        public delegate void ObservableStackChangedEventHandler(T item);
        public delegate void ObservableStackClearEventHandler(T[] items);

        public event ObservableStackChangedEventHandler Popping;
        public event ObservableStackChangedEventHandler Popped;
        public event ObservableStackChangedEventHandler Pushing;
        public event ObservableStackChangedEventHandler Pushed;
        public event ObservableStackClearEventHandler Clearing;
        public event ObservableStackClearEventHandler Cleared;

        public ObservableStack()
            : base() { }
        public ObservableStack(IEnumerable<T> collection)
            : base(collection) { }
        public ObservableStack(int capacity)
            : base(capacity) { }

        public new void Clear()
        {
            T[] items = Clearing != null || Cleared != null ? base.ToArray() : null;

            // Invoke event handlers
            if (Clearing != null)
            {
                var clearing = Clearing;
                clearing?.Invoke(items);
            }

            base.Clear();

            // Invoke event handlers
            if (Cleared != null)
            {
                var cleared = Cleared;
                cleared?.Invoke(items);
            }
        }
        public new T Pop()
        {
            // Invoke event handlers
            if (Popping != null)
            {
                var popping = Popping;
                popping?.Invoke(base.Peek());
            }

            T item = base.Pop();

            // Invoke event handlers
            if (Popped != null)
            {
                var popped = Popped;
                popped?.Invoke(item);
            }

            return item;
        }
        public new void Push(T item)
        {
            // Invoke event handlers
            if (Pushing != null)
            {
                var pushing = Pushing;
                pushing?.Invoke(item);
            }

            base.Push(item);

            // Invoke event handlers
            if (Pushed != null)
            {
                ObservableStackChangedEventHandler pushed = Pushed;
                pushed?.Invoke(item);
            }
        }
        public new void TrimExcess()
        {
            base.TrimExcess();
        }
    }
}
