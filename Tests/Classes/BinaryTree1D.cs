﻿using System;
using System.Collections.Generic;
using Iodynis.Libraries.Utility.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Extensions.Test
{
    [TestClass]
    public class BinaryTree1DTest
    {
	    [TestMethod]
	    public void Test()
	    {
            BinaryTree1D<int, int> tree = new BinaryTree1D<int, int>();

            List<int> keys = new List<int>();
            List<int> values = new List<int>();
            int count = 100;
            for (int i = 0; i <= count; i++)
            {
                keys.Add(i * 10);
                values.Add(i * 100);
            }
            for (int i = 0; i <= count; i++)
            {
                int j =  (i * 17) % count;
                tree[keys[i]] = values[i];
            }

            // Get
	        Assert.AreEqual(0, tree[0]);
	        Assert.AreEqual(100, tree[10]);
	        Assert.AreEqual(200, tree[20]);
	        Assert.AreEqual(count * 10, tree[count]);
	        Assert.ThrowsException<Exception>(() => tree[-100] );
	        Assert.ThrowsException<Exception>(() => tree[15] );
	        Assert.ThrowsException<Exception>(() => tree[100000000] );

            // Ceiling
	        Assert.AreEqual(0, tree.GetCeiling(-100));
	        Assert.AreEqual(0, tree.GetCeiling(0));
	        Assert.AreEqual(100, tree.GetCeiling(100));
	        Assert.AreEqual(110, tree.GetCeiling(101));
	        Assert.AreEqual(110, tree.GetCeiling(109));
	        Assert.AreEqual(110, tree.GetCeiling(110));
	        Assert.AreEqual(count * 10, tree.GetCeiling(count * 10));
	        Assert.AreEqual(count * 10, tree.GetCeiling(100000000));

            // Floor
	        Assert.AreEqual(0, tree.GetFloor(-100));
	        Assert.AreEqual(0, tree.GetFloor(0));
	        Assert.AreEqual(100, tree.GetFloor(100));
	        Assert.AreEqual(100, tree.GetFloor(101));
	        Assert.AreEqual(100, tree.GetFloor(109));
	        Assert.AreEqual(110, tree.GetFloor(110));
	        Assert.AreEqual(count * 10, tree.GetFloor(count * 10));
	        Assert.AreEqual(count * 10, tree.GetFloor(100000000));
        }
    }
}
