﻿using System;
using Iodynis.Libraries.Utility.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Extensions.Test
{
    [TestClass]
    public class IntegerTest
    {
	    [TestMethod]
	    public void RoundToSignificantDigits()
	    {
	        Assert.AreEqual(100000, 123456.RoundToSignificantDigits(1));
	        Assert.AreEqual(120000, 123456.RoundToSignificantDigits(2));
	        Assert.AreEqual(123000, 123456.RoundToSignificantDigits(3));
	        Assert.AreEqual(123500, 123456.RoundToSignificantDigits(4));
	        Assert.AreEqual(123460, 123456.RoundToSignificantDigits(5));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(6));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(7));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(8));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(9));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(10));
	        Assert.AreEqual(123456, 123456.RoundToSignificantDigits(int.MaxValue));

	        Assert.AreEqual(-100000, -123456.RoundToSignificantDigits(1));
	        Assert.AreEqual(-120000, -123456.RoundToSignificantDigits(2));
	        Assert.AreEqual(-123000, -123456.RoundToSignificantDigits(3));
	        Assert.AreEqual(-123500, -123456.RoundToSignificantDigits(4));
	        Assert.AreEqual(-123460, -123456.RoundToSignificantDigits(5));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(6));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(7));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(8));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(9));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(10));
	        Assert.AreEqual(-123456, -123456.RoundToSignificantDigits(int.MaxValue));

            // Test overflow
	        Assert.AreEqual(200, 199.RoundToSignificantDigits(2));
	        Assert.AreEqual(-200, -199.RoundToSignificantDigits(2));
	        Assert.AreEqual(2_140_000_000, Int32.MaxValue.RoundToSignificantDigits(3));
	        Assert.AreEqual(-2_140_000_000, Int32.MinValue.RoundToSignificantDigits(3));

	        Assert.AreEqual(Int32.MaxValue, Int32.MaxValue.RoundToSignificantDigits(10));
	        Assert.AreEqual(Int32.MinValue, Int32.MinValue.RoundToSignificantDigits(10));

	        Assert.ThrowsException<ArgumentException>(() => 1.RoundToSignificantDigits(0));
	        Assert.ThrowsException<ArgumentException>(() => 1.RoundToSignificantDigits(-1));
	    }
	    [TestMethod]
	    public void TruncateToSignificantDigits()
	    {
	        Assert.AreEqual(100000, 123456.TruncateToSignificantDigits(1));
	        Assert.AreEqual(120000, 123456.TruncateToSignificantDigits(2));
	        Assert.AreEqual(123000, 123456.TruncateToSignificantDigits(3));
	        Assert.AreEqual(123400, 123456.TruncateToSignificantDigits(4));
	        Assert.AreEqual(123450, 123456.TruncateToSignificantDigits(5));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(6));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(7));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(8));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(9));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(10));
	        Assert.AreEqual(123456, 123456.TruncateToSignificantDigits(int.MaxValue));

	        Assert.AreEqual(-100000, -123456.TruncateToSignificantDigits(1));
	        Assert.AreEqual(-120000, -123456.TruncateToSignificantDigits(2));
	        Assert.AreEqual(-123000, -123456.TruncateToSignificantDigits(3));
	        Assert.AreEqual(-123400, -123456.TruncateToSignificantDigits(4));
	        Assert.AreEqual(-123450, -123456.TruncateToSignificantDigits(5));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(6));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(7));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(8));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(9));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(10));
	        Assert.AreEqual(-123456, -123456.TruncateToSignificantDigits(int.MaxValue));

            // Test overflow
	        Assert.AreEqual(100, 199.TruncateToSignificantDigits(1));
	        Assert.AreEqual(190, 199.TruncateToSignificantDigits(2));
	        Assert.AreEqual(199, 199.TruncateToSignificantDigits(3));

	        Assert.AreEqual(Int32.MaxValue, Int32.MaxValue.TruncateToSignificantDigits(10));
	        Assert.AreEqual(Int32.MinValue, Int32.MinValue.TruncateToSignificantDigits(10));

	        Assert.AreEqual(-100, -199.TruncateToSignificantDigits(1));
	        Assert.AreEqual(-190, -199.TruncateToSignificantDigits(2));
	        Assert.AreEqual(-199, -199.TruncateToSignificantDigits(3));

	        Assert.ThrowsException<ArgumentException>(() => 1.TruncateToSignificantDigits(0));
	        Assert.ThrowsException<ArgumentException>(() => 1.TruncateToSignificantDigits(-1));
	    }
	    [TestMethod]
	    public void CountDigits()
	    {
            foreach (int multiplier in new int[] { 2, 3, 10 })
            {
                for (Int32 i = 1; i < Int32.MaxValue / multiplier; i *= multiplier)
                {
                    Assert.AreEqual(i.ToString().Length, i.CountDigits());
                    Assert.AreEqual(-i.ToString().Length, -i.CountDigits());
                }
            }

            Assert.AreEqual(Int32.MaxValue.ToString().Length, Int32.MaxValue.CountDigits());
            Assert.AreEqual(Int32.MinValue.ToString().Length - 1 /* minus sign */, Int32.MinValue.CountDigits());
        }
    }
}
