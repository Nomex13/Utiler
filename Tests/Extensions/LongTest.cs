﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Extensions.Test
{
    [TestClass]
    public class LongTest
    {
	    [TestMethod]
	    public void RoundToSignificantDigits()
	    {
	        Assert.AreEqual(1000000000000000000, 1234567890123456789.RoundToSignificantDigits(1));
	        Assert.AreEqual(1200000000000000000, 1234567890123456789.RoundToSignificantDigits(2));
	        Assert.AreEqual(1230000000000000000, 1234567890123456789.RoundToSignificantDigits(3));
	        Assert.AreEqual(1235000000000000000, 1234567890123456789.RoundToSignificantDigits(4));
	        Assert.AreEqual(1234600000000000000, 1234567890123456789.RoundToSignificantDigits(5));
	        Assert.AreEqual(1234570000000000000, 1234567890123456789.RoundToSignificantDigits(6));
	        Assert.AreEqual(1234568000000000000, 1234567890123456789.RoundToSignificantDigits(7));
	        Assert.AreEqual(1234567900000000000, 1234567890123456789.RoundToSignificantDigits(8));
	        Assert.AreEqual(1234567890000000000, 1234567890123456789.RoundToSignificantDigits(9));
	        Assert.AreEqual(1234567890000000000, 1234567890123456789.RoundToSignificantDigits(10));
	        Assert.AreEqual(1234567890100000000, 1234567890123456789.RoundToSignificantDigits(11));
	        Assert.AreEqual(1234567890120000000, 1234567890123456789.RoundToSignificantDigits(12));
	        Assert.AreEqual(1234567890123000000, 1234567890123456789.RoundToSignificantDigits(13));
	        Assert.AreEqual(1234567890123500000, 1234567890123456789.RoundToSignificantDigits(14));
	        Assert.AreEqual(1234567890123460000, 1234567890123456789.RoundToSignificantDigits(15));
	        Assert.AreEqual(1234567890123457000, 1234567890123456789.RoundToSignificantDigits(16));
	        Assert.AreEqual(1234567890123456800, 1234567890123456789.RoundToSignificantDigits(17));
	        Assert.AreEqual(1234567890123456790, 1234567890123456789.RoundToSignificantDigits(18));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.RoundToSignificantDigits(19));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.RoundToSignificantDigits(20));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.RoundToSignificantDigits(21));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.RoundToSignificantDigits(int.MaxValue));

	        Assert.AreEqual(-1000000000000000000, -1234567890123456789.RoundToSignificantDigits(1));
	        Assert.AreEqual(-1200000000000000000, -1234567890123456789.RoundToSignificantDigits(2));
	        Assert.AreEqual(-1230000000000000000, -1234567890123456789.RoundToSignificantDigits(3));
	        Assert.AreEqual(-1235000000000000000, -1234567890123456789.RoundToSignificantDigits(4));
	        Assert.AreEqual(-1234600000000000000, -1234567890123456789.RoundToSignificantDigits(5));
	        Assert.AreEqual(-1234570000000000000, -1234567890123456789.RoundToSignificantDigits(6));
	        Assert.AreEqual(-1234568000000000000, -1234567890123456789.RoundToSignificantDigits(7));
	        Assert.AreEqual(-1234567900000000000, -1234567890123456789.RoundToSignificantDigits(8));
	        Assert.AreEqual(-1234567890000000000, -1234567890123456789.RoundToSignificantDigits(9));
	        Assert.AreEqual(-1234567890000000000, -1234567890123456789.RoundToSignificantDigits(10));
	        Assert.AreEqual(-1234567890100000000, -1234567890123456789.RoundToSignificantDigits(11));
	        Assert.AreEqual(-1234567890120000000, -1234567890123456789.RoundToSignificantDigits(12));
	        Assert.AreEqual(-1234567890123000000, -1234567890123456789.RoundToSignificantDigits(13));
	        Assert.AreEqual(-1234567890123500000, -1234567890123456789.RoundToSignificantDigits(14));
	        Assert.AreEqual(-1234567890123460000, -1234567890123456789.RoundToSignificantDigits(15));
	        Assert.AreEqual(-1234567890123457000, -1234567890123456789.RoundToSignificantDigits(16));
	        Assert.AreEqual(-1234567890123456800, -1234567890123456789.RoundToSignificantDigits(17));
	        Assert.AreEqual(-1234567890123456790, -1234567890123456789.RoundToSignificantDigits(18));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.RoundToSignificantDigits(19));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.RoundToSignificantDigits(20));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.RoundToSignificantDigits(21));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.RoundToSignificantDigits(int.MaxValue));

            // Test overflow
	        Assert.AreEqual(200, 199.RoundToSignificantDigits(2));
	        Assert.AreEqual(-200, -199.RoundToSignificantDigits(2));

	        Assert.AreEqual(Int64.MaxValue, Int64.MaxValue.RoundToSignificantDigits(19));
	        Assert.AreEqual(Int64.MinValue, Int64.MinValue.RoundToSignificantDigits(19));
	        Assert.AreEqual(9_223_300_000_000_000_000L, Int64.MaxValue.RoundToSignificantDigits(5));
	        Assert.AreEqual(-9_223_300_000_000_000_000L, Int64.MinValue.RoundToSignificantDigits(5));

	        Assert.ThrowsException<ArgumentException>(() => 1.RoundToSignificantDigits(0));
	        Assert.ThrowsException<ArgumentException>(() => 1.RoundToSignificantDigits(-1));
	    }
	    [TestMethod]
	    public void TruncateToSignificantDigits()
	    {
	        Assert.AreEqual(1000000000000000000, 1234567890123456789.TruncateToSignificantDigits(1));
	        Assert.AreEqual(1200000000000000000, 1234567890123456789.TruncateToSignificantDigits(2));
	        Assert.AreEqual(1230000000000000000, 1234567890123456789.TruncateToSignificantDigits(3));
	        Assert.AreEqual(1234000000000000000, 1234567890123456789.TruncateToSignificantDigits(4));
	        Assert.AreEqual(1234500000000000000, 1234567890123456789.TruncateToSignificantDigits(5));
	        Assert.AreEqual(1234560000000000000, 1234567890123456789.TruncateToSignificantDigits(6));
	        Assert.AreEqual(1234567000000000000, 1234567890123456789.TruncateToSignificantDigits(7));
	        Assert.AreEqual(1234567800000000000, 1234567890123456789.TruncateToSignificantDigits(8));
	        Assert.AreEqual(1234567890000000000, 1234567890123456789.TruncateToSignificantDigits(9));
	        Assert.AreEqual(1234567890000000000, 1234567890123456789.TruncateToSignificantDigits(10));
	        Assert.AreEqual(1234567890100000000, 1234567890123456789.TruncateToSignificantDigits(11));
	        Assert.AreEqual(1234567890120000000, 1234567890123456789.TruncateToSignificantDigits(12));
	        Assert.AreEqual(1234567890123000000, 1234567890123456789.TruncateToSignificantDigits(13));
	        Assert.AreEqual(1234567890123400000, 1234567890123456789.TruncateToSignificantDigits(14));
	        Assert.AreEqual(1234567890123450000, 1234567890123456789.TruncateToSignificantDigits(15));
	        Assert.AreEqual(1234567890123456000, 1234567890123456789.TruncateToSignificantDigits(16));
	        Assert.AreEqual(1234567890123456700, 1234567890123456789.TruncateToSignificantDigits(17));
	        Assert.AreEqual(1234567890123456780, 1234567890123456789.TruncateToSignificantDigits(18));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.TruncateToSignificantDigits(19));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.TruncateToSignificantDigits(20));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.TruncateToSignificantDigits(21));
	        Assert.AreEqual(1234567890123456789, 1234567890123456789.TruncateToSignificantDigits(int.MaxValue));

	        Assert.AreEqual(-1000000000000000000, -1234567890123456789.TruncateToSignificantDigits(1));
	        Assert.AreEqual(-1200000000000000000, -1234567890123456789.TruncateToSignificantDigits(2));
	        Assert.AreEqual(-1230000000000000000, -1234567890123456789.TruncateToSignificantDigits(3));
	        Assert.AreEqual(-1234000000000000000, -1234567890123456789.TruncateToSignificantDigits(4));
	        Assert.AreEqual(-1234500000000000000, -1234567890123456789.TruncateToSignificantDigits(5));
	        Assert.AreEqual(-1234560000000000000, -1234567890123456789.TruncateToSignificantDigits(6));
	        Assert.AreEqual(-1234567000000000000, -1234567890123456789.TruncateToSignificantDigits(7));
	        Assert.AreEqual(-1234567800000000000, -1234567890123456789.TruncateToSignificantDigits(8));
	        Assert.AreEqual(-1234567890000000000, -1234567890123456789.TruncateToSignificantDigits(9));
	        Assert.AreEqual(-1234567890000000000, -1234567890123456789.TruncateToSignificantDigits(10));
	        Assert.AreEqual(-1234567890100000000, -1234567890123456789.TruncateToSignificantDigits(11));
	        Assert.AreEqual(-1234567890120000000, -1234567890123456789.TruncateToSignificantDigits(12));
	        Assert.AreEqual(-1234567890123000000, -1234567890123456789.TruncateToSignificantDigits(13));
	        Assert.AreEqual(-1234567890123400000, -1234567890123456789.TruncateToSignificantDigits(14));
	        Assert.AreEqual(-1234567890123450000, -1234567890123456789.TruncateToSignificantDigits(15));
	        Assert.AreEqual(-1234567890123456000, -1234567890123456789.TruncateToSignificantDigits(16));
	        Assert.AreEqual(-1234567890123456700, -1234567890123456789.TruncateToSignificantDigits(17));
	        Assert.AreEqual(-1234567890123456780, -1234567890123456789.TruncateToSignificantDigits(18));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.TruncateToSignificantDigits(19));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.TruncateToSignificantDigits(20));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.TruncateToSignificantDigits(21));
	        Assert.AreEqual(-1234567890123456789, -1234567890123456789.TruncateToSignificantDigits(int.MaxValue));

            // Test overflow
	        Assert.AreEqual(100L, 199L.TruncateToSignificantDigits(1));
	        Assert.AreEqual(190L, 199L.TruncateToSignificantDigits(2));
	        Assert.AreEqual(199L, 199L.TruncateToSignificantDigits(3));

	        Assert.AreEqual(-100L, -199L.TruncateToSignificantDigits(1));
	        Assert.AreEqual(-190L, -199L.TruncateToSignificantDigits(2));
	        Assert.AreEqual(-199L, -199L.TruncateToSignificantDigits(3));

	        Assert.AreEqual(Int64.MaxValue, Int64.MaxValue.TruncateToSignificantDigits(19));
	        Assert.AreEqual(Int64.MinValue, Int64.MinValue.TruncateToSignificantDigits(19));

	        Assert.ThrowsException<ArgumentException>(() => 1.TruncateToSignificantDigits(0));
	        Assert.ThrowsException<ArgumentException>(() => 1.TruncateToSignificantDigits(-1));
	    }
	    [TestMethod]
	    public void CountDigits()
	    {
            foreach (int multiplier in new int[] { 2, 3, 10 })
            {
                for (Int64 i = 1; i < Int64.MaxValue / multiplier; i *= multiplier)
                {
                    Assert.AreEqual(i.ToString().Length, i.CountDigits());
                    Assert.AreEqual(-i.ToString().Length, -i.CountDigits());
                }
            }

            Assert.AreEqual(Int64.MaxValue.ToString().Length, Int64.MaxValue.CountDigits());
            Assert.AreEqual(Int64.MinValue.ToString().Length - 1 /* minus sign */, Int64.MinValue.CountDigits());
        }
    }
}
