﻿using System;
using System.Collections.Generic;
using Iodynis.Libraries.Utility.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Test
{
    [TestClass]
    public class SortedListTest
    {
		[TestMethod]
		public void GetNearestDown()
		{
            SortedList<int, int> list1 = new SortedList<int, int>()
            {
                { 0, -1},
                { 1, -1},
                { 2, -1},
                { 3, -1},
                { 4, -1},
                { 5, -1},
                { 6, -1},
                { 7, -1},
                { 8, -1},
                { 9, -1},
                { 10, -1},
            };
            SortedList<int, int> list2 = new SortedList<int, int>()
            {
                { 0, -1},
                { 10, -1},
                { 20, -1},
                { 30, -1},
                { 40, -1},
                { 50, -1},
                { 60, -1},
                { 70, -1},
                { 80, -1},
                { 90, -1},
                { 100, -1},
            };
            SortedList<int, int> list3 = new SortedList<int, int>()
            {
                { 10, -1},
                { 15, -1},
                { 20, -1},
                { 25, -1},
                { 30, -1},
                { 35, -1},
                { 40, -1},
                { 45, -1},
                { 50, -1},
            };

            for (int i = 0; i <= 10; i++) Assert.AreEqual(i, list1.GetNearestDown(i));
            for (int i = 0; i < 100; i++) Assert.AreEqual(i - i % 10, list2.GetNearestDown(i));
            for (int i = 10; i < 50; i++) Assert.AreEqual(i - i % 5, list3.GetNearestDown(i));
            for (int i = 60; i < 100; i++) Assert.AreEqual(50, list3.GetNearestDown(i));
            for (int i = 0; i < 10; i++) Assert.AreEqual(10, list3.GetNearestDown(i));

            Assert.ThrowsException<ArgumentNullException>(() => ((SortedList<int, int>)null).GetNearestDown(10));
			Assert.ThrowsException<ArgumentException>(() => new SortedList<int, int>().GetNearestDown(10));
        }
    }
}
