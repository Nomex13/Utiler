﻿using System;
using Iodynis.Libraries.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Test
{
    [TestClass]
    public class FilesystemTest
    {
		[TestMethod]
		public void FileGetName()
		{
			Assert.AreEqual(@"test.tar.gz", Utiler.FileGetName(@"C:\Program Files\test.tar.gz"));
			Assert.AreEqual(@"test.tar", Utiler.FileGetName(@"file:///Stuff\test.tar"));
			Assert.AreEqual(@"test", Utiler.FileGetName(@"./..///test"));
			Assert.AreEqual(@"", Utiler.FileGetName(@"./..///test/"));
			Assert.AreEqual(@"Testing", Utiler.FileGetName(@"Testing"));
			Assert.AreEqual(@"", Utiler.FileGetName(@".\"));
			Assert.AreEqual(@"", Utiler.FileGetName(@""));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.FileGetName(null));
		}
		[TestMethod]
		public void FileGetExtension()
		{
			Assert.AreEqual(@"gz", Utiler.FileGetExtension(@"C:\Program Files\test.tar.gz"));
			Assert.AreEqual(@"tar", Utiler.FileGetExtension(@"file:///Stuff/test.tar"));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@"file:///Stuff\test.tar."));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@"./..///test"));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@"./..///test/"));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@"Testing"));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@".\"));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@""));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@"."));
			Assert.AreEqual(@"", Utiler.FileGetExtension(@".."));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.FileGetExtension(null));
		}
		[TestMethod]
		public void FileGetNameWithoutExtension()
		{
			Assert.AreEqual(@"test.tar.gz", Utiler.FileGetNameWithoutExtension(@"C:\Program Files\test.tar.gz", 0));
			Assert.AreEqual(@"test.tar", Utiler.FileGetNameWithoutExtension(@"C:\Program Files\test.tar.gz", 1));
			Assert.AreEqual(@"test1", Utiler.FileGetNameWithoutExtension(@"C:\Program Files\test1.tar.gz", 2));
			Assert.AreEqual(@"test2", Utiler.FileGetNameWithoutExtension(@"C:\Program Files\test2.tar.gz", 3));
			Assert.AreEqual(@"test3", Utiler.FileGetNameWithoutExtension(@"C:\Program Files\test3.tar.gz", 100));
			Assert.AreEqual(@"test4", Utiler.FileGetNameWithoutExtension(@"file:///Stuff\test4.tar"));
			Assert.AreEqual(@"test5", Utiler.FileGetNameWithoutExtension(@"/test5"));
			Assert.AreEqual(@"test6", Utiler.FileGetNameWithoutExtension(@"./..///test6"));
			Assert.AreEqual(@"", Utiler.FileGetNameWithoutExtension(@"./..///test/"));
			Assert.AreEqual(@"Testing", Utiler.FileGetNameWithoutExtension(@"Testing"));
			Assert.AreEqual(@"", Utiler.FileGetNameWithoutExtension(@".\"));
			Assert.AreEqual(@"", Utiler.FileGetNameWithoutExtension(@""));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.FileGetNameWithoutExtension(null, 1));
			Assert.ThrowsException<ArgumentException>(() => Utiler.FileGetNameWithoutExtension("test.tar.gz", -1));
		}
	    [TestMethod]
	    public void FileGetDirectoryAndName()
	    {
	        Assert.AreEqual((@"C:\Program Files\", @"test.tar.gz"), Utiler.FileGetDirectoryAndName(@"C:\Program Files\test.tar.gz"));
	        Assert.AreEqual((@"file:///Stuff\", @"test.tar"), Utiler.FileGetDirectoryAndName(@"file:///Stuff\test.tar"));
	        Assert.AreEqual((@"./..///", @"test"), Utiler.FileGetDirectoryAndName(@"./..///test"));
	        Assert.AreEqual((@"./..///test/", @""), Utiler.FileGetDirectoryAndName(@"./..///test/"));
	        Assert.AreEqual((@"", @"Testing"), Utiler.FileGetDirectoryAndName(@"Testing"));
	        Assert.AreEqual((@".\", @""), Utiler.FileGetDirectoryAndName(@".\"));
	        Assert.AreEqual((@"", @""), Utiler.FileGetDirectoryAndName(@""));

	        Assert.ThrowsException<ArgumentNullException>(() => Utiler.FileGetDirectoryAndName(null));
	    }
	    [TestMethod]
	    public void FileGetDirectoryAndNameAndExtension()
	    {
	        Assert.AreEqual((@"C:\Program Files\", @"test.tar", @".gz"), Utiler.FileGetDirectoryAndNameAndExtension(@"C:\Program Files\test.tar.gz"));
	        Assert.AreEqual((@"C:\Program Files\", @"test", @".tar.gz"), Utiler.FileGetDirectoryAndNameAndExtension(@"C:\Program Files\test.tar.gz", 2));
	        Assert.AreEqual((@"C:\Program Files\", @"test", @".tar.gz"), Utiler.FileGetDirectoryAndNameAndExtension(@"C:\Program Files\test.tar.gz", 3));
	        Assert.AreEqual((@"file:///Stuff\", @"test", @".tar"), Utiler.FileGetDirectoryAndNameAndExtension(@"file:///Stuff\test.tar"));
	        Assert.AreEqual((@"./..///", @"test", @""), Utiler.FileGetDirectoryAndNameAndExtension(@"./..///test"));
	        Assert.AreEqual((@"./..///test/", @"", @""), Utiler.FileGetDirectoryAndNameAndExtension(@"./..///test/"));
	        Assert.AreEqual((@"", @"Testing", @""), Utiler.FileGetDirectoryAndNameAndExtension(@"Testing"));
	        Assert.AreEqual((@".\", @"", @""), Utiler.FileGetDirectoryAndNameAndExtension(@".\"));
	        Assert.AreEqual((@"", @"", @""), Utiler.FileGetDirectoryAndNameAndExtension(@""));

	        Assert.ThrowsException<ArgumentNullException>(() => Utiler.FileGetDirectoryAndNameAndExtension(null));
	    }
    }
}
